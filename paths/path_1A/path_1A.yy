{
    "id": "543ae8b6-dfb8-4a3f-8b03-bcf098b60e6f",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_1A",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "347eb0f9-d0d7-4e89-8783-2e6905147f73",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 224,
            "speed": 100
        },
        {
            "id": "721a4d6b-0721-43b9-8328-d066ab702d30",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 64,
            "speed": 100
        },
        {
            "id": "c52ac616-221c-4fcc-9b22-2258839da250",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 64,
            "speed": 100
        },
        {
            "id": "0de672c1-5b86-4c95-a27c-668429ee738b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 64,
            "speed": 100
        },
        {
            "id": "593d8ff5-0f75-403a-8b63-610446e4bff7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 512,
            "speed": 100
        },
        {
            "id": "961d9329-1ae1-4235-823a-3febf05d48ed",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 512,
            "speed": 100
        },
        {
            "id": "a2e9e2f2-03af-484c-b332-eb75f694da36",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 512,
            "speed": 100
        },
        {
            "id": "09b4328c-3755-47ae-94de-161cfc186c44",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 544,
            "speed": 100
        },
        {
            "id": "e967164a-58ae-4e65-8a89-0a6e3339ff26",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 832,
            "y": 576,
            "speed": 100
        },
        {
            "id": "b2a92f38-4b2a-48d2-aba3-033ed93e7b7a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 640,
            "speed": 100
        },
        {
            "id": "383a52f0-d765-4a44-bb9c-9dde10174de6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 864,
            "y": 704,
            "speed": 100
        },
        {
            "id": "94644da5-1999-4d67-81bf-ab7d7b8ad3bf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 800,
            "y": 704,
            "speed": 100
        },
        {
            "id": "478e3873-1bae-4e2b-bb81-59de1585dd24",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 768,
            "y": 640,
            "speed": 100
        },
        {
            "id": "c5796768-6ffa-49a4-9b80-0d9811e53ca1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 640,
            "speed": 100
        },
        {
            "id": "40f1ca58-06c7-4608-bb2a-9f22fb6c3299",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704,
            "y": 672,
            "speed": 100
        },
        {
            "id": "29bf7244-2036-4cec-93f2-0a3d2a83f503",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": 704,
            "speed": 100
        },
        {
            "id": "333bc87d-4ca6-495a-803e-97c904d3bacb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 704,
            "speed": 100
        },
        {
            "id": "2c249eb0-c959-4409-a5e0-62811a491d5b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 384,
            "speed": 100
        },
        {
            "id": "12050e4a-7388-4696-a4b0-a5e5f18f222c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 384,
            "speed": 100
        },
        {
            "id": "c29e2385-f2c1-4141-b661-8b9f1c3ea299",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 352,
            "speed": 100
        },
        {
            "id": "b77cbff5-be67-472a-9c0f-7d98536b2c73",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 352,
            "speed": 100
        },
        {
            "id": "9b1dbec1-7c0c-47bc-8289-f9242e761c6c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 320,
            "speed": 100
        },
        {
            "id": "a93aa004-979b-47dd-8a04-1ea862e0ca1f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 256,
            "speed": 100
        },
        {
            "id": "8d17ad8a-44b9-4af5-8093-34679a28971a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 256,
            "speed": 100
        },
        {
            "id": "90ca23a9-f8d6-4bd8-b931-2e6c6daacd0e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 256,
            "speed": 100
        },
        {
            "id": "6e39fcb6-9133-4c59-a2ac-e8031a834495",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 288,
            "speed": 100
        },
        {
            "id": "bb180ff4-3726-413c-a3a3-6afd7023a9b2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 288,
            "speed": 100
        },
        {
            "id": "ce44534e-0990-4245-a3ef-1a3b1fdec79f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 256,
            "speed": 100
        },
        {
            "id": "6815ea8e-744b-46d3-8f90-5c8f323793c7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 160,
            "speed": 100
        },
        {
            "id": "c45d3414-4eb6-4a98-a8ed-5ed04e95bc59",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 160,
            "speed": 100
        },
        {
            "id": "e5702031-2753-48a2-9833-d7e715f3184e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 544,
            "y": 96,
            "speed": 100
        },
        {
            "id": "17c31526-d248-4563-b7d1-551b1eafa511",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 160,
            "speed": 100
        },
        {
            "id": "0894b812-92f3-4f26-9b1f-e3a32d84964f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 160,
            "speed": 100
        },
        {
            "id": "d5e20d46-1f3f-40d4-abd8-b8df4b887534",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 160,
            "speed": 100
        },
        {
            "id": "22cdd048-8b77-419b-8935-0bf32713a6fc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 224,
            "speed": 100
        },
        {
            "id": "62c76c4e-aebe-461f-bce3-3382a308dedf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 288,
            "speed": 100
        },
        {
            "id": "60388f07-d41b-4c18-a4f1-abf7fc0ea7ad",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 352,
            "speed": 100
        },
        {
            "id": "b918093c-47f4-48e8-831f-0dae252b1102",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 384,
            "speed": 100
        },
        {
            "id": "e38be0a8-dcd8-4bf4-b72a-9931878be3f3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 448,
            "speed": 100
        },
        {
            "id": "903e4e46-04bc-4e3b-b7bf-7bdf8efcd246",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 448,
            "speed": 100
        },
        {
            "id": "d04cee72-48e9-4ed5-9d2d-e5ed7c218a84",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 480,
            "speed": 100
        },
        {
            "id": "b93f8a54-e7cd-436c-88a9-ecdc2d2ee3fd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 544,
            "speed": 100
        },
        {
            "id": "dfc67dea-4b08-4ab4-bfa6-214fab261885",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 448,
            "y": 608,
            "speed": 100
        },
        {
            "id": "03cd511f-eddb-4d87-a83c-843ed9078ea4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 704,
            "speed": 100
        },
        {
            "id": "9e47129c-be94-487b-a142-e39d81bc6825",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 704,
            "speed": 100
        },
        {
            "id": "8b0ea483-088e-4ec5-838d-50b8be8287ff",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 512,
            "speed": 100
        },
        {
            "id": "81692dc4-390d-4ae8-8eb1-bfb393326d48",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 416,
            "speed": 100
        },
        {
            "id": "f909dd5e-cc26-4cfe-bc02-d884bee37b82",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 352,
            "speed": 100
        },
        {
            "id": "3ada4ce7-3121-49af-8912-fd49a4c6fae6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 352,
            "speed": 100
        },
        {
            "id": "eabdc490-2660-4fb7-b651-e3574c6881c3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 352,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}