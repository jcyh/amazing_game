{
    "id": "c329d312-c3b0-4937-b4f9-278a494d0ac9",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_titles",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Viner Hand ITC",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "d5026a3b-cc86-488e-a346-7b2f5e80fc4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 77,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e9c6c79b-3e66-421e-9253-008140b7c543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 77,
                "offset": 3,
                "shift": 19,
                "w": 17,
                "x": 430,
                "y": 239
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "587396eb-449b-4426-b86c-10c10835dbf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 77,
                "offset": 5,
                "shift": 22,
                "w": 16,
                "x": 412,
                "y": 239
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e4f7a0f9-1a3d-47fa-893e-f2341bcc637c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 77,
                "offset": -1,
                "shift": 30,
                "w": 32,
                "x": 378,
                "y": 239
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "4b0e2424-3f09-4e0b-a128-0a1e5c59a097",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 77,
                "offset": 1,
                "shift": 32,
                "w": 27,
                "x": 349,
                "y": 239
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "39cb5860-c871-4a74-8003-007bb3f50bec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 77,
                "offset": -2,
                "shift": 34,
                "w": 37,
                "x": 310,
                "y": 239
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b2ce72b3-fb9f-4e38-8ade-505d05290336",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 77,
                "offset": 1,
                "shift": 27,
                "w": 23,
                "x": 285,
                "y": 239
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "484e65bd-3037-4ee6-8128-2d2cae9ec60d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 77,
                "offset": 5,
                "shift": 14,
                "w": 8,
                "x": 275,
                "y": 239
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "a0b700f6-f783-4e14-a70a-05ecd5a06332",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 77,
                "offset": 9,
                "shift": 26,
                "w": 19,
                "x": 254,
                "y": 239
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "52e5cff9-e7b5-4b6b-a143-188b74fb4c7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 77,
                "offset": 1,
                "shift": 26,
                "w": 16,
                "x": 236,
                "y": 239
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e807d331-1dcd-4159-bb4e-784f748ef34d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 77,
                "offset": 4,
                "shift": 31,
                "w": 25,
                "x": 449,
                "y": 239
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "288696a1-8d46-4ee7-bcbf-be843562884f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 77,
                "offset": 5,
                "shift": 40,
                "w": 30,
                "x": 204,
                "y": 239
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "0f1c33ff-d678-4893-af65-871274b2fcbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 77,
                "offset": 0,
                "shift": 14,
                "w": 9,
                "x": 162,
                "y": 239
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "3d92626e-3026-4541-97a4-907510cd7e2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 77,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 144,
                "y": 239
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "660ca28c-33ea-4e59-a7fb-b292530c3942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 77,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 132,
                "y": 239
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "48692b74-ab3f-432c-b0df-3561125b9678",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 77,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 109,
                "y": 239
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "8fa999f1-4d7b-467e-ada2-c800b1d768fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 77,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 88,
                "y": 239
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2d5dbadd-4ae4-41c5-a826-f51e669dc4d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 77,
                "offset": 1,
                "shift": 15,
                "w": 17,
                "x": 69,
                "y": 239
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "34dab5b1-d82c-41a9-a368-c059ab3455c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 77,
                "offset": -4,
                "shift": 33,
                "w": 36,
                "x": 31,
                "y": 239
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c2d9f274-c917-4b54-83cb-6d21a1f1d5ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 77,
                "offset": -3,
                "shift": 23,
                "w": 27,
                "x": 2,
                "y": 239
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "b687edfa-a57c-4d18-98c4-4cea3378838e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 77,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 458,
                "y": 160
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "8ede6122-5003-4993-940c-8c6009f5b967",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 77,
                "offset": -3,
                "shift": 21,
                "w": 29,
                "x": 173,
                "y": 239
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "b862ee28-eab9-40ec-879a-45a395c3e0dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 77,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 476,
                "y": 239
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "77fed581-f84e-4211-aeaa-0bdcac9315eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 77,
                "offset": 2,
                "shift": 23,
                "w": 23,
                "x": 2,
                "y": 318
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "64132c27-cecc-48f0-b503-491a410cc1d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 77,
                "offset": 0,
                "shift": 24,
                "w": 28,
                "x": 27,
                "y": 318
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "5c7683d8-58f1-4e08-8e53-e2bea7a66d8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 77,
                "offset": 2,
                "shift": 22,
                "w": 23,
                "x": 233,
                "y": 397
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "55e8b201-a737-4804-950a-d4934ed1355a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 77,
                "offset": 1,
                "shift": 14,
                "w": 9,
                "x": 222,
                "y": 397
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6ba85bf1-438a-42ab-8be9-0810abe36b55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 77,
                "offset": 1,
                "shift": 14,
                "w": 9,
                "x": 211,
                "y": 397
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5cf206bf-8a92-4fe5-8c6a-7ee12dbc660a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 77,
                "offset": 6,
                "shift": 40,
                "w": 28,
                "x": 181,
                "y": 397
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "de5f5f17-e535-4c93-9294-7478e4f9f05e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 77,
                "offset": 5,
                "shift": 40,
                "w": 30,
                "x": 149,
                "y": 397
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "c49caea9-16e1-49eb-ab1b-6322f748ad93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 77,
                "offset": 6,
                "shift": 40,
                "w": 28,
                "x": 119,
                "y": 397
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c94f9a00-7a24-4b20-a893-3c5c40f7dde4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 77,
                "offset": 3,
                "shift": 27,
                "w": 27,
                "x": 90,
                "y": 397
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "7e12d6e9-47c1-4077-8ded-700e1961087f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 77,
                "offset": 3,
                "shift": 47,
                "w": 41,
                "x": 47,
                "y": 397
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "74460a60-ecf7-418c-ae05-1388e65f1bc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 77,
                "offset": -1,
                "shift": 38,
                "w": 43,
                "x": 2,
                "y": 397
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "4188cbd5-05aa-4a9c-a416-5191f41bcf6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 77,
                "offset": 2,
                "shift": 37,
                "w": 34,
                "x": 451,
                "y": 318
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9d693b0e-867c-499f-925d-e6f4ea87db4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 77,
                "offset": 1,
                "shift": 35,
                "w": 34,
                "x": 415,
                "y": 318
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "0221b343-f5bd-422a-bc17-c5ee1c408ba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 77,
                "offset": 0,
                "shift": 36,
                "w": 36,
                "x": 377,
                "y": 318
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "eb29c995-7fe9-4869-963b-8ca4fb5d44ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 77,
                "offset": -2,
                "shift": 33,
                "w": 33,
                "x": 342,
                "y": 318
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "54e9e83f-e6fc-41e8-94ee-a37feeacc5a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 77,
                "offset": 1,
                "shift": 28,
                "w": 30,
                "x": 310,
                "y": 318
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "422163f9-dc6b-4052-b5ed-2ed575cc34b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 77,
                "offset": 0,
                "shift": 30,
                "w": 26,
                "x": 282,
                "y": 318
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f008ebb0-cd23-4c72-8e37-8fe44631b786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 77,
                "offset": 1,
                "shift": 39,
                "w": 37,
                "x": 243,
                "y": 318
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8c9c68b5-a625-4dc0-a6b2-15f6864df2f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 77,
                "offset": 0,
                "shift": 19,
                "w": 22,
                "x": 219,
                "y": 318
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6ae85631-362a-4f83-be2d-99feae9d5379",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 77,
                "offset": -10,
                "shift": 22,
                "w": 34,
                "x": 183,
                "y": 318
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "185d2000-40b6-4209-8a7a-b47389c5e846",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 77,
                "offset": 0,
                "shift": 42,
                "w": 43,
                "x": 138,
                "y": 318
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "ef52b68d-d11c-4c2a-888a-1282110b05e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 77,
                "offset": -3,
                "shift": 34,
                "w": 36,
                "x": 100,
                "y": 318
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "24b4eaa7-53fe-4f17-9ed1-da12cd5ea2e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 77,
                "offset": 0,
                "shift": 44,
                "w": 41,
                "x": 57,
                "y": 318
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "0c0c8a56-90a9-4f12-8f82-e95fbbe56a55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 77,
                "offset": 0,
                "shift": 39,
                "w": 42,
                "x": 414,
                "y": 160
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "2abb3933-9729-4a6f-96af-7076780ef75f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 77,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 390,
                "y": 160
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "6e7f3659-6cd7-4efa-9d86-c0d6954da1ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 77,
                "offset": -1,
                "shift": 35,
                "w": 36,
                "x": 352,
                "y": 160
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "170dddc6-62f6-4667-8622-e989de8f8d65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 77,
                "offset": 1,
                "shift": 40,
                "w": 39,
                "x": 159,
                "y": 81
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "2321853d-dc06-453b-a7c7-25ecc9aba9ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 77,
                "offset": -1,
                "shift": 33,
                "w": 33,
                "x": 102,
                "y": 81
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "406da3e9-84fe-4af0-9fab-a3630aedb67e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 77,
                "offset": 0,
                "shift": 33,
                "w": 30,
                "x": 70,
                "y": 81
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "46b288af-ffb9-4f4e-8d0d-2c1521d29eeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 77,
                "offset": 1,
                "shift": 29,
                "w": 34,
                "x": 34,
                "y": 81
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "449ba5bc-746d-4407-b22a-64d8e2d45de8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 77,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 2,
                "y": 81
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "144778e2-2e8f-430a-b2a7-0e6cb98dd78b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 77,
                "offset": 0,
                "shift": 32,
                "w": 33,
                "x": 449,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "95ea8559-a14d-4ae5-b829-11aa5ed2dc76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 77,
                "offset": 0,
                "shift": 54,
                "w": 53,
                "x": 394,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "01b3a043-8191-4ce1-af67-c30f6079b97b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 77,
                "offset": 0,
                "shift": 31,
                "w": 30,
                "x": 362,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "72bead01-0c83-4198-8b8e-17e934ac2bbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 77,
                "offset": 2,
                "shift": 35,
                "w": 31,
                "x": 329,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "21171efe-9988-4bd9-bfbe-d3f0d34d2cbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 77,
                "offset": -1,
                "shift": 33,
                "w": 33,
                "x": 294,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c014f37b-dc60-4e83-a32b-69f2a7b945ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 77,
                "offset": 4,
                "shift": 26,
                "w": 20,
                "x": 137,
                "y": 81
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "4c54dbc3-97c4-453c-b9fd-00418d234788",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 77,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 274,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "76c6e2bd-792c-4c58-86f6-f3ce9160683e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 77,
                "offset": 3,
                "shift": 26,
                "w": 18,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "fe30a59d-3dbd-4dfd-922e-69ed6d962fe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 77,
                "offset": 9,
                "shift": 48,
                "w": 30,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "f41e6cb5-1974-426b-bba7-abb2cd3f5180",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 77,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "8a872d47-8d83-4b87-947f-322b2d98e531",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 77,
                "offset": 6,
                "shift": 24,
                "w": 10,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "3850a755-65a3-45f8-a56a-42076564f6f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 77,
                "offset": 0,
                "shift": 30,
                "w": 31,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c84c605e-6bb7-40a8-a4b5-6cc2fe5d96eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 77,
                "offset": -2,
                "shift": 24,
                "w": 25,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "03bcbcc7-8fdf-4ad3-a957-3e8ddc228827",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 77,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "73b80c1d-1666-4b24-9164-876d1d6f3772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 77,
                "offset": 0,
                "shift": 24,
                "w": 25,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "bf1b4497-de48-42bb-a37b-3411214ff5af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 77,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "2ce19e37-5f5e-4730-824a-30cb7fe4b67a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 77,
                "offset": -10,
                "shift": 19,
                "w": 36,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "0733a632-92a4-40ec-a12b-b8d9d04e8fb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 77,
                "offset": 0,
                "shift": 23,
                "w": 21,
                "x": 200,
                "y": 81
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "21d5fc3a-7d0e-4502-b2ac-5cf806d146ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 77,
                "offset": 1,
                "shift": 30,
                "w": 30,
                "x": 2,
                "y": 160
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3af56267-c922-41fd-8eed-6d69e87f9ee3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 77,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 223,
                "y": 81
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "ebd9ca04-613d-49e4-a1bf-a5a771c430de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 77,
                "offset": -11,
                "shift": 19,
                "w": 28,
                "x": 305,
                "y": 160
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "300b18a0-efe4-4c9b-af58-d66ec559fb04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 77,
                "offset": -2,
                "shift": 34,
                "w": 35,
                "x": 268,
                "y": 160
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "970acb03-7fd7-44ca-a36f-96212118a917",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 77,
                "offset": 1,
                "shift": 19,
                "w": 15,
                "x": 251,
                "y": 160
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f0c0e764-2588-4392-8f8b-ff86d3f0855e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 77,
                "offset": 0,
                "shift": 42,
                "w": 41,
                "x": 208,
                "y": 160
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3b7f2a50-4929-4c7f-a399-d92c7128839c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 77,
                "offset": 0,
                "shift": 32,
                "w": 31,
                "x": 175,
                "y": 160
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "33eb80fc-2a09-492f-8a91-46af0af6e39c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 77,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 154,
                "y": 160
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "84131edc-3c7b-4969-82eb-de57bacced69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 77,
                "offset": -5,
                "shift": 26,
                "w": 28,
                "x": 124,
                "y": 160
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6ac82d21-2cc1-4783-9f32-7317306c0d26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 77,
                "offset": 0,
                "shift": 27,
                "w": 41,
                "x": 81,
                "y": 160
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0e1d4521-6139-476f-8491-0ae08a77add3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 77,
                "offset": 1,
                "shift": 19,
                "w": 20,
                "x": 59,
                "y": 160
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "13b0bbc6-9c0e-4842-acbe-92f2ed17f0b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 77,
                "offset": -1,
                "shift": 16,
                "w": 15,
                "x": 335,
                "y": 160
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "75a3d025-d27d-42f2-9a91-7ffa3e803003",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 77,
                "offset": 0,
                "shift": 22,
                "w": 23,
                "x": 34,
                "y": 160
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "2c2d2e6f-f69a-49dd-acc0-86251e1f5aef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 77,
                "offset": 0,
                "shift": 33,
                "w": 29,
                "x": 453,
                "y": 81
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0b348301-f9cc-479e-b80e-fd72b2f3a027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 77,
                "offset": 0,
                "shift": 30,
                "w": 31,
                "x": 420,
                "y": 81
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "53315fac-aea2-4faf-b6f8-2a54ac6991a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 77,
                "offset": 1,
                "shift": 39,
                "w": 40,
                "x": 378,
                "y": 81
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2a48c1ec-4b27-451e-a5de-0907e81a93d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 77,
                "offset": 0,
                "shift": 25,
                "w": 26,
                "x": 350,
                "y": 81
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "dd69073a-06bc-4a1a-a8f9-3638f9942ae3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 77,
                "offset": 0,
                "shift": 31,
                "w": 28,
                "x": 320,
                "y": 81
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "fa64292c-b440-4cad-bc5e-c25bcb9cfdd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 77,
                "offset": -3,
                "shift": 26,
                "w": 28,
                "x": 290,
                "y": 81
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "68e48b96-d0c5-4c77-acd2-74fd884d98c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 77,
                "offset": 6,
                "shift": 26,
                "w": 20,
                "x": 268,
                "y": 81
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "170470b5-69a4-4318-aecd-a75acbf5c633",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 77,
                "offset": 10,
                "shift": 24,
                "w": 4,
                "x": 262,
                "y": 81
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "8072d1fe-4d15-4dd7-94ac-6deb11331fc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 77,
                "offset": 1,
                "shift": 26,
                "w": 19,
                "x": 241,
                "y": 81
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "aa9e0a68-3684-467c-a324-51f25a366734",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 77,
                "offset": 4,
                "shift": 40,
                "w": 32,
                "x": 258,
                "y": 397
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "275eb2d3-d859-409d-9585-87de72519127",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 77,
                "offset": 9,
                "shift": 47,
                "w": 28,
                "x": 292,
                "y": 397
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 36,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}