{
    "id": "615e6bfc-c553-4a4e-a161-fca73ee97353",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_hud",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Cambria",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "d8cbe7df-768c-4fd2-a362-8326d18168b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 38,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b4c1eeef-9317-4f53-bdee-4604bcc0aec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 38,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 102,
                "y": 82
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a1c30a8d-f19d-4531-8e71-9e98aaf2bc5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 38,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 91,
                "y": 82
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b03ae88c-0b71-4dcf-aec9-f8229f7dcc3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 38,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 71,
                "y": 82
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "9e9f7c5c-a7ad-4d02-b813-63d23f5e34e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 38,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 53,
                "y": 82
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "39eb47e3-d494-4e75-8cac-63d98699be5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 38,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 25,
                "y": 82
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "6b1ac452-3747-451c-b18c-784ee36d3e8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 38,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "331a8460-15e8-474c-9afe-57690cb79b76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 38,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 502,
                "y": 42
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e31d3947-860c-4c05-9698-71f36ca45e03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 38,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 490,
                "y": 42
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "83fc0c87-6fad-4423-9fa2-8517c0188a62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 38,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 478,
                "y": 42
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "4b42438f-fa7d-47b4-942c-62b29ee1cd4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 108,
                "y": 82
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a21993a3-de87-4b54-b9aa-0ccfeb2cb9e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 461,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "971d97af-081b-4af4-b484-9477e659ea9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 38,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 437,
                "y": 42
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "f36d13da-a022-4a02-95bc-6e9adebbade5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 38,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 427,
                "y": 42
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0d490661-ffd2-4c64-9c3b-c1cc56bd3b67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 38,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 421,
                "y": 42
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "43753998-9f5c-47cf-9f10-a554fb1cc9d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 404,
                "y": 42
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e6461eee-17f1-4a37-b9d1-bc322d666a85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 388,
                "y": 42
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e0779767-da99-458a-ab9a-d76429c7cbd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 372,
                "y": 42
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "9de4ae1c-526f-45bf-b78e-a89647f34580",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 356,
                "y": 42
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "4d2e7d07-3140-4408-a341-3d47d7bfaf8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 340,
                "y": 42
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "cbfb509b-159b-42cb-a816-31882ada6f63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 322,
                "y": 42
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "dc633d80-723f-419f-a0bf-ee3c9b576e3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 445,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "63e2a4e4-27a1-4823-92a9-cd4bf7651ec2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 122,
                "y": 82
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8bf66d05-3a20-4da6-a1f8-83e2d54f861b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 139,
                "y": 82
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b6e40621-67df-4d9a-847a-6dcbf832b023",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 156,
                "y": 82
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "2e322636-c5cc-46ae-8420-1df06f9e3bc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 15,
                "y": 122
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "892a6897-54f0-405a-9980-409290fd3ab9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 38,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 9,
                "y": 122
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d1026cf0-dbbd-4f48-bb27-05b7ae47278f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 38,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ce738d8b-1e84-4686-bf69-fddc873eb633",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 493,
                "y": 82
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6c5a84f8-14dd-47cc-8e04-021f8371b524",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 476,
                "y": 82
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "de4092d0-130f-4dab-8de1-43a71cd1aca8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 459,
                "y": 82
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "62fea55f-60fc-49f8-8a5d-4bf85af5cc71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 445,
                "y": 82
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e67b139c-c79d-4c58-bbd3-1d2aae3b094a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 38,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 418,
                "y": 82
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a7fccd31-2ac4-4952-ac16-870a36943c92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 38,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 396,
                "y": 82
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e1912096-7f9f-4891-b64a-b0097bb50159",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 38,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 377,
                "y": 82
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ebe02dc3-f27e-45f0-8e48-41a2c7a108bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 359,
                "y": 82
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "dfef2169-19af-4b5b-b596-25655cc33283",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 38,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 338,
                "y": 82
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4542146a-042c-4b2a-a7f7-8d85924ee89e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 320,
                "y": 82
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "677ce15d-caa6-420e-829d-ece807c01c63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 302,
                "y": 82
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "a10be118-ff47-4cd5-ba48-3a3e79aecaae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 38,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 282,
                "y": 82
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "efb3b447-b6cb-4131-9c1b-25acf7014f39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 38,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 260,
                "y": 82
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f0b8f751-5a1b-4347-b2fc-214711796847",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 38,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 250,
                "y": 82
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "cd793af9-e64f-40f9-a91b-ee4c5ea39b17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 38,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 239,
                "y": 82
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "625504a5-0298-4fe3-be44-a2714fc2c4b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 38,
                "offset": 1,
                "shift": 20,
                "w": 20,
                "x": 217,
                "y": 82
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "647a734b-f96c-4663-bc51-2f9661959e97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 199,
                "y": 82
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "cadcc0fe-1596-4ba8-b466-d2472573bfd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 38,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 173,
                "y": 82
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "4b1e5993-f0fb-4d8c-a552-891e46ec15d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 38,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 300,
                "y": 42
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e6fcaee7-51b8-4fde-8187-735a04b0a237",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 38,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 279,
                "y": 42
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "81a7b2a7-f123-4b2e-9030-f0e702d61116",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 261,
                "y": 42
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "20a98037-36d9-4bb3-8420-9d57fb0b765e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 38,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 372,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "05a19dca-b2a1-4574-8112-ea565c8c9ea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 38,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 341,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f985b5c0-bd86-43ab-bc89-45545559ec50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 38,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 325,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1a31d061-8659-497f-b5e9-cd5ddc9b571b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 38,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 306,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "4bed4392-1b44-4b3e-913b-6d4e43577058",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 38,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 285,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "858c63e9-98e1-417f-a213-8ab11ecb68c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 263,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "59ab5aae-0342-441f-b722-e894f8c527f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 38,
                "offset": 0,
                "shift": 29,
                "w": 30,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "68eadb5a-f3e4-4781-901b-505db6b455c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "bff4cfde-ab74-47f3-9c1c-70bd655a50fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "29c1800d-0a3b-470a-b934-2cb7a573479f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b9f596ca-3700-4bb4-acd3-00dc8118a6fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 38,
                "offset": 3,
                "shift": 11,
                "w": 8,
                "x": 362,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "4dd5d0a3-c35d-4ec2-bc33-3ec8347e35a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f85b237e-fc6a-4d7d-b94a-d928c0678fbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d78cb54b-636c-4eeb-b11b-6234db9ecb8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 38,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "93ea0ce1-d88f-4ab2-873f-bc04129a5368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 38,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "52f6c674-5564-4431-83bb-dcd71d274036",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8052baf4-24ff-4945-a2db-c420f842aa69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 38,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "e935fcc0-8682-4664-8346-836bd087119b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4d56c20e-ce24-4c1c-92c7-fe5606b97b1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ea244179-97e3-4a22-ae51-7a5a13e52774",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9caf9ba8-cb1e-4a41-9aab-fec5a90d2532",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 38,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "26000b8f-b086-4441-ba39-21a910be8bd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 38,
                "offset": 1,
                "shift": 10,
                "w": 11,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c098975d-5c9e-4a6e-a42b-103df7fa0f49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 394,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "1a9953ae-b5a9-4db6-bc8f-14858d4c7c4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 66,
                "y": 42
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "48224c21-f0b2-47c2-a599-a5d67a817410",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 38,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 412,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "11695326-fc1d-4324-8702-af341d7af158",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 6,
                "x": 239,
                "y": 42
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "aba632da-f6b2-47bd-a844-09893a58817c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 220,
                "y": 42
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c6050e10-33bd-4f1e-ae62-73b18b2c21c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 210,
                "y": 42
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "7d2c8011-fa10-4cce-b385-42affd2f1d5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 38,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 183,
                "y": 42
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3fe7b8a3-d9f3-48b8-9327-4cef7be60815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 165,
                "y": 42
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "594ea067-2755-46a1-bc3f-6ec79c06ce43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 38,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 148,
                "y": 42
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b946f9a0-f064-4a99-b854-aac0f0fb4711",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 130,
                "y": 42
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6b12f05b-5c8a-435e-ad78-2e7d50925d48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 112,
                "y": 42
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ebc4be26-f19f-4998-9d18-0c6b5c5ad003",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 98,
                "y": 42
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e3b4f964-c231-45c2-98ae-af861a037397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 247,
                "y": 42
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "495700c3-bffc-4fd8-9621-6d22339596b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 84,
                "y": 42
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d76f0eb1-754f-4ebc-9451-695ba6b6cdd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 48,
                "y": 42
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "435b82fd-370e-4aff-b8e0-1acbc9cd7359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 29,
                "y": 42
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "6e9335df-951e-4a6d-83c2-db325975e509",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 38,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8257e8a9-1ba9-40ff-9d03-4c0b309ff9f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 38,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 487,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a32725f5-128a-4409-981f-e8fe77980b64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 468,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ba427e43-95d3-48d9-b098-447a4435901c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 453,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "e8d29ff6-806d-4021-aa84-a9ed915dd2ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 440,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "1106ba13-947e-4c40-98c3-32b15cb6e2c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 38,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 434,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "58222b47-33d6-4205-8d99-5341a0921b9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 38,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 421,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c71c7d44-a9e0-4df5-855c-6eb1984d3ddb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 38,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 32,
                "y": 122
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "eb70d1c3-18da-48ed-a00c-5ed754a05997",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 38,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 54,
                "y": 122
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "3451c453-2840-412b-9e5e-2f3fb546b42b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 34
        },
        {
            "id": "4094975a-cb2d-46d2-8085-4d6fb5eb76c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 39
        },
        {
            "id": "c89fabab-13eb-44b9-8a65-eaacf80079fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 42
        },
        {
            "id": "204e5216-ab63-40ef-b041-517fb5d40d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 45
        },
        {
            "id": "7c20f3dc-3ef4-4f30-b24c-2ba9f09e5e10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 67
        },
        {
            "id": "8d6d89c8-e6bc-4add-8e38-240684a665b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 71
        },
        {
            "id": "d8419cbe-60cf-4533-aa39-722a233c8d73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 79
        },
        {
            "id": "9c6cadbf-20b0-4373-9bee-2242aa11b66b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 81
        },
        {
            "id": "2c6db068-e789-433a-9bf6-286f2448e5ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "9907d9f5-6417-41b0-bf57-0ecf4f9e7963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 85
        },
        {
            "id": "49493f95-00c8-4cf4-96e9-6417f4904e40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "042b7c0f-af24-4e53-98eb-5d98fdbf2eb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "a0ce72c2-1637-4105-9279-a2e3ebc17de4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "74f96caa-60e7-4418-b091-7c3c29be6194",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "ef05b80c-82db-410b-8574-779cf7afe2df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "b8585b27-50af-4d93-9989-e4ee4c079f27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "8c0e197c-e2ea-4bae-a1d6-c021726384dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 170
        },
        {
            "id": "b42304ae-4315-496a-aab2-65bcfd01a401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 173
        },
        {
            "id": "fdc1f8b2-adf1-4ef3-9535-7f6040b2f11c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 186
        },
        {
            "id": "7dd705f7-7a39-473e-8c68-f22758a5a7bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 199
        },
        {
            "id": "89a7065e-8be1-4f14-b56d-c4bacb6a5fdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 210
        },
        {
            "id": "70f60951-1661-4bd1-9b6e-b0967aa8d6f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 211
        },
        {
            "id": "69f005f4-bca0-4ef0-ac56-912a67c3e569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 212
        },
        {
            "id": "e58db836-f639-4614-821d-535423cd2837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 213
        },
        {
            "id": "cd5d97dd-32a5-4ddc-a4f9-fc83cf134ce7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 214
        },
        {
            "id": "b3bd8e9f-2719-4636-88fd-de6dfebd77aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 216
        },
        {
            "id": "0bec8785-decc-496f-944b-8d3dfd1ed465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 217
        },
        {
            "id": "fb900ec4-2a86-4edf-8fa6-2f4dbbff0180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 218
        },
        {
            "id": "f57e59bb-4d10-41c4-b759-346bac07e5fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 219
        },
        {
            "id": "16d2a389-1366-4eb7-a154-60e9fd9e3e50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 220
        },
        {
            "id": "99a829d0-8eb3-4dad-89cc-31f501644788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 221
        },
        {
            "id": "3ec28983-d252-481d-9a0e-6aea7edb419f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 253
        },
        {
            "id": "f99dd8e9-f73e-4fc6-8aa0-a977dcf6af5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 255
        },
        {
            "id": "644cc45a-e9a2-461e-acca-dfcffd68e3d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 262
        },
        {
            "id": "372a77fd-1574-488e-864f-6ba29db49656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 264
        },
        {
            "id": "647c116d-40ac-4ceb-82e3-19c2a80620c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 266
        },
        {
            "id": "4f544178-f0db-4f3f-89d4-4e660225681c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 268
        },
        {
            "id": "bb6e4fc6-0baa-4aa4-bcfd-7e81f3d5194e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 284
        },
        {
            "id": "1ae2a410-6540-4b0f-92f0-a87ab8319079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 286
        },
        {
            "id": "64e32b56-124f-4041-9b4a-ae33742531b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 288
        },
        {
            "id": "1f871d66-5d52-411a-93e7-2ae6a105e541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 290
        },
        {
            "id": "0d1b6ec9-4552-4b5d-a9d8-6f195b3a037b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 329
        },
        {
            "id": "815013ff-5626-443e-aed4-0000ee4a72d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 332
        },
        {
            "id": "1de56021-497b-4418-8298-262898a7892a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 334
        },
        {
            "id": "1b38f070-08a0-4f9b-ae2b-912dd7ce0680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 336
        },
        {
            "id": "059c473f-cfc4-4f05-a78e-378926567c5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 338
        },
        {
            "id": "eafb7a01-53a6-42c5-a7bc-00d7347306ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 354
        },
        {
            "id": "9e5308e5-0fa2-4742-b74e-7a72b697af4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 356
        },
        {
            "id": "3868580b-873b-404d-aad8-6769b1bc95dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 358
        },
        {
            "id": "6f84f604-d010-4dc8-bf9b-53fe1cd13fb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 360
        },
        {
            "id": "a5aaeb75-db96-4f23-9707-303ffe49fa55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 362
        },
        {
            "id": "4c72bec0-19ea-4f97-a7c8-f3f5266987ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 364
        },
        {
            "id": "b6a3e752-809e-48ba-aded-177933e4e45e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 366
        },
        {
            "id": "563f60cf-41c0-4754-bc8c-a0e3c287da31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 368
        },
        {
            "id": "04dd6263-0566-447c-b524-9999bfb86ae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 370
        },
        {
            "id": "134fee26-2f75-463a-88f3-8ad64666685d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 372
        },
        {
            "id": "d9e8c366-b20c-4282-9003-f978fd470525",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 373
        },
        {
            "id": "8a51ca5c-8b39-491f-99e1-97f772c3ab39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 374
        },
        {
            "id": "8556b684-ea6d-4e88-a849-62c25369b727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 375
        },
        {
            "id": "82374a12-9e48-4963-87ad-bbb727b90724",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 376
        },
        {
            "id": "a322c557-b799-4585-8914-34bcdaf8c5db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 431
        },
        {
            "id": "249970be-4b14-4703-9c66-7bfe6ab7cedd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 510
        },
        {
            "id": "c1e5220e-f941-46e9-b2c0-3e9b0c8d5662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 538
        },
        {
            "id": "a381ec4e-ca73-430c-853c-4c25d34fe0ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7808
        },
        {
            "id": "9e5962ec-a821-4708-bb74-09396840de89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7809
        },
        {
            "id": "00496ad1-f6f4-4247-9027-a68e5bd7d508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7810
        },
        {
            "id": "a0a012b0-0445-417a-af58-1181511a5103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7811
        },
        {
            "id": "ce1d6fe5-5793-448d-93fd-209a89e8732e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7812
        },
        {
            "id": "acea5f0e-683f-4409-9981-6800b4e5f4a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7813
        },
        {
            "id": "a820c46a-fde8-43eb-9a01-97248207218e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7908
        },
        {
            "id": "795a943c-1f4b-466c-8d41-b9dba210a84f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7910
        },
        {
            "id": "7f6681c4-3f87-4e15-b5a6-0830753c8e2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7912
        },
        {
            "id": "31da094d-6937-493f-ad4f-3b624ce1124f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7914
        },
        {
            "id": "ab448851-3514-4dfc-b148-9f17118299e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7916
        },
        {
            "id": "d6cccaf6-7170-478e-80e1-b9b25ec1097b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7918
        },
        {
            "id": "d1497e2f-05a4-40e5-9454-9bb88e918ff3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7920
        },
        {
            "id": "3e18dcd2-9bdd-4265-8544-f079828e390b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7922
        },
        {
            "id": "21d9cfae-9bc5-4c9c-acbf-f293c85ea398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7923
        },
        {
            "id": "3154edc9-9b89-469d-bb3a-e3796e1413e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7924
        },
        {
            "id": "e3be2eb0-a43e-44e2-93bd-d53f6ea41846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7925
        },
        {
            "id": "16444b78-b751-4825-9077-de3bed4fafaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7926
        },
        {
            "id": "8cb87588-bf65-448f-9dbe-e0ed1f4101c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7927
        },
        {
            "id": "94f413fe-726b-45a9-95d1-a56a804bf213",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7928
        },
        {
            "id": "6ac788cd-3369-445c-81f5-373f31cfbb44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7929
        },
        {
            "id": "6da486ab-9411-41ac-af08-f0ca5eae80b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8208
        },
        {
            "id": "4993cc50-4107-4a63-ad09-2f34d27690c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8209
        },
        {
            "id": "21038a44-6db4-48ec-979c-7de00f936c8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8211
        },
        {
            "id": "667914f0-3290-48ed-bfc0-89ebe82095c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8212
        },
        {
            "id": "3dab5aae-af25-4e2b-848b-06f9a5cc75e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8213
        },
        {
            "id": "a8f49f6e-1077-4398-8528-511ad99455b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8216
        },
        {
            "id": "92fa6baa-eb59-476e-89d2-b696996e5102",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "432c2e88-89c2-4877-8ec0-c86941959da3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8220
        },
        {
            "id": "f5be8de4-7460-4180-a98b-0e9cba3f04f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8221
        },
        {
            "id": "8eb60e96-d2f8-4aa5-bab8-5bc1f3268458",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8482
        },
        {
            "id": "1b46f180-4452-46b7-a646-2f7c9e0f0b62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 89
        },
        {
            "id": "bff4333d-f5fb-4a11-941e-45a4f49873b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 198
        },
        {
            "id": "7880198f-b6de-414f-b640-79bc0469b178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 221
        },
        {
            "id": "10edb927-dc34-4fd0-8d92-7a032adfd59b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 374
        },
        {
            "id": "0eacd554-a98a-47f5-bb67-5a4ae112dd93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 376
        },
        {
            "id": "65dad847-b194-4e57-9ffc-0faabb8ef241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 508
        },
        {
            "id": "b81a942a-c6ab-4b47-ad6f-e36016dad757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7922
        },
        {
            "id": "6e41730d-d97c-48fb-b9b9-e74d32d2551b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7924
        },
        {
            "id": "a71ae393-0968-4a33-958e-599339940bc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7926
        },
        {
            "id": "e18289a3-ccde-4f6b-9e9d-99719d76aa0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7928
        },
        {
            "id": "c5d0ba8c-e21e-47e9-a9e8-b8936bef51b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 44
        },
        {
            "id": "00ebdc70-e099-4078-98b7-08a5f72182cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 46
        },
        {
            "id": "6bf43395-f0de-4248-8fa9-6c2b0fd7447e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 65
        },
        {
            "id": "a82d9e9d-7183-4e42-8c45-f7f2e002caeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 86
        },
        {
            "id": "90cb75dd-5843-4833-aeaf-b45162884509",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "1d564af7-e6cb-416e-85d4-4e9654089d76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 89
        },
        {
            "id": "929871c7-4d38-4b3c-995f-cca3a3749e8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 90
        },
        {
            "id": "48273d18-aa10-4190-b115-6b27d8764b84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 192
        },
        {
            "id": "d70ea576-848f-424c-825f-af006cf3116f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 193
        },
        {
            "id": "fec21c13-75ab-4da7-8317-97e11c9eec0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 194
        },
        {
            "id": "24eec9a7-79cb-4c0c-b4cc-77762043c945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 195
        },
        {
            "id": "8e5db8cb-0cba-4936-bdf5-d1d302e9b831",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 196
        },
        {
            "id": "4a17cf94-3ece-41a8-80b4-6d21b2608bc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 197
        },
        {
            "id": "0e7a1985-22b2-4eef-b19b-1eca6ad85f91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 198
        },
        {
            "id": "b25e87d9-0145-4d78-95c6-998593888c41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 221
        },
        {
            "id": "58a1fb7e-0b23-4f88-a0ff-71ec3b85a3bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 256
        },
        {
            "id": "e57f62ac-12a4-4056-a77a-30965e66c410",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 258
        },
        {
            "id": "de66ec0d-7c06-455b-8f39-4461c04d1e9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 260
        },
        {
            "id": "5f19cc24-f92a-4fe6-99ca-62440d9a65dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 374
        },
        {
            "id": "136e22ee-07fb-4ef5-a040-fd4e5131413f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 376
        },
        {
            "id": "94ca1cd6-7007-4e89-bafc-3435cfe9bfad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 377
        },
        {
            "id": "17320f86-09f4-42ce-9a57-5f4d427e5281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 379
        },
        {
            "id": "bdca9d85-0dc4-405a-9f74-bae8d9db4322",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 381
        },
        {
            "id": "3bc3a358-56f2-4bc6-961b-a30dfbb6f3ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 506
        },
        {
            "id": "fd3f5396-e997-46b0-8407-47d93823b89b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 508
        },
        {
            "id": "e6124ca3-f1a1-4df6-a60b-b15bb281b393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7840
        },
        {
            "id": "13d0f0b1-b6d5-4cd8-96b3-33286785aeb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7842
        },
        {
            "id": "994096a7-43a0-489a-864e-f95a84c0e151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7844
        },
        {
            "id": "a3e513a0-da2b-4a8f-93a9-d6415ab98f34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7846
        },
        {
            "id": "1d02c1ad-0925-4d8a-895a-051c652dbe26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7848
        },
        {
            "id": "1a319db4-2342-402f-9883-4220ba7aa95d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7850
        },
        {
            "id": "925d2b64-f986-4ece-b222-308b14b25156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7852
        },
        {
            "id": "e6ecbfa7-3ef6-4807-a355-d234432f515d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7854
        },
        {
            "id": "3ff6ae4f-18df-4fea-b7fa-f76a51624e11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7856
        },
        {
            "id": "addd3a89-2e8d-428b-969d-f84e4baa00ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7858
        },
        {
            "id": "23b2df5c-0364-4873-b3fc-bc8d991889eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7860
        },
        {
            "id": "5c05d16c-b55d-4a5e-969a-7173a4ef2d69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7862
        },
        {
            "id": "455e522d-cd94-4216-82e3-521636c5b27a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7922
        },
        {
            "id": "221f3719-302b-46cb-875a-9a3830c443bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7924
        },
        {
            "id": "ac954caf-dcc0-4d27-9061-561863dfd6fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7926
        },
        {
            "id": "3ea0bb19-84b8-416c-b415-86891238bdfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7928
        },
        {
            "id": "f9caa373-4fb9-49e3-8f3c-f3f143e5bb26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 8218
        },
        {
            "id": "e739402b-4ef2-4571-9b25-76a0fea71818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 8222
        },
        {
            "id": "d6b3b905-b3f4-410c-b0c8-90b813035d61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 8230
        },
        {
            "id": "fe262500-8111-4e52-a081-2874db325025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 118
        },
        {
            "id": "c74b2ab7-5ac2-4868-b8d3-2297810debd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 121
        },
        {
            "id": "73ee060c-19f9-4ba0-8f48-fa8082cf9585",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 198
        },
        {
            "id": "2091c207-76f0-4e3a-8553-6340fafb8cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 253
        },
        {
            "id": "10df7f56-5b51-4780-a425-1c229b9838d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 255
        },
        {
            "id": "428090d3-0557-4ba6-9284-1497b020aa57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 375
        },
        {
            "id": "281bca01-91fc-432a-a30f-2340c10fdeeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 508
        },
        {
            "id": "ba0ee433-c208-417e-b2da-6873b38f0861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7923
        },
        {
            "id": "89b98a84-2a0b-435b-8b0c-4d130d3ed9df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7925
        },
        {
            "id": "91e5573a-54a4-46dc-b71e-f486d09d1f2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7927
        },
        {
            "id": "f9690e59-7169-4324-b552-77a1b6b1411b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7929
        },
        {
            "id": "59e44b8e-246c-4b2c-a479-b012b7c1cd5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "e36c3631-b21f-4268-8ef5-a077d398e773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 45
        },
        {
            "id": "4b916cb7-8481-494a-9634-2ddaa832882d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "6d9d2578-e218-476f-a352-a3cad2aa7457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 58
        },
        {
            "id": "68ddec86-710a-47b4-add1-3a0adddd65a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 59
        },
        {
            "id": "b466ba16-143e-437d-868c-1789f00e0c6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "d93be4d3-027e-42d0-8270-a1f2da67aedf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "da33a9c2-9533-4ec6-8a65-a2e9dab0f0e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 99
        },
        {
            "id": "a4bf448d-aaf6-4de0-9716-067c2b624ede",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 100
        },
        {
            "id": "62a954a9-ccf4-4da5-9a78-b94936863112",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 101
        },
        {
            "id": "140881ac-e298-417b-b42f-ef159f5503f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 103
        },
        {
            "id": "e9a5efdb-769e-4f05-a2d6-68120eca9788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 109
        },
        {
            "id": "e2dbd7d7-4561-46ad-a1dc-0cd9323f3984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 110
        },
        {
            "id": "3a327227-1b4c-4984-ba61-aee8dd3b417d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 111
        },
        {
            "id": "4b93ac23-bf31-4b12-8404-987d2c30b046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 112
        },
        {
            "id": "99a4d566-97ff-481b-a362-aca4b4c17e29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 113
        },
        {
            "id": "ddde52c0-3a00-48ca-850e-ee43a21fb59a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 114
        },
        {
            "id": "c190cb20-ee5e-45b5-93b0-6a1acb807f39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 115
        },
        {
            "id": "6de7c905-a981-45c4-812a-cdda542e3ed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 120
        },
        {
            "id": "f7f87eb4-8c5e-42e6-827c-9694a5287831",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 122
        },
        {
            "id": "191922de-01e8-4cab-8d48-bd36886b6bc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 171
        },
        {
            "id": "58d5e403-a37f-44a6-9ac4-0697eaf16a45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 173
        },
        {
            "id": "6c37f64a-9e0a-4e3f-9bf9-f7f4ade93546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 187
        },
        {
            "id": "b248c15e-fa1a-48aa-8740-04a76b0be043",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 192
        },
        {
            "id": "922220a0-174a-47c8-91dd-0755d45395bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 193
        },
        {
            "id": "0af370d5-5b75-49e3-b680-fb6b8edc9575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 194
        },
        {
            "id": "cbaa7004-0f63-4634-8d93-8bd0da4c7ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 195
        },
        {
            "id": "3f4831f2-ebb4-49ba-a2e0-1d1cdc552478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 196
        },
        {
            "id": "39a01f73-e4df-45c4-b8ea-ffbc3a7bbfd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 197
        },
        {
            "id": "66200eb5-00bc-4c25-ab40-be48f680fd15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 198
        },
        {
            "id": "84458fc8-6df8-4802-ba8c-6f83f4a39371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 224
        },
        {
            "id": "a5b46d08-3623-4d63-99e6-a30413c4c69e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 225
        },
        {
            "id": "e77bc7a6-d317-4e46-a2a7-19ec4e436727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 226
        },
        {
            "id": "316d3287-aa40-4f6a-86ff-32ddc325d6a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 227
        },
        {
            "id": "21e5496d-0b46-46ed-ace2-5e98950d6be2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 228
        },
        {
            "id": "d474ecc0-9bb3-4036-9d1e-d6660787b739",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 229
        },
        {
            "id": "c08a3899-ad9c-48aa-b840-abe2f753352f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 230
        },
        {
            "id": "752115bd-5138-4507-b728-c3c252036a60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 231
        },
        {
            "id": "9f6e02d4-7838-4346-b405-a1f9d8c6b89a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 232
        },
        {
            "id": "29b275f2-0ba7-4e34-81c9-2d70f78a2e6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 233
        },
        {
            "id": "27dade39-644a-4e37-988b-da1f3e75c6ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 234
        },
        {
            "id": "eb2c11d1-36da-4689-95a7-d3aa52f76162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 235
        },
        {
            "id": "28f2f5e3-48ce-424b-a915-d7f94e638d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 240
        },
        {
            "id": "3cbf8878-7817-4648-b8a0-1b167ceec35e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 241
        },
        {
            "id": "1b34a52e-357b-4c9f-ae71-ade5449a505b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 242
        },
        {
            "id": "1d5b7fb6-75d5-4a56-a4d9-0bf1c47e4865",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 243
        },
        {
            "id": "8c030f74-934f-455c-a607-e1b7f5b601ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 244
        },
        {
            "id": "af64f581-0f82-47df-9d38-7b3115699bff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 245
        },
        {
            "id": "06ba05b6-4502-43d7-a0ff-107b96a2aee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 246
        },
        {
            "id": "b516b7b1-47bf-44bf-86b7-fc8dc79700bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 248
        },
        {
            "id": "6cf9d45d-144a-4156-97ab-3137122193c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 256
        },
        {
            "id": "59f68bf5-a7e3-45d3-be17-cc45e2791d37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 257
        },
        {
            "id": "58b7ec7c-d4f2-46f0-92fe-cafdee485bf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 258
        },
        {
            "id": "65e85332-c1e4-4dc6-8788-43c7be48cc45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 259
        },
        {
            "id": "1fe40907-9e2e-4fe7-a93e-f913d41bbce4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 260
        },
        {
            "id": "a85f4f15-4efa-47c1-994d-06810e7c6ff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 261
        },
        {
            "id": "18209545-635e-44fd-83c3-6e7137bcbf6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 263
        },
        {
            "id": "9849c740-f0db-4d1d-8803-d16375640a0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 265
        },
        {
            "id": "30dc446b-7181-4e79-8c4e-69972aa7d4ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 267
        },
        {
            "id": "17acf37c-c99e-4c2f-a090-551524eb0543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 269
        },
        {
            "id": "40194e39-382e-41ed-a4d0-ad2cea2f8c6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 271
        },
        {
            "id": "617e9a1f-ecb8-4baa-8172-1e3e5d6bfd93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 273
        },
        {
            "id": "0e19a84b-845b-4e0c-a852-192358073abc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 275
        },
        {
            "id": "56b21800-3be4-448b-967c-3aff158621ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 277
        },
        {
            "id": "8bc0d484-a7da-4fa3-b771-f237421e725e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 279
        },
        {
            "id": "eab568ff-28bd-451a-a9e7-11213b8cab12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 281
        },
        {
            "id": "484a98f4-cf55-4ead-8ed3-06732a530628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 283
        },
        {
            "id": "c191328c-7841-48a7-9746-69ee84352c9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 285
        },
        {
            "id": "f66eb0bf-df1b-415f-9336-547831fde963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 287
        },
        {
            "id": "a187d713-f184-4d2d-8695-55e1ca3cb90e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 289
        },
        {
            "id": "dbadc86a-ff75-4128-9926-32972ac67e24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 291
        },
        {
            "id": "be8f7d97-985e-4a35-94a2-ee659f0a50d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 305
        },
        {
            "id": "11b540e6-fe9f-4d4b-8d09-5bf9be7acb55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 312
        },
        {
            "id": "8f963650-1a24-42b7-bfae-ced7fbcbcde1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 324
        },
        {
            "id": "c7ee1856-74d1-430e-adef-0f97a5fe01d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 326
        },
        {
            "id": "3bf052b6-6487-44a7-95a2-f97960cae9c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 328
        },
        {
            "id": "f9cbf716-220a-4066-8596-ea698d0c9a64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 331
        },
        {
            "id": "c7c58180-77db-405d-a8c8-04230a80cac5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 333
        },
        {
            "id": "91c2172f-03ba-4c1f-b490-cd28316a1432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 335
        },
        {
            "id": "82e67152-7e8e-4bdf-aa5f-b5a73d38bfa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 337
        },
        {
            "id": "0b59e352-94b3-4972-ad3a-f80359af2241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 339
        },
        {
            "id": "7cecb962-8873-4104-8991-b4d18ffc393b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 341
        },
        {
            "id": "9ff1f193-96db-472a-8fdd-f7d5267f9a7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 343
        },
        {
            "id": "1fb6de6a-cc49-4d8a-8c2a-d56609fa59a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 347
        },
        {
            "id": "4e67d6b6-4b90-4086-bc19-6592e5005589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 351
        },
        {
            "id": "ba310afb-37e5-4e71-b264-cd5572d1e6e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 378
        },
        {
            "id": "71493033-e67e-451c-9b92-75ac4511b165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 380
        },
        {
            "id": "1224a2dc-74c1-4ae3-989d-5deb0d4e0123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 382
        },
        {
            "id": "14dfd94a-c65f-40b3-b0df-b74023bf3a36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 417
        },
        {
            "id": "682e0281-d2e1-4815-aac2-4daf388953f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 506
        },
        {
            "id": "7673c947-ae7a-4947-83cf-4b68c1ed33d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 507
        },
        {
            "id": "94560af6-e70a-4b6a-a49a-721780e6195b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 508
        },
        {
            "id": "4d9f9f02-6049-4ddc-9566-a23f1430ea9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 509
        },
        {
            "id": "a69a5dd8-1ebc-479a-9def-55e8f109c018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 511
        },
        {
            "id": "d9d9c05d-b44e-410c-981e-3fee108591ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 537
        },
        {
            "id": "6b9636fe-69b6-402c-9988-4a67f2606540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7840
        },
        {
            "id": "809d241d-6dca-4ea3-b1ac-68c06bba491b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7841
        },
        {
            "id": "0ca91160-8e0c-47ac-913b-c644f110819c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7842
        },
        {
            "id": "ebfab102-724f-4585-b68c-66f4690552d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7843
        },
        {
            "id": "b9ecaae5-0f18-46e8-9d4f-f659c02881be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7844
        },
        {
            "id": "6c7a7b71-2b5f-408b-90a4-d4ce2e8cb9f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7845
        },
        {
            "id": "e936680c-1255-48c6-bd34-0fc4578ff14f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7846
        },
        {
            "id": "01b0a905-a384-4aa9-8554-04626427d4f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7847
        },
        {
            "id": "5c546eed-fa12-4280-99f2-86b36267c3b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7848
        },
        {
            "id": "209fda46-0e83-49b1-903b-1cfed85ef7b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7849
        },
        {
            "id": "99ad434d-e079-485b-a0fa-8ba661d8df71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7850
        },
        {
            "id": "b368b5a4-5121-4840-baf8-b21aa4d1555f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7851
        },
        {
            "id": "2d82e194-37df-47ff-a3e2-52313815dd64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7852
        },
        {
            "id": "4c7c1f6e-bc0a-4179-82ca-1cff2da254e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7853
        },
        {
            "id": "f92dcd98-3afe-4aa3-94d8-ecacc8851eef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7854
        },
        {
            "id": "a9c5e7d7-a41a-4854-9016-bdc9ad376447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7855
        },
        {
            "id": "808539c5-6fba-4309-a0c9-8f12fa2747ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7856
        },
        {
            "id": "d8bf96fc-01e2-4c84-b48b-592f9ab1c7c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7857
        },
        {
            "id": "8825d314-e692-43d6-99cc-4e2c06a1cb74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7858
        },
        {
            "id": "4cc6ad51-66fc-4461-9e30-185608616ca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7859
        },
        {
            "id": "939f4f42-86a7-4d41-951e-069928bed5ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7860
        },
        {
            "id": "d6589613-4d9b-445b-9075-910516c4ee29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7861
        },
        {
            "id": "d87bcf64-9bde-4f96-9648-e73b5694faf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7862
        },
        {
            "id": "b5502f1c-fb8d-48f0-bf63-5c0174564a82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7863
        },
        {
            "id": "2cca1c16-f65a-4914-9ba0-08b8614d2c04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7865
        },
        {
            "id": "2c83e074-b779-4719-a760-7380843f242a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7867
        },
        {
            "id": "256addbf-d463-4849-8b20-8b2dbab674f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7869
        },
        {
            "id": "e713b057-428e-4ae9-8c08-ec297d2a2af6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7871
        },
        {
            "id": "1178e522-8b82-4fcb-85ea-7ca8587f88ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7873
        },
        {
            "id": "13c1afb1-44a3-467d-bcaf-88f2d61d75cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7875
        },
        {
            "id": "16429d03-bec1-49ef-b5d5-1d3cd5915f96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7877
        },
        {
            "id": "4f0c6e51-ea95-484a-ba5c-1d11d4121d31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7879
        },
        {
            "id": "f13db0d1-8846-4c05-9cce-e6a33cca10e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7885
        },
        {
            "id": "1be0263f-212a-451e-a7a7-0d8130e46a3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7887
        },
        {
            "id": "7ebd004a-036a-4a83-b9b4-0506f8278740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7889
        },
        {
            "id": "f34307d4-2b85-49c9-88e9-d535e7666d6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7891
        },
        {
            "id": "66b96950-44f1-4263-a9ab-207dfff5de29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7893
        },
        {
            "id": "48577652-077c-4bb6-94f6-980b5dc8f019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7895
        },
        {
            "id": "2e6e624a-75a1-4b34-af6f-88d251881d51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7897
        },
        {
            "id": "fa5ed40c-8733-471a-be4a-0e398cd0de75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7899
        },
        {
            "id": "c2e06196-a321-4589-8ea8-fa618abe5805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7901
        },
        {
            "id": "c3388560-dd76-45ae-966b-b87a28ab5745",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7903
        },
        {
            "id": "e5c0a771-3e98-4faf-9f21-6141ee02f74d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7905
        },
        {
            "id": "f773181b-f89a-42e6-9ca4-082e35c9dcfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7907
        },
        {
            "id": "9f65dca9-7211-4f14-b6db-10af52c35bb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8208
        },
        {
            "id": "726a1bcb-97c8-45dd-822b-41699939e202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8209
        },
        {
            "id": "c410b497-7e03-4983-bd72-9e71bf1f6b5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8211
        },
        {
            "id": "2e7fb367-8276-4a57-981c-d31be75f02a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8212
        },
        {
            "id": "7c8a2a4b-7f29-4267-9b59-33f70670747c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8213
        },
        {
            "id": "70a769b1-19e1-4f56-8da2-4178a161fe1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 8218
        },
        {
            "id": "54b123a3-fea6-481b-9fdd-7e4b97d97541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 8222
        },
        {
            "id": "1e7c0cf8-5e56-4933-866a-708f6dcef7b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 8230
        },
        {
            "id": "1e7f23dc-53a0-468d-9f79-8274595802a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8249
        },
        {
            "id": "8723429c-613c-4663-add3-aa8bd74bb5b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8250
        },
        {
            "id": "a3bf534e-8a55-448f-a8fe-83cc31576599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 89
        },
        {
            "id": "4fad567b-aafd-4a51-aa0a-e69aefffc759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 221
        },
        {
            "id": "3a6c05c7-a4d2-44a0-b4b8-4c7aa3ce30d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 374
        },
        {
            "id": "74af2300-c7ee-4e2f-b642-ccb59368c5d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 376
        },
        {
            "id": "f72eda75-d6c7-48c5-89f4-8bbaadf74843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7922
        },
        {
            "id": "b32994f7-b55d-41aa-b89d-5d9dcedec0bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7924
        },
        {
            "id": "85bd5d41-2b9f-4763-b5fa-7f1263d6d8d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7926
        },
        {
            "id": "b3a626d0-988f-4524-8cdc-81131be71951",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7928
        },
        {
            "id": "0b2c2a3f-4e6b-4faa-b0c9-1b7303b278d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 65
        },
        {
            "id": "68614ebd-1db7-4f2e-9a81-319fd2f2e615",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 192
        },
        {
            "id": "d7a96630-a396-4ad9-b94e-f13936b003d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 193
        },
        {
            "id": "841a3485-2cc1-453b-a351-1cfcca77c172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 194
        },
        {
            "id": "43003d18-b8ba-4a7d-a941-1ce6e1b10d2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 195
        },
        {
            "id": "67b3ed09-450e-47af-b97e-d32a056a8120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 196
        },
        {
            "id": "76aef742-a1a7-4552-b5d1-77c766fd52bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 197
        },
        {
            "id": "4acf3b74-fef9-404f-b623-c19f75d6f37e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 198
        },
        {
            "id": "f9e4b492-4967-4751-9649-bbbed4e77b5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 256
        },
        {
            "id": "4c1c1f3e-23f7-410a-96b8-7d31ade44a89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 258
        },
        {
            "id": "82f19301-da38-49e8-8bee-2f3341e4573c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 260
        },
        {
            "id": "441c17bb-41d3-4eaa-b7c2-b3aa720c4438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 506
        },
        {
            "id": "e315506e-07b2-4645-89f2-b900f26dbb7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 508
        },
        {
            "id": "a26297df-3c51-4c8b-b81b-8978c43f8ac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7840
        },
        {
            "id": "0f342c6a-d552-418d-8b68-b86514c191aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7842
        },
        {
            "id": "8616e7b2-b3e2-4a82-942e-6c92d53b4fd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7844
        },
        {
            "id": "9179e048-7e0c-48bf-b74f-82e866db9e28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7846
        },
        {
            "id": "e615bc36-0910-4f15-bbe5-44865f05e3c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7848
        },
        {
            "id": "e96b465c-f5ab-4ac7-ac2b-42954e8c72d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7850
        },
        {
            "id": "d8b467ad-186b-4c83-a66d-ae88d59ef559",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7852
        },
        {
            "id": "22ca8fdb-d7ee-4692-afc1-7e21ee95fffe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7854
        },
        {
            "id": "bd45eccd-52de-4ad8-9a51-67527e6ec93a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7856
        },
        {
            "id": "243007cc-644c-4878-bdb1-0d2ee41c3ef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7858
        },
        {
            "id": "5f57920f-799b-415d-bb61-18bf656768ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7860
        },
        {
            "id": "cbb2366b-987a-45a7-821b-9c3f65a59f65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7862
        },
        {
            "id": "61cdc25d-48b8-458c-b351-44320afe5fb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 45
        },
        {
            "id": "72c51e10-6ffb-468f-8a0b-9c6d25bad9a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "d0722336-e488-472e-8039-2d170b6fdc03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "2e03aa44-deea-416c-9d31-ba336a6b215d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "bd74caaf-67f0-4f8a-be47-a00563dc5030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "23fa0239-a866-41e1-8709-d2749524a9c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 99
        },
        {
            "id": "527ca22b-9379-4289-92a7-8030deec6d77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 100
        },
        {
            "id": "17b7876a-cc6b-4a28-a33c-84f68f5ccc49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 101
        },
        {
            "id": "761ee729-015f-4788-a322-60409bcb9401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 111
        },
        {
            "id": "edbf6849-04c2-4ec5-8f67-0d1995bb33da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 113
        },
        {
            "id": "01072825-0913-446e-8ecc-6c2341129dea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 116
        },
        {
            "id": "e56a6fcf-5a22-4cb4-8228-e1dc0bd22e4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 117
        },
        {
            "id": "31bc7101-ff28-4df1-a231-ae9f55ab2865",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 118
        },
        {
            "id": "d5e909a5-3a65-4cc3-9e1a-f50cbaef9a5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 119
        },
        {
            "id": "5682eaaa-2ffd-4ffb-8f0d-9a5e32d09b28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 121
        },
        {
            "id": "d793c473-c791-4983-9c60-c1734c6e3cb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 171
        },
        {
            "id": "9cf1b358-2a0c-447e-8d4e-56ade9a6cd2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 173
        },
        {
            "id": "04d5c1cd-3998-47a3-b1d2-b231b5ab8e19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 199
        },
        {
            "id": "6f262b7f-49fc-4f2f-8f53-36829d9c459e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 210
        },
        {
            "id": "ce55b356-489c-43c6-98b5-f9a9a18773af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 211
        },
        {
            "id": "31c251f7-c3b9-473d-ba9a-5e0a61554820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 212
        },
        {
            "id": "3b8edec8-f9f8-47a5-b04d-c96f88e3a6fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 213
        },
        {
            "id": "99b4fb44-c838-411a-9b1f-37361f16f050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 214
        },
        {
            "id": "b7ca8dd7-7587-4037-94fd-018d4d42d927",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 216
        },
        {
            "id": "1b0bc749-6094-4556-857e-0eb4110b68f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 231
        },
        {
            "id": "467da05e-8ae9-4843-a4fa-b8820fb1947a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 232
        },
        {
            "id": "7893bd9c-b8f7-487e-8640-603bbce8283e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 233
        },
        {
            "id": "dcad05a1-2a9b-4746-8031-4188bc96ae4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 234
        },
        {
            "id": "7ae34f0d-8c41-4611-9c99-942faa49437c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 235
        },
        {
            "id": "9615a66b-d3b4-4ac4-91bf-55f822275812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 240
        },
        {
            "id": "4152e877-bfa4-421a-9afc-3b0662bda72c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 242
        },
        {
            "id": "55418fa2-f253-4423-8a56-79623591181a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 243
        },
        {
            "id": "800456b1-9a5f-426a-a9da-b4171fe33874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 244
        },
        {
            "id": "7da66847-df81-403a-bf67-1994fe5e672b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 245
        },
        {
            "id": "9d7bf679-56b4-440b-9b45-d42280dee0fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 246
        },
        {
            "id": "7a255cbd-11af-4cac-8164-3c7bfc87442d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 248
        },
        {
            "id": "977d6853-ddd3-45d8-9f01-9f400c045ddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 249
        },
        {
            "id": "7fe62cff-85a8-493c-878b-08ee118de321",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 250
        },
        {
            "id": "1cab765f-c35f-44c5-993f-06e230ee8774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 251
        },
        {
            "id": "02c1d5f2-9a20-489f-8593-bcd856bc3350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 252
        },
        {
            "id": "1cd27c15-8960-42ca-b251-61b1a79b5897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 253
        },
        {
            "id": "e01e266c-44b9-450a-ab05-911412f25c07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 255
        },
        {
            "id": "d645b478-4d79-4a21-bfca-9623de271c84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 262
        },
        {
            "id": "4ac7e4cb-1e01-4a92-bb4d-cdc8817b1c7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 263
        },
        {
            "id": "14452804-21f1-441e-8f85-ec9712f22ee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 264
        },
        {
            "id": "23b4b07c-9376-49bb-87af-d344df624f1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 265
        },
        {
            "id": "ef60ccbf-4a27-4a7d-9e51-cce762572d6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 266
        },
        {
            "id": "f02818d3-3e01-4387-b62d-efa4b515287e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 267
        },
        {
            "id": "037eaa42-ac30-49cf-89b7-c9114c8f8be9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 268
        },
        {
            "id": "fe8fdc30-fe6a-41ca-94d8-628b860b6042",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 269
        },
        {
            "id": "2245a30e-f918-4a03-b244-e129c9ad9fec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 271
        },
        {
            "id": "5cb0ea68-0795-44d3-865f-06ccfda9b205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 273
        },
        {
            "id": "cf971065-162c-448f-bfdd-519f1c98531d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 275
        },
        {
            "id": "41a71de1-270c-4b6c-a3a9-eb6429fa8f54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 277
        },
        {
            "id": "caab099d-9e14-4b55-bb7b-9ed43451f175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 279
        },
        {
            "id": "54ce8438-d55f-4776-b946-3a53ca6718a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 281
        },
        {
            "id": "0799d1a7-4e41-4944-9648-d8f2c7a0cf2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 283
        },
        {
            "id": "0e67e336-3095-4211-ae1c-0cd405eca951",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 284
        },
        {
            "id": "7e0a2854-1ea0-4f76-a5e2-be587dde51ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 286
        },
        {
            "id": "7a79cd76-886f-49f2-85d0-ae7b09526e03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 288
        },
        {
            "id": "7ed57c3f-5b3b-4e54-a39e-b9db594f2d0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 290
        },
        {
            "id": "9f9ed7dc-d7ce-42cc-b3be-af70dfb92a24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 332
        },
        {
            "id": "500ffcfd-b4f9-4e2d-8d19-4fcdbccc6b83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 333
        },
        {
            "id": "4bd1275f-f638-41d8-bcca-95239b5102d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 334
        },
        {
            "id": "db4ee273-85a8-49d6-b666-98d01c5e8de9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 335
        },
        {
            "id": "680bb12c-c735-4f5f-b7b6-c24bfd31163d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 336
        },
        {
            "id": "e945d235-9d39-48bb-98da-00804ca90837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 337
        },
        {
            "id": "132d0cb1-1d0d-4dbf-96eb-4d0a6eadd66d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 338
        },
        {
            "id": "b622d478-08c4-4267-894a-4ea97c4ffc1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 339
        },
        {
            "id": "6efbbaf8-893f-4739-a240-990150c99805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 355
        },
        {
            "id": "39b5c51e-417a-445c-8e04-4692574743d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 357
        },
        {
            "id": "7104feac-4a4d-4580-8890-5cb3109c5c6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 361
        },
        {
            "id": "a513725b-1a4a-4200-8e7a-54360f90095b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 363
        },
        {
            "id": "0b890a0a-2fb1-4bb9-b9d7-533be0cbfb01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 365
        },
        {
            "id": "a9a557b1-18ef-405a-ad58-4ed4ecae4b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 367
        },
        {
            "id": "31b99715-0c2a-462c-be9c-fefa52816a5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 369
        },
        {
            "id": "4148ce8e-b160-4211-b506-ce1f779c1d90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 371
        },
        {
            "id": "e773f3d5-92e7-40a1-927b-4d65b178d367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 373
        },
        {
            "id": "1dd41312-3b40-4027-bd6a-81b63218677f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 375
        },
        {
            "id": "f2551e2d-c8b4-4e35-a26c-0bb690681f67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 417
        },
        {
            "id": "01ab72c6-9d41-42cf-814b-73facc3cb6cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 432
        },
        {
            "id": "818ed089-3608-491e-bb72-423771ba72db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 510
        },
        {
            "id": "4b5c91c6-4db2-4bc2-a4bb-5e9a4d4f33be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 511
        },
        {
            "id": "be66048e-e3fa-469b-90e3-89297feb0fb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 539
        },
        {
            "id": "49149b14-003a-4773-8d82-34a40858c6a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7809
        },
        {
            "id": "cc8ccbc8-1daa-4f2d-8a73-0fa0579c0303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7811
        },
        {
            "id": "afb99561-8458-4ceb-824a-049e3e60038a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7813
        },
        {
            "id": "e2482d15-b40c-4178-a206-2942be52be57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7865
        },
        {
            "id": "5ece6710-9ce2-4a93-9091-2c833105e0b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7867
        },
        {
            "id": "9fc3eb3f-891a-4fb9-b657-4ce338524b4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7869
        },
        {
            "id": "bfa4f1c6-d098-4ad4-a6e5-919376cd6e27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7871
        },
        {
            "id": "33447cd0-a780-4444-b303-2fa8afa40906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7873
        },
        {
            "id": "8745d694-e7c6-49e5-8f93-6bc36322fe46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7875
        },
        {
            "id": "d7799bdd-7884-43ab-a7ad-442b4062bd00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7877
        },
        {
            "id": "c250e627-46ce-4b56-9927-8fbbf031ad8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7879
        },
        {
            "id": "f0e02a45-5575-4fd8-8bba-cc2061a9ab3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7885
        },
        {
            "id": "0e92b6fb-9fcd-4c88-8662-29596665f145",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7887
        },
        {
            "id": "b6c068e0-018e-4ecd-95ac-985bd6f5a0fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7889
        },
        {
            "id": "c69b0fee-c05b-491a-a397-40006d006f00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7891
        },
        {
            "id": "c9b91b90-fd76-46e2-87ff-11669706fbcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7893
        },
        {
            "id": "9e961b21-9077-4805-a429-178330c77108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7895
        },
        {
            "id": "1b4afaa2-c5bc-40fa-83c2-1831c63b28d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7897
        },
        {
            "id": "9748ce40-4f32-4497-a170-75c60c178a21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7899
        },
        {
            "id": "32123ca2-d219-49cb-bdcc-a883718995d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7901
        },
        {
            "id": "b6d89123-eae3-4a45-8afa-4f900d955280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7903
        },
        {
            "id": "8229b281-c8b4-4d84-83ca-8517163d29d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7905
        },
        {
            "id": "7ba064f8-0b99-4aea-9aba-de730b64cfc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7907
        },
        {
            "id": "c6a06c31-4917-4370-b0b8-3940d26c3294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7909
        },
        {
            "id": "466e1a70-40c9-4055-bef3-572ad0797289",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7911
        },
        {
            "id": "e9c3db91-83a8-451c-8111-96739667550c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7913
        },
        {
            "id": "19c39d6f-8df6-486e-a303-4767a8e398d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7915
        },
        {
            "id": "dff62171-6898-44b8-a502-ecd26f78620c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7917
        },
        {
            "id": "6e7479b6-91b9-4e7b-ba4c-907e44af909b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7919
        },
        {
            "id": "9e2cfcaa-f958-438f-92ab-acc084fb2b6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7923
        },
        {
            "id": "506e898b-06c3-4d2f-ac27-6cb32ef68132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7925
        },
        {
            "id": "3b1eac2b-6f3f-451d-b5eb-d971b472365d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7927
        },
        {
            "id": "6c463c86-9bcc-4a0f-b1e5-efb922a8f064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7929
        },
        {
            "id": "762604b5-3ab2-4ca5-8e78-6c80485566f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8208
        },
        {
            "id": "188188c7-9363-496f-8b3a-b13d12f21222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8209
        },
        {
            "id": "9a47ad1d-635f-4842-ac0d-0d0e1ee435db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8211
        },
        {
            "id": "bd9a1b49-1be9-41aa-a1fb-ec452f9217a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8212
        },
        {
            "id": "fbc91faf-2cf7-404d-b67b-5d91c77a295d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8213
        },
        {
            "id": "c54e6d98-e1af-4c67-a95c-a6e1c5a5eab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8249
        },
        {
            "id": "e41aec73-5d07-40e9-b72a-ef694469e08b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 34
        },
        {
            "id": "90b4e856-90f5-4f22-86f1-be151fa0664b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 39
        },
        {
            "id": "69737e9e-fdc8-44de-84b5-07a3b8a85c5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 42
        },
        {
            "id": "f2c707a7-a13d-4cd0-84d5-9e450419d83c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 84
        },
        {
            "id": "a58ee6d4-54eb-4cc2-b8db-84a8029581cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 85
        },
        {
            "id": "783f6cd8-e9e0-48ab-aa30-e4cfffdab526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 86
        },
        {
            "id": "e06134ee-42cc-4e39-aab0-98a21c32a6f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "5d36e20c-8658-478e-8f7f-901daafbae83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 89
        },
        {
            "id": "3a255ebc-7cdd-4bed-b6ba-0f34473fb84e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "8a1b6c3b-ec92-4efa-bf64-c33463235e35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "4ecfb3d8-e121-4147-9cb9-fdcca2958e4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "d5c1175d-7460-44a4-b0a9-76b4cc632f40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 170
        },
        {
            "id": "b1411f84-e0d1-4313-9af4-d10c520055a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 186
        },
        {
            "id": "8fe2684d-92b0-4ef2-ad03-77b0aaceb3f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 217
        },
        {
            "id": "270420d3-d5cb-4f65-b9d9-9cde6ba39bf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 218
        },
        {
            "id": "73d29206-7b82-44c3-a19e-27a4e33954e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 219
        },
        {
            "id": "8278bb44-5220-43a7-b97f-ab90fe2459d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 220
        },
        {
            "id": "bb0ba523-3975-44de-99e6-c3832a46225a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 221
        },
        {
            "id": "47beb74b-0d20-47bd-b792-dd7c44871c09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 253
        },
        {
            "id": "04ea89ad-8461-4ca6-956a-5dbc294641c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 255
        },
        {
            "id": "99db338b-ca44-425d-bbf3-4d9ecc23c92e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 329
        },
        {
            "id": "340741fb-1523-4ac6-9388-20bccf0106d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 354
        },
        {
            "id": "30bea7ca-6688-407f-9c55-7d35ed07603b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 356
        },
        {
            "id": "187cd637-cfec-4cd7-8676-2d7d55f3fb77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 358
        },
        {
            "id": "703a6b31-21b8-4d59-9572-7dac4ee34621",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 360
        },
        {
            "id": "9da99d60-5902-44c2-8817-bf180854c909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 362
        },
        {
            "id": "5b521297-9c81-484d-a00d-5a2fae02fb3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 364
        },
        {
            "id": "5810976d-c1be-4197-abfe-95cd28ea768f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 366
        },
        {
            "id": "19d4beda-2257-4d7a-9a45-6c5a7547e2f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 368
        },
        {
            "id": "44b3f55d-a0eb-4137-a7ff-f1ae94bd56b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 370
        },
        {
            "id": "c483fe88-1ef4-4549-ae7e-e5c81de1d94b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 372
        },
        {
            "id": "4b73c1f4-dbbd-4c09-8b55-6d0aaa95a030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 373
        },
        {
            "id": "0528afc4-5136-49ae-b759-70b36bfbe9a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 374
        },
        {
            "id": "9ad3a9a1-a991-46b2-9002-248674c5f432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 375
        },
        {
            "id": "75012a4a-4b40-4052-be40-c3fc8bf66021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 376
        },
        {
            "id": "15d8e37f-5976-4811-8698-82b0f03f14ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 431
        },
        {
            "id": "5fd5feec-50c4-46d3-8287-16582830914f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 538
        },
        {
            "id": "0b103c3c-b7ee-47fb-aadd-2e2353c8c3b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7808
        },
        {
            "id": "c6712b47-33eb-48d7-8fa6-8948db247beb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7809
        },
        {
            "id": "f8c97c1c-b45a-4562-8501-a3b80673c904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7810
        },
        {
            "id": "0f30e756-8d0d-466b-b1d7-b6bb2ec02a58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7811
        },
        {
            "id": "ee7d5557-0bd8-4af6-b2ad-06a56e6fee2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7812
        },
        {
            "id": "ee301027-cc4c-472d-aafe-c7f37276cd14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7813
        },
        {
            "id": "5a2ee4ac-acf4-453f-a2e7-44048e5ba61f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7908
        },
        {
            "id": "073ff58b-1d7c-41b9-b590-7b3d6221a55a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7910
        },
        {
            "id": "dabfea0b-69f9-4810-8efc-4f1a4a72c268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7912
        },
        {
            "id": "b597b461-41b2-46a7-b39c-05461c23840e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7914
        },
        {
            "id": "2c24dc9c-36c0-4b32-af0d-a78e7c89f110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7916
        },
        {
            "id": "23c28b3c-e0d5-4956-a38c-14680b769b80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7918
        },
        {
            "id": "aa9e696f-454c-4390-9581-a735dfc74415",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7920
        },
        {
            "id": "711f7d9a-d98b-487d-8594-ff089e01fe43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7922
        },
        {
            "id": "20356379-8a44-49d6-aad2-7dcc8ffd3296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7923
        },
        {
            "id": "56dc5079-853f-4a8b-8068-efa6d45c172c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7924
        },
        {
            "id": "4265f28b-f70a-4cc2-94e1-dd3dea69197f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7925
        },
        {
            "id": "17b29ce7-4d91-4335-82ec-682dc0427368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7926
        },
        {
            "id": "9456552a-52d4-4eff-806e-db86b1704e76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7927
        },
        {
            "id": "a58a6c1e-a3b0-4988-93a6-7c461d43b484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7928
        },
        {
            "id": "8d29f7d2-d1de-47c8-bfc6-a44555072c0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7929
        },
        {
            "id": "ec0f8e92-8bb2-40d3-8658-3843571763d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8216
        },
        {
            "id": "c1efdd3c-e3eb-46ac-9441-2d05fe9ab888",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8217
        },
        {
            "id": "ad2b0891-5dc9-4978-8845-5d2b4f6de2b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8220
        },
        {
            "id": "4bebaee5-36c2-48d1-ab6f-0dfca5f517ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8221
        },
        {
            "id": "d909b4ac-90ee-405f-8a5f-fba55bf9c624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8482
        },
        {
            "id": "4cc01803-5728-430e-9b4b-0e549cd5cac1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 65
        },
        {
            "id": "d338ea54-9463-4416-906b-65a59648fcf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 192
        },
        {
            "id": "d6ee09ab-8de8-4045-875a-1019b2008b63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 193
        },
        {
            "id": "4023fdf8-53f0-4b67-95c0-f7b3186afa4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 194
        },
        {
            "id": "c6b186ea-9c75-4777-84d7-0d16c6d97871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 195
        },
        {
            "id": "ae17ec04-d8b1-4879-89cb-1ed8a83dd90f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 196
        },
        {
            "id": "2911794c-7e4a-47a9-ad68-b646a9458c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 197
        },
        {
            "id": "12f888d8-656e-4a0e-9da2-c22ba055b040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 198
        },
        {
            "id": "e9942a1a-ef5c-4b95-8f5c-a5ee7873aeba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 256
        },
        {
            "id": "93596893-94cd-422b-9790-681bf98675a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 258
        },
        {
            "id": "9cb0a364-77f5-409f-ab9a-847079f9b20f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 260
        },
        {
            "id": "3d420a8e-8a1d-4772-86c5-6e2de78b5662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 506
        },
        {
            "id": "7add5233-e8b3-4b8b-a3be-f2aecefcf685",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 508
        },
        {
            "id": "920f9d7b-0e68-4ae7-87a1-2cbd67f623fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 7840
        },
        {
            "id": "91d9602a-ef8b-4416-afb5-d5d93c2648c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 7842
        },
        {
            "id": "de095f9b-90e7-498a-860f-cb53fcadccfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 7844
        },
        {
            "id": "734b7107-cb4a-4c2e-9443-1b2eddec11d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 7846
        },
        {
            "id": "a5691928-d971-434f-a69e-b1d5e530c599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 7848
        },
        {
            "id": "469df6a9-6dfe-4099-88cb-bb38aedd0458",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 7850
        },
        {
            "id": "a830859e-3e1a-4ea9-8978-1a9f99f05edb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 7852
        },
        {
            "id": "7d95e2db-a9ff-40c2-8de7-1616ebf3498e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 7854
        },
        {
            "id": "e77cb838-1571-4153-8889-75f8131dbd05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 7856
        },
        {
            "id": "f3609d6d-8070-456c-9e23-c1a7051dc2a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 7858
        },
        {
            "id": "f2daa273-7918-4309-9974-8885cb61d106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 7860
        },
        {
            "id": "8feca342-0662-4997-bd65-c6f56bc2faf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 7862
        },
        {
            "id": "3b908fb3-91fb-43a0-84a0-ff996b02f18e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 44
        },
        {
            "id": "bfbb2c0a-408c-4acc-871d-40e3cce8afaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "1002dc4b-ca68-4a1e-b584-8bdb67a2deb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 65
        },
        {
            "id": "e9383cf5-ab8b-4e62-a288-3c1976edb522",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 86
        },
        {
            "id": "f51b5e49-c8c7-46db-841b-0c773e064a11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "563ac44f-cea5-479b-88a0-1fb60d021996",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "a42ca8bf-8b87-4e85-a758-8cf43647c7f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 90
        },
        {
            "id": "63aaa18f-abd0-41a7-893b-6bc6536f3a0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 192
        },
        {
            "id": "3c21d71f-584f-4cc6-a559-3c2087e35fa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 193
        },
        {
            "id": "1e4a33c4-6f54-473e-943e-0ef3e36af4be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 194
        },
        {
            "id": "5ceb5765-e40c-4fe9-a359-c0dde3ffd46f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 195
        },
        {
            "id": "22901232-2cd4-4d31-ad2c-e582deed36fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 196
        },
        {
            "id": "26c756d1-22a5-456a-ad97-715c75cf03fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 197
        },
        {
            "id": "e2d51a43-9591-4af2-ad84-93ffc14ac199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 198
        },
        {
            "id": "7dbf7083-3623-4992-b267-a7f5ff571609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 221
        },
        {
            "id": "3da5be4f-1743-4bbc-a76d-e311efb7fe99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 256
        },
        {
            "id": "a62f806a-a73b-40e0-ab87-9d7320e7d145",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 258
        },
        {
            "id": "73b95909-fe84-44b1-98a6-904f9cd4a06e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 260
        },
        {
            "id": "ca8e46bf-27eb-4391-bbe9-aeec096c74be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 374
        },
        {
            "id": "3d4b1fae-a3d6-45cd-b846-2dbebbe041d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 376
        },
        {
            "id": "a7755f7e-5829-43cf-8281-d73b28101433",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 377
        },
        {
            "id": "6a30d865-6cf0-4966-a653-e80d5c560275",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 379
        },
        {
            "id": "b9a6a655-c04a-4ae0-aacf-701b39c28d9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 381
        },
        {
            "id": "7b8b5770-b20b-474c-b740-987dceee4140",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 506
        },
        {
            "id": "2569028c-b613-4061-807a-a9eacc198fe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 508
        },
        {
            "id": "734544ea-a583-41fd-a68f-1c6407a3f498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7840
        },
        {
            "id": "f356ee0e-8e32-4dc8-8cb7-20c7fd0ea1b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7842
        },
        {
            "id": "66ecfde8-776e-4ef2-a815-4fa425ebb67d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7844
        },
        {
            "id": "663e8c7d-8004-496b-a863-2df4440eab9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7846
        },
        {
            "id": "658fc6ba-bdb7-42ca-9a3f-b00658503165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7848
        },
        {
            "id": "9e337030-1757-434e-bd4b-a5ff3cd69f5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7850
        },
        {
            "id": "c390ad6d-8fdb-4823-be90-0eafcfc7f42f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7852
        },
        {
            "id": "9ab76eb6-ca70-47e8-b192-e5568458c204",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7854
        },
        {
            "id": "aedb6ef8-d9b6-40c5-9c39-a80dbda1bd18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7856
        },
        {
            "id": "efd18d7d-32df-4606-a4b1-895ff5a32ede",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7858
        },
        {
            "id": "2b8897b3-3c3c-49c2-90d9-93091bc1e339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7860
        },
        {
            "id": "9117d6a4-0eac-4449-b6f2-acd7597d731b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7862
        },
        {
            "id": "87e16d2c-97f1-4454-8055-cec53b18d1cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7922
        },
        {
            "id": "b07a3406-a14f-4e6b-8d41-fcccba98a320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7924
        },
        {
            "id": "0acf43e6-0b50-474d-935b-0d8dd13c0933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7926
        },
        {
            "id": "8dd9e74f-e2b6-4998-81aa-11d52bb1fd99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7928
        },
        {
            "id": "5022510d-8f19-4e6b-9ec3-c677fb2a9ac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8218
        },
        {
            "id": "ec9f7672-4c91-49e5-9281-4620f41c275d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8222
        },
        {
            "id": "039b46b6-4771-43e2-ba7b-55a467c480f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8230
        },
        {
            "id": "8c091315-a8c9-4036-b094-4838c513d133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 44
        },
        {
            "id": "6c586d6d-8836-4801-9d86-afde18d334ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 45
        },
        {
            "id": "78b3b472-507b-4d38-bba0-ad7f46494c79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 46
        },
        {
            "id": "e73fcf2f-3341-4809-be45-bd5fd30e62b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "8c6e30ac-4f3d-4846-a9da-32c4175c3c1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "747be050-5bf6-4ceb-bcb6-53ca9722154c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 99
        },
        {
            "id": "d90e2eaa-6f84-4739-83bc-6f1b4c6e966c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 100
        },
        {
            "id": "2deeeeb5-a8e3-4b2a-af42-0ff02da42c6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "1907bd1f-83f8-4497-975e-d177fb4cb353",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "1a7d6eb3-dd1f-4f9a-b905-31320d9e664e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 113
        },
        {
            "id": "1b103434-c530-44ac-b78b-1e8927d50190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 171
        },
        {
            "id": "a963a87a-8877-405b-a75a-48be98b66de1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 173
        },
        {
            "id": "14910f11-1a6d-4a4e-bea2-ed6440ac9656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 192
        },
        {
            "id": "77e5b2de-cf7e-4699-85ce-856e9f7a138e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 193
        },
        {
            "id": "4073ff78-f7d0-443b-ba59-8883ed43a2cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 194
        },
        {
            "id": "02fe1456-3eed-42ed-be36-ed2fa721d56c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 195
        },
        {
            "id": "bbbff946-56a5-411d-8e1a-d3db76bbe462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 196
        },
        {
            "id": "2bb4b2ff-85fd-43ad-8166-121cdee7783a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 197
        },
        {
            "id": "55aea5e8-20e8-46f9-875c-04c0af9f1fd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 198
        },
        {
            "id": "79a0f7fe-2254-4631-a0d9-3e67b32376f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 224
        },
        {
            "id": "5a9e3802-bd8f-43f9-8e8a-785130d62bd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 225
        },
        {
            "id": "22bc7194-6eea-42a1-b2bc-e79cb6b726f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 226
        },
        {
            "id": "397b0e5c-f5e1-4ed3-a989-75ac0fedd8b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 227
        },
        {
            "id": "f3b84c17-7668-4659-b272-8e1661e951a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 228
        },
        {
            "id": "9815ede5-32ff-4595-900b-6494c9acbdad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 229
        },
        {
            "id": "f9dfab31-656f-49d6-b3f8-0b578d1ab94a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 230
        },
        {
            "id": "aafbe4c5-192b-4671-bb6e-cff0c31390b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 231
        },
        {
            "id": "65ae0b57-818a-48fd-9089-285a8f6d2a8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 232
        },
        {
            "id": "29d21494-7cd2-4fc7-8d53-0520e18f2b9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 233
        },
        {
            "id": "8c6640d0-02fa-487a-bf2b-23d302c0946a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 234
        },
        {
            "id": "3b3605e7-b78f-405a-9839-a3f5fd84b5a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 235
        },
        {
            "id": "0b66b69c-5767-4857-bae7-e81571841bdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 242
        },
        {
            "id": "a0697816-d8df-4092-9c68-d7dfc16cfea6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 243
        },
        {
            "id": "ec724f3c-37d2-45c7-9563-6f761f1c0446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 244
        },
        {
            "id": "5719c84b-e74e-4a39-a729-c36ab4a16f1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 245
        },
        {
            "id": "76460a09-64d0-4ba7-b4e1-7f4cc019c303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 246
        },
        {
            "id": "5b8ca9cd-87f1-4c99-ba3d-e1379fb7a5ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 248
        },
        {
            "id": "f049236f-60e0-4a2b-9ee0-701985687a57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 256
        },
        {
            "id": "48dfec68-2963-4d21-b0ff-e418c0668063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 257
        },
        {
            "id": "c6208ed3-b1bf-478c-b910-00c48226fc93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 258
        },
        {
            "id": "72c91d10-6720-4ccc-88ed-3b7b8be42b1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 259
        },
        {
            "id": "be82d46b-6978-4688-9626-7e4ced78043d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 260
        },
        {
            "id": "84a65008-f320-45dc-a923-afc9a49a1807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 261
        },
        {
            "id": "b011af43-b2ed-40cd-acfb-f92bb301cf1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 263
        },
        {
            "id": "4bd534ac-3c85-4af4-b02b-0a495d71c128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 265
        },
        {
            "id": "493feafc-c562-4bd3-8e02-1749e9f07ec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 267
        },
        {
            "id": "0f68151b-7230-466c-9ed0-8abe6c4330fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 269
        },
        {
            "id": "fa34e10c-22bb-4d01-a06e-568d4c1fb75d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 271
        },
        {
            "id": "73169e16-5c3c-43d3-b1e6-f4e19ba842c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 273
        },
        {
            "id": "add0acc8-6666-49b8-8571-c91a83930249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 275
        },
        {
            "id": "3958d799-a139-430b-984d-2f0a7f250c67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 277
        },
        {
            "id": "3967409c-172e-4db0-a3d9-4b06797e2318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 279
        },
        {
            "id": "7d2d82b0-f529-429a-91ff-5ec0674a820c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 281
        },
        {
            "id": "57b80c68-4806-4647-8c14-6787f6fd8110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 283
        },
        {
            "id": "d3f72a48-d017-4131-90ab-b50da7d7b7a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 333
        },
        {
            "id": "07568017-eb10-48f1-9971-7cae18c25d3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 335
        },
        {
            "id": "bb00b778-29d2-478c-85f8-5ef519290f57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 337
        },
        {
            "id": "37464854-9be6-47d3-b145-a31468841bdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 339
        },
        {
            "id": "e0181bc2-61c1-4e23-9bef-bda60c0fb619",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 417
        },
        {
            "id": "659afb5d-c50f-4935-8fcb-40cfe38b3357",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 506
        },
        {
            "id": "5ccebcbd-3c65-4dab-9fbf-7a0874e22d27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 507
        },
        {
            "id": "41d071a2-4a84-467a-89d7-a0fa074bf42f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 508
        },
        {
            "id": "08377f9b-1063-4d64-9eb6-a3f0b5834476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 509
        },
        {
            "id": "9a7de20a-11eb-4ea6-a638-8c47f8caec8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 511
        },
        {
            "id": "bfb6490d-e585-45fa-9a41-be28f7ad029d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7840
        },
        {
            "id": "7d85d362-4c5f-4426-8acd-08038e69c3ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7841
        },
        {
            "id": "ba54ae10-21e1-4560-bd7b-83163ccb92b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7842
        },
        {
            "id": "ef3516ea-7a1d-44ef-9324-62a1d48a2fd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7843
        },
        {
            "id": "638203cb-2b57-49ea-bc59-d3cb8800013c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7844
        },
        {
            "id": "e03fdbd3-bf77-493d-aa6b-835eb6241d85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7845
        },
        {
            "id": "07a69ded-ffbd-452e-bb30-36352fe01ecd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7846
        },
        {
            "id": "0544f8f4-de28-457d-977b-60bbce248aa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7847
        },
        {
            "id": "46888f5c-ee10-4817-9b86-5e653e53e8fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7848
        },
        {
            "id": "67316f3c-2444-44e3-8d31-0920ac9d324d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7849
        },
        {
            "id": "8a461e8d-40ef-4801-a312-94582fb22642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7850
        },
        {
            "id": "6caf1c93-8841-4365-80ab-d57426e8ccab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7851
        },
        {
            "id": "08bfd411-397d-45e3-af96-121c6a5211cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7852
        },
        {
            "id": "d02b5d26-4ceb-4e75-a862-a64775225328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7853
        },
        {
            "id": "bfa14d37-6907-407b-85dc-b2bd80d8346b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7854
        },
        {
            "id": "44f5ce9b-ba5b-46bc-a1fe-6c395fe33384",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7855
        },
        {
            "id": "5478238f-22e8-4db9-bb0e-5b61756265c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7856
        },
        {
            "id": "d6221809-a3e9-48b0-826f-009527e8961e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7857
        },
        {
            "id": "205165ec-9f50-4504-ae6a-0bd75ed56a8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7858
        },
        {
            "id": "1d36cc51-ffd9-4d9c-9a4f-9c8097562221",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7859
        },
        {
            "id": "ffaed2e5-8ed8-4ac1-a6a5-d8a5fae6d76e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7860
        },
        {
            "id": "bbf3f524-fc40-4f03-93e0-19fa970526ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7861
        },
        {
            "id": "8a51c5eb-e992-475b-8325-b2a152491888",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7862
        },
        {
            "id": "835cd4ac-3b7d-4823-9a93-88ec3deccbfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7863
        },
        {
            "id": "75afbe5b-d019-4747-8911-ca25f1a5d89d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7865
        },
        {
            "id": "da1422d7-8701-408a-96d9-4bcce719819e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7867
        },
        {
            "id": "978126b6-6236-4bbb-ba50-311eea0b954d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7869
        },
        {
            "id": "dd287a48-35e3-4678-aa9a-0c66bad1f772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7871
        },
        {
            "id": "1af03155-79c4-4275-b1a5-0a0f2cf967cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7873
        },
        {
            "id": "19dd17c1-2635-4510-a51a-a903b77f6a5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7875
        },
        {
            "id": "ffcbaf83-cce3-4615-b30c-45505e905491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7877
        },
        {
            "id": "a944435a-aab0-4028-98f5-87b992615968",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7879
        },
        {
            "id": "b68dcfba-ffae-49ef-9b94-dfe5006ca6b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7885
        },
        {
            "id": "26fad32c-627b-482a-a3d4-ee0f374636a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7887
        },
        {
            "id": "37b4162a-0070-4eff-9b70-250e5b39d8d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7889
        },
        {
            "id": "3f55e59a-92d6-4bd0-8bee-9bc08a9223e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7891
        },
        {
            "id": "e541e6c3-b7bf-4412-bd6b-fb7e1f3453ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7893
        },
        {
            "id": "4d88fd2d-dbe1-40f5-84c9-4af20b003be2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7895
        },
        {
            "id": "7b1d1df4-5da1-4501-b04d-7c702219a03f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7897
        },
        {
            "id": "047c4cf4-7c7c-4e3c-8859-c2d81bb485c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7899
        },
        {
            "id": "895b4790-f135-43b5-9f4d-430a64bcbe2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7901
        },
        {
            "id": "aba92b39-ddfc-4e0b-b23a-bdbf60031f85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7903
        },
        {
            "id": "44eb5ee8-2730-4daa-85ef-7a0002209543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7905
        },
        {
            "id": "5a9025df-cc0e-4ffe-849e-d8ba9a901b4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7907
        },
        {
            "id": "a22eb369-cd63-498b-9b32-8584b2f3eafa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8208
        },
        {
            "id": "45b66837-6064-4a6e-ac03-626d1044e788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8209
        },
        {
            "id": "5352c2ba-23c5-4bef-82af-823ad821d4f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8211
        },
        {
            "id": "d0393a6d-79da-4bdd-9dbb-b497759e9cb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8212
        },
        {
            "id": "5e5b0237-0c41-4573-a245-37090f52147e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8213
        },
        {
            "id": "bcb1be08-462a-4a75-971c-a43e85eb6daa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 8218
        },
        {
            "id": "d3ea5ac2-5c8c-4e8c-861d-e31bd488de96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 8222
        },
        {
            "id": "803cddb4-c942-4ce8-84ac-c5f20dc29bbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 8230
        },
        {
            "id": "8029051d-ce8c-40bd-afc6-07ba2d0767e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8249
        },
        {
            "id": "a354915e-5ec3-40e0-b171-3ef34831a996",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 44
        },
        {
            "id": "08003661-6c03-4e52-93fa-227bffb37fab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 46
        },
        {
            "id": "e59c155b-62c9-4816-a564-87f9257d2512",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 65
        },
        {
            "id": "1621de3b-8497-42b6-9764-c9a9db6885a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 86
        },
        {
            "id": "ad1c6228-4a5a-41b9-b205-c79fb01cb711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "0ddf5f57-afe1-40cb-8b52-291b38586a61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 89
        },
        {
            "id": "8255985e-07f4-46d0-8924-327d5d26cca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 90
        },
        {
            "id": "d4e3c8a8-3c9f-4ed5-aa31-494e65037d10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 192
        },
        {
            "id": "3dcd2860-13e8-4518-99a4-d87ae358093e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 193
        },
        {
            "id": "6a8efca7-80d8-4833-bf20-f7d2a4a4383a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 194
        },
        {
            "id": "1f2cefb8-1670-4b47-9780-438ac4bbb5f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 195
        },
        {
            "id": "4833d7f6-b3a2-48e0-86b1-ab55f015c329",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 196
        },
        {
            "id": "19175e49-95d9-47af-955b-48fdceb1fd75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 197
        },
        {
            "id": "09b3d120-164a-4a92-baf7-32500a3ec2f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 198
        },
        {
            "id": "b0e78953-88b1-46e5-a19d-29d4b5b117e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 221
        },
        {
            "id": "b2d1ed15-3cf8-41c3-9cae-42fc1115523d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 256
        },
        {
            "id": "2e318e4e-488b-4161-80e5-7dec8becf5d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 258
        },
        {
            "id": "751de3bc-9d73-4086-9712-fbfdee1b9a9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 260
        },
        {
            "id": "ff97e543-6158-4ad0-bc48-7d8229dcd1dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 374
        },
        {
            "id": "6d545f88-4339-452d-8749-73cba03340e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 376
        },
        {
            "id": "e206e013-a196-4a9b-bc38-555a084b1b02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 377
        },
        {
            "id": "6fa239bd-0685-4259-82d8-9c9b3bee00e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 379
        },
        {
            "id": "3ea10633-96e4-4b6c-abea-c76f0c8cfb25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 381
        },
        {
            "id": "065f3408-aa93-45c5-996d-d67d660f932c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 506
        },
        {
            "id": "01a7114c-8072-4b4f-9e25-ffeba92238fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 508
        },
        {
            "id": "5d0c3c7d-ed64-454b-8087-08670be718b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7840
        },
        {
            "id": "f1576ad4-abfe-4607-a2ec-6b8fbdc8ea13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7842
        },
        {
            "id": "10a36e8c-b9a8-4653-ac42-4af32d2706d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7844
        },
        {
            "id": "38813b08-c224-431d-9951-577eb4e597db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7846
        },
        {
            "id": "949b39ff-183d-4d80-b8d3-7b9c1c75aaae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7848
        },
        {
            "id": "70c9a4ca-175b-4b1d-9a54-6a0b4e95fb6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7850
        },
        {
            "id": "85a7dc7d-670e-40b1-9c9c-0d7f45b8d895",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7852
        },
        {
            "id": "0d9dea89-7c56-4b5c-9b7e-724bc820c246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7854
        },
        {
            "id": "5b2046cb-a9a4-42f2-b5eb-0d0c1ab08c20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7856
        },
        {
            "id": "91272b34-4a46-4585-aabd-7b25705f4e82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7858
        },
        {
            "id": "a3eafb13-1715-4688-8264-f2b3fb2dd708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7860
        },
        {
            "id": "064d9c2a-9ab2-41d3-b30a-816c50b2bb7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7862
        },
        {
            "id": "9659dae7-670f-4e66-8401-5bcb3e11dd1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7922
        },
        {
            "id": "eaf92543-c86d-447b-b2be-c5a61a3fb9cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7924
        },
        {
            "id": "e6afe20c-6456-4e01-972c-8a61fc6717a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7926
        },
        {
            "id": "a57eba50-23da-45d8-8662-801825b3f267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7928
        },
        {
            "id": "f3a2f964-e1a7-4851-a330-e26aa073cda2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 8218
        },
        {
            "id": "12ff2be1-08c5-4bef-a244-9933c62c6215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 8222
        },
        {
            "id": "7d161e45-ba8e-40db-8270-2fd1890cf93c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 8230
        },
        {
            "id": "02380f4e-fe6e-4cef-9199-70e9c0483014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 34
        },
        {
            "id": "6940415f-db2d-4cd5-84fd-da878d529715",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 39
        },
        {
            "id": "560aa2da-fc12-48af-8933-d39990c90323",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 45
        },
        {
            "id": "d61bc07b-04ee-46ac-ab3a-956e9c387bfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 67
        },
        {
            "id": "ffa1f31e-9c17-4688-9472-8276bb7b824a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 71
        },
        {
            "id": "cc2e61a9-87ac-4895-9722-5af62ffa0c33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 79
        },
        {
            "id": "cf733606-8d69-41c8-b31a-67e984bc505e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 81
        },
        {
            "id": "aed2ae65-0ac7-4871-a625-04a0a1308de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "9522de39-9be6-44cc-83e6-193ebc7d8896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 85
        },
        {
            "id": "f2774b90-9480-4bc1-9032-5e3155d19b69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "a83e3be9-90d4-403c-aa18-30bf7a3c3edc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "352742b8-e86d-4d96-8467-44cd82a26a55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "fcd66c7e-60cf-43f3-aaf9-a878314cda82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 99
        },
        {
            "id": "8d2be760-2819-41fe-aad6-17357f02336d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 100
        },
        {
            "id": "90aac63b-be90-4131-b04c-24031b625bf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 101
        },
        {
            "id": "2d3bb168-5bd9-4efb-a42b-fb2ef36b5959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 111
        },
        {
            "id": "1d23f933-73d2-41e9-bdda-f2af81552788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 113
        },
        {
            "id": "1c46289e-a520-4afa-930c-380844c742c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 117
        },
        {
            "id": "90bf4f3c-a2e7-4ee6-a435-19aad923738d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 118
        },
        {
            "id": "52ae5c03-576b-41a6-8d11-dafc4dc305dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 121
        },
        {
            "id": "92a00f07-581c-4eec-90b4-4340f542834b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 170
        },
        {
            "id": "1b66be7a-bfef-41cd-9a89-7b30b61f7dc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 173
        },
        {
            "id": "80b33b1e-4831-42bd-93c5-a7a1f86d7fe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 186
        },
        {
            "id": "f82d2537-4d84-4d14-bbb9-2344e3b72af0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 199
        },
        {
            "id": "d37c135c-894f-4d9c-9153-af3aa6505127",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 210
        },
        {
            "id": "f9b5194f-5085-4442-8b7f-169b44ff3dda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 211
        },
        {
            "id": "dc81a81a-5568-44c3-a3e6-b04ef7f30b38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 212
        },
        {
            "id": "99f345db-24a4-473f-9acf-5f1b9755b3d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 213
        },
        {
            "id": "524b1b12-4553-4697-86a1-ca702b0601e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 214
        },
        {
            "id": "088a4d4f-9833-4ab4-9471-6e4ea6d8b427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 216
        },
        {
            "id": "809b5562-d18f-4265-b04e-553171efc2de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 217
        },
        {
            "id": "8d678292-7f52-4271-b3df-af32ea3d6498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 218
        },
        {
            "id": "fd4a0794-9990-4d9b-92bd-970708f39ffd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 219
        },
        {
            "id": "62834c10-2ecf-4f9b-b603-e39569b0c019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 220
        },
        {
            "id": "9314e310-3531-43d4-9528-2fb805931e79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 221
        },
        {
            "id": "0a79552f-5b99-4dd8-9222-13845ff2b63f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 231
        },
        {
            "id": "e161101f-8c08-4fe1-8aff-a0c695da49e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 232
        },
        {
            "id": "d9277c6d-ca7f-4839-b0e1-9c60961a86e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 233
        },
        {
            "id": "06257f7d-0b0b-4847-8768-f327f5c338a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 234
        },
        {
            "id": "8b81434f-79e7-410f-9c25-4639193774d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 235
        },
        {
            "id": "74b3ccf4-3462-4af2-b861-995d81e911a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 242
        },
        {
            "id": "1de28f61-10c0-44c5-9c7a-cae71c9645fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 243
        },
        {
            "id": "452e230d-10d8-4acf-837f-9aa2b9c86910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 244
        },
        {
            "id": "5c4be16e-7da4-4b08-9216-bbe4a85ac08c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 245
        },
        {
            "id": "4d822c69-eb93-4d14-ab4b-5636577d05b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 246
        },
        {
            "id": "38bb009d-32ee-4f58-b3e1-170619a4a41f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 248
        },
        {
            "id": "91868a29-d365-40c8-a826-7d8385a88327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 249
        },
        {
            "id": "b0d17bec-1528-4e18-88f6-7fe469552dd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 250
        },
        {
            "id": "4d6921d1-26da-4eb9-abf7-aaeb5377c7f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 251
        },
        {
            "id": "ad30df26-e3d3-45d3-9dfd-49c4c260b377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 252
        },
        {
            "id": "773e963c-0feb-4d13-9f22-7c49696f6aba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 253
        },
        {
            "id": "51f8fa4c-48a3-4735-bbfa-bf19fd8b4fcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 255
        },
        {
            "id": "881ba2a0-22f4-45c4-899c-f0b5fd51bd0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 262
        },
        {
            "id": "a212bd71-7103-4487-a6ed-7146c29ed5e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 263
        },
        {
            "id": "a148f0fc-b0e2-4864-80cc-14b39e8f239c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 264
        },
        {
            "id": "95dc5e78-a51f-4c08-914f-66ea591f368a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 265
        },
        {
            "id": "c7e64dcb-1cd3-4898-b6e5-97cf3a461251",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 266
        },
        {
            "id": "998e0bf7-d9b9-4a4d-9add-9b25310df97e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 267
        },
        {
            "id": "29204fe9-e890-4d0f-a408-2fe662a7a68d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 268
        },
        {
            "id": "e83f2d80-0492-4d4a-9fd4-004f6383b2bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 269
        },
        {
            "id": "9730bb71-b702-478c-a1d8-5f1fad94f32b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 271
        },
        {
            "id": "35f17e30-a41e-4b55-8b05-8ee6681671dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 273
        },
        {
            "id": "ad4f819f-4f91-4dd2-a5d5-83af5917e564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 275
        },
        {
            "id": "5dd3a669-a3d8-4ea6-8074-1c0fe05d1b6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 277
        },
        {
            "id": "300449dc-a778-423b-8c19-ff7299181b2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 279
        },
        {
            "id": "64ad5552-972a-4232-9dc2-7b8913be864b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 281
        },
        {
            "id": "d5d37449-3c36-4fc9-bb20-67ce2d4294fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 283
        },
        {
            "id": "27fec3b9-6a8d-41bb-8039-f73dd566ffd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 284
        },
        {
            "id": "188ef4a0-8914-4d59-bd80-367d4984edc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 286
        },
        {
            "id": "0383ac68-c6f0-416f-a0ea-2d935c234204",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 288
        },
        {
            "id": "f05cc8f7-08dd-4313-98ad-20e3a9088fb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 290
        },
        {
            "id": "6a5cdeda-002b-4bc4-bda4-d6419287694e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 329
        },
        {
            "id": "d6ec3406-cb46-4104-a6dc-76b744dc9e62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 332
        },
        {
            "id": "485c4467-aae1-487b-887d-452eca454f02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 333
        },
        {
            "id": "504f7fcc-8995-4ac7-b025-8d723acf4cfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 334
        },
        {
            "id": "311d1884-de1e-4072-b22b-8cea561ae0e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 335
        },
        {
            "id": "157bbace-999b-4b2b-969e-b5e8b86cc6b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 336
        },
        {
            "id": "17425339-7df1-417a-a031-d79626c5c3b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 337
        },
        {
            "id": "444d9b16-bb67-44d2-907c-0f7127245080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 338
        },
        {
            "id": "6aa8e533-ee29-42d4-bfc5-4fc5d0227f83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 339
        },
        {
            "id": "61eec14e-3d1b-4509-a6c2-2764915bccf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 354
        },
        {
            "id": "0614fcd1-b9d0-4eac-89c6-a8eea1a52b57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 356
        },
        {
            "id": "78c819b9-575b-4042-881a-126e44e32eca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 358
        },
        {
            "id": "877c2884-e7f9-450f-b0c9-a2e963390400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 360
        },
        {
            "id": "89617f56-ae52-4f64-8f13-ce04df65bb46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 361
        },
        {
            "id": "896ec5e3-009b-42d2-b58c-2807f7f0de27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 362
        },
        {
            "id": "7d2b91c9-678c-4fc3-b39d-3f4be375df88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 363
        },
        {
            "id": "da5df833-7654-4704-a1dc-1d04df428fbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 364
        },
        {
            "id": "951a8b55-4f09-411d-8a84-5d7920e59352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 365
        },
        {
            "id": "695aa09d-3bc6-4185-bf31-63fffed56a35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 366
        },
        {
            "id": "5df48010-9019-48d4-9868-d9dd84fbd1ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 367
        },
        {
            "id": "a778b801-1881-4dd8-be44-1c7ce6bca9e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 368
        },
        {
            "id": "356c551e-b1cc-4c48-b8fc-ad227be1f09a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 369
        },
        {
            "id": "1b4354a3-2033-4b6a-9d3a-e749c34ada0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 370
        },
        {
            "id": "a10321fe-f1f1-4c63-8d49-a54cab3d5d2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 371
        },
        {
            "id": "057e0fd1-6cd5-4bdc-b7f7-c75043ebc7d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 372
        },
        {
            "id": "400b37ea-6dd4-48f6-843c-1d3bfb57e8e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 374
        },
        {
            "id": "a588f847-be08-4bc0-b53a-7bcfa77b4703",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 375
        },
        {
            "id": "35fa461b-c360-48c5-a48a-b0c71aeb556a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 376
        },
        {
            "id": "c7dce343-626d-4dc5-880f-dd18fd12ac04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 417
        },
        {
            "id": "c7e024ae-97bf-45f1-8934-3899cae51126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 431
        },
        {
            "id": "76a97745-b550-40de-846b-72c9b27511c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 432
        },
        {
            "id": "71d2e067-1a91-4fb5-9c1d-ac932f2f3039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 510
        },
        {
            "id": "0c9bc178-80c7-4431-b692-aba1a5e4146c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 511
        },
        {
            "id": "e817718d-03d8-46e2-9e4b-7f393aff24f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 538
        },
        {
            "id": "cf207de4-4f58-4a7a-a118-f60c755ac4f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7808
        },
        {
            "id": "08c6f457-26c3-47c2-890d-860503f6e332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7810
        },
        {
            "id": "01473361-7c96-4364-9d53-770abd101509",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7812
        },
        {
            "id": "9fa25369-9272-4e43-ae35-b89e3eb9d67d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7865
        },
        {
            "id": "7f58b470-7cf8-43ca-bedc-24087e8808d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7867
        },
        {
            "id": "e816a270-3ab7-4071-9597-b1117987a8c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7869
        },
        {
            "id": "efa7a7be-e5a8-4073-9782-119447a8daac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7871
        },
        {
            "id": "517ea85d-afbe-48e2-bd97-3dd4455046eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7873
        },
        {
            "id": "1699f20d-3f53-4fde-b521-00275f7a3b0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7875
        },
        {
            "id": "2cbd275f-e173-46f5-b0b6-84dda6ade901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7877
        },
        {
            "id": "4623dc55-e1be-482b-b5cc-3cabcf3118be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7879
        },
        {
            "id": "dde4688e-20bb-476d-81df-87db748a4b10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7885
        },
        {
            "id": "80c8f978-b60e-46e7-ad4c-5e3dd0ebfed1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7887
        },
        {
            "id": "8392032c-29f2-4d12-8cc2-16b61a8df27c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7889
        },
        {
            "id": "6531a962-2187-4f14-af31-661196b41758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7891
        },
        {
            "id": "6325bba3-142d-4269-85ad-5b9358c93672",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7893
        },
        {
            "id": "4cbe90b9-1ec0-49ab-b664-ecc67b3122b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7895
        },
        {
            "id": "de4197e2-0fa7-458d-9dbd-5eb497b768a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7897
        },
        {
            "id": "058575b0-5d04-4f34-8e0a-6cc56028d29b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7899
        },
        {
            "id": "8f38dd84-31cc-4634-ba49-7c37888f7716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7901
        },
        {
            "id": "f36f5e43-60fd-4dc5-af05-6aab23d236a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7903
        },
        {
            "id": "e0f6b673-3a85-4bde-91bb-8f6008e3c277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7905
        },
        {
            "id": "c780bf96-225b-4acf-92bf-860c04b4bf4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7907
        },
        {
            "id": "3169a381-1ae8-4f7a-b00b-a90ebace39c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7908
        },
        {
            "id": "181cea9c-c58d-42b0-838f-bd3e5139a4e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7909
        },
        {
            "id": "a965937e-440d-4134-9079-75289bef37b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7910
        },
        {
            "id": "4415d5b8-d971-48fb-93b8-938b2b7051c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7911
        },
        {
            "id": "c104c740-b37d-4a12-81eb-0abdbd0144b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7912
        },
        {
            "id": "ecc4a01f-eb25-439f-be7e-eb4fcbd8f8ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7913
        },
        {
            "id": "8f0e5733-4348-405f-a915-eac684d731e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7914
        },
        {
            "id": "2110f9bf-64bb-4906-b046-718f7a43c3f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7915
        },
        {
            "id": "bc583aa9-6ae0-41de-be01-16d276873a53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7916
        },
        {
            "id": "6d50b2a9-9a8a-4028-8bcf-13be354ffe4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7917
        },
        {
            "id": "75d7a04f-2b24-41cb-bc75-4a1d97099509",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7918
        },
        {
            "id": "8fc4e5b1-033b-4ba3-baa7-8bd5a48c2f3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7919
        },
        {
            "id": "09d5a004-c8a4-4755-8a43-68846d90119d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7920
        },
        {
            "id": "1d3cff09-4615-43b7-a1e5-c7efde16b01b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7922
        },
        {
            "id": "0bb9b9b5-2e21-469f-8dc3-a7294c68292c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7923
        },
        {
            "id": "a93c8baa-ce5d-455c-9495-92312d1945af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7924
        },
        {
            "id": "dc64233c-7857-4a1b-b977-79562896f09c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7925
        },
        {
            "id": "36aad79e-1958-4bf9-834c-a2b7bd303e95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7926
        },
        {
            "id": "cd2dde36-3da1-42e0-8d83-1743e65824aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7927
        },
        {
            "id": "cb363867-4376-41ee-bc49-223ff30a8976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7928
        },
        {
            "id": "2a85fed4-a872-4628-bb64-33f600e8236e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 7929
        },
        {
            "id": "ebea7aaf-be71-4905-b3a4-ae6a4b2300f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8208
        },
        {
            "id": "eabf5f14-a3ff-4b66-bc4c-ab0b73bd91c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8209
        },
        {
            "id": "2f2f318e-91f8-48ae-beb8-2419113c975c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8211
        },
        {
            "id": "9581626a-da6c-401d-9509-c306f55dfe94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8212
        },
        {
            "id": "2808a504-9a64-4955-b2ac-88b2b9fa1ddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8213
        },
        {
            "id": "866e815e-d446-414b-8606-91c78703d892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8216
        },
        {
            "id": "36eae324-bc95-4d90-b42e-42bcf0766baa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8217
        },
        {
            "id": "bcaa963f-35fd-4a0d-8838-9d5ddbf39415",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8220
        },
        {
            "id": "3c20605c-6354-45fb-b3f7-0b8cb8e3fd7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8221
        },
        {
            "id": "c53b3f96-3096-47eb-9f2b-8a02ce161ded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 118
        },
        {
            "id": "97c90fb7-cf86-49a8-a3e3-98223880c914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 121
        },
        {
            "id": "4445d067-9930-44c9-b7ce-951ecc7b780e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 198
        },
        {
            "id": "f75a1453-f739-4163-9c92-62d8beb700b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 253
        },
        {
            "id": "72fbc53b-4ec4-4e59-b10a-4a1e06ef03d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 255
        },
        {
            "id": "1996b53f-ff62-4da5-9167-e70561f1aac5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 375
        },
        {
            "id": "5af497eb-b9c7-4e16-8623-a0005077bc58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 508
        },
        {
            "id": "ca049d89-b073-4c66-afd2-404b917002da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 7923
        },
        {
            "id": "e758b906-8f6d-4181-83bb-31d1b9a86885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 7925
        },
        {
            "id": "ff2134ea-f63f-4eef-944d-8f815c0d25a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 7927
        },
        {
            "id": "e702f645-8ffe-4282-aec8-6e7383c264f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 7929
        },
        {
            "id": "64c443b9-6668-43d5-991c-ac196f63fbce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 44
        },
        {
            "id": "e7b0a28d-725c-4b8c-ab30-5484454910ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "5a225776-eb9a-4173-968c-86a24cdd4956",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 46
        },
        {
            "id": "9806cb69-aa74-445d-82de-b36d2f5f9c34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "78e3086f-89bb-4ec6-92b3-733bf7a89e5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "c6186d0d-51c8-455e-897e-c4699dfcc4c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 65
        },
        {
            "id": "58d25b0b-dd17-48ac-90ba-edf4ec3d51d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "4e11d4ac-f378-4b9b-81ec-37b23b14956e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "51231bde-4413-4d54-939e-ec563c0db072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 100
        },
        {
            "id": "ad848149-885f-458f-972c-a746e0368f95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "7b49bd91-632d-404d-823e-aecc772cd25a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 103
        },
        {
            "id": "8401ca09-e33c-4c82-86a0-2edb71932922",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 109
        },
        {
            "id": "5966389f-f619-45e1-a1bb-9b896b2890d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 110
        },
        {
            "id": "eb4de6dd-5923-47d5-a043-4bd837fbdbbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "69e75a3f-be18-491a-bb9f-be5ea687be5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 112
        },
        {
            "id": "765946dc-67d8-452c-940d-9894c07dd711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 113
        },
        {
            "id": "c5c7ce73-8643-4b39-b631-c73f59115a67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "5431cd90-4927-4a3a-b691-4159fbfee342",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "1a5bcb4b-23d3-410f-910d-7f710994bbd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "9d685058-60f3-4dda-a27a-c8302bbb0f92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "2a2d5238-b6fa-4cc0-9f70-3d0e206b1300",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "fa16f27b-78aa-4a5b-acfe-9e870d09a11a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 120
        },
        {
            "id": "299b73c1-aa6b-40ce-9b07-8e04f0b1e2e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "f9af1e83-5467-401a-8bd4-695ead4c36f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 122
        },
        {
            "id": "087cb02c-4d34-4176-be2f-dda46a87144a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 171
        },
        {
            "id": "0f8a716e-a146-44d0-bd9f-ac74d11804fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 173
        },
        {
            "id": "8a51b61f-593d-4ec4-8879-94499718f15f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 187
        },
        {
            "id": "a62cd0c0-c42e-404c-ad8e-93e7c329bec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 192
        },
        {
            "id": "3cc45811-4c86-408a-8407-90fa90117c7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 193
        },
        {
            "id": "bacf87c1-e80f-44e7-abae-14dadd4ac807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 194
        },
        {
            "id": "869a8378-956d-45ed-b749-cb0699a4a9df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 195
        },
        {
            "id": "6a1dc6f8-45ad-4eeb-8b1a-d97a37efe8c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 196
        },
        {
            "id": "7309bc34-9166-4954-9ad6-fa2885bb4b51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 197
        },
        {
            "id": "5ab97e4d-a9ed-4273-889a-80e3e4ec6504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 198
        },
        {
            "id": "3c65c449-2464-4a25-89d8-95cce4fab718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 224
        },
        {
            "id": "7b83a19a-7c6b-44e5-b4c1-9eb797e41625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 225
        },
        {
            "id": "7270d2a4-2899-4db6-aef9-a32a7eae3f18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 226
        },
        {
            "id": "697405bf-4008-4585-b7c7-e14a9a911aad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 227
        },
        {
            "id": "6124675f-8d28-4265-9116-f5126560ff10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 228
        },
        {
            "id": "5e9ff903-f7fa-403d-b3e0-e0c2179d966a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 229
        },
        {
            "id": "9ca8283c-3c3c-4d65-b415-ea67d5d37e28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 230
        },
        {
            "id": "551513ac-31f2-4e0f-9169-20a087503cfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 231
        },
        {
            "id": "5cf733e2-87b8-441e-ac27-39fc631ffe2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 232
        },
        {
            "id": "ec5ba4b2-3eec-4fb6-a353-f855272f3496",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 233
        },
        {
            "id": "2611b67b-24be-453e-bbd0-d705fc2e1ca2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 234
        },
        {
            "id": "8fb498ba-a0f5-406b-9858-000dbac268c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 235
        },
        {
            "id": "c8d45317-9f3f-4be8-a20f-15eed182b0a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 236
        },
        {
            "id": "e1225479-495c-40a3-be4e-b9bd6556eb73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 238
        },
        {
            "id": "0a497d47-1fd6-4f86-805f-79a600e3e2a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 239
        },
        {
            "id": "2dfa7ac1-36cc-4387-a2f3-273b8be4d093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 240
        },
        {
            "id": "36f3c6bd-ef53-4151-aeeb-6450bf197055",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 241
        },
        {
            "id": "127c9bc6-524b-46c0-8713-0f5968b22062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 242
        },
        {
            "id": "1811cb78-4d78-4a92-acd7-a8952a1f11da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 243
        },
        {
            "id": "6d185216-1e4e-420c-a5a4-30c28f70f857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 244
        },
        {
            "id": "dac550e2-3748-4bae-98da-5765389b4917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 245
        },
        {
            "id": "e834d508-93fa-4cba-b84e-b2f40a9109a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 246
        },
        {
            "id": "ba6fb54d-819b-4346-865a-6364321a63e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 248
        },
        {
            "id": "e853f13c-5fe1-4c03-910e-ae7163769ed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 249
        },
        {
            "id": "aebdb29b-7fe4-45f5-8107-02d96c087a98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 250
        },
        {
            "id": "33074595-e987-4e6f-a10f-8fd3a4f2ed22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 251
        },
        {
            "id": "417556a0-cf20-42a8-8c69-cb27c2681b28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 252
        },
        {
            "id": "e5b80ed9-6f5d-4701-8fa2-9e74b7eefc87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 253
        },
        {
            "id": "fc0c11d3-fe00-4906-8ce6-b03c6767250c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 255
        },
        {
            "id": "61f2ce4d-c6c8-4bdb-948d-cb87e9e27895",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 256
        },
        {
            "id": "f68d535e-1256-4085-892f-84db6e013a9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 257
        },
        {
            "id": "e731f4c1-61a7-4d1f-8f96-4197aa604059",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 258
        },
        {
            "id": "71721ea6-5c4f-4f8b-a504-cb1666594b16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 259
        },
        {
            "id": "fca2bf5f-44a8-4552-a991-867b6129f200",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 260
        },
        {
            "id": "cd128a96-60e6-470e-a605-a325e9df82a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 261
        },
        {
            "id": "7a56532c-94a0-42f3-84a7-989a93fd4046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 263
        },
        {
            "id": "7fd953c2-2344-461f-89d8-2849668ff4d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 265
        },
        {
            "id": "89545fc7-1df9-4307-a0a6-829842cf6c8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 267
        },
        {
            "id": "2bd5f65b-0e2c-4cc6-b618-69ce072c7d4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 269
        },
        {
            "id": "133c0765-9512-4de7-91a0-4bd09f5e0cff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 271
        },
        {
            "id": "302a423d-f67e-45fe-82ae-324dfd409d8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 273
        },
        {
            "id": "18b9ae7c-201b-4033-a038-345710fd13f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 275
        },
        {
            "id": "11c195d8-e60a-403a-9115-3c1e64a8b392",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 277
        },
        {
            "id": "7d0a79e4-f73f-437c-8fa0-ae2312fbb54f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 279
        },
        {
            "id": "274e9d74-720b-4e6b-9138-6bd2134be7d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 281
        },
        {
            "id": "ff33d962-e790-4018-a93a-6b4c4a56f48d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 283
        },
        {
            "id": "b0611f38-e404-417d-bcb7-c3d94b8c7fec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 285
        },
        {
            "id": "9172d330-ddba-4f20-bc73-937c0ae7ec3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 287
        },
        {
            "id": "a8146011-24a6-4799-921e-9495c1164c95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 289
        },
        {
            "id": "f004fa72-2119-41a9-93ff-95b226d5b719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 291
        },
        {
            "id": "c9eda8b0-d3cc-404e-82f8-3eab9b9d8d42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 297
        },
        {
            "id": "068245ac-62df-4b97-b57b-b317178a040e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 299
        },
        {
            "id": "3b94a6ba-50a8-4159-b9a4-94d3419e4c28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 301
        },
        {
            "id": "b3be208f-ea69-4a3d-817d-063eeff6080f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 305
        },
        {
            "id": "d89d2311-e268-4be2-bdcf-025d72faac32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 309
        },
        {
            "id": "42ebe4e0-0bc7-499f-993f-8991b56d9412",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 312
        },
        {
            "id": "0a8099d5-fa2e-4745-8b91-e8eaa69ba43b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 324
        },
        {
            "id": "a04c7c22-f7e0-4ca9-a05b-7cf80275b2ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 326
        },
        {
            "id": "05e8f20b-5b78-4d74-91be-347398c350f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 328
        },
        {
            "id": "7f6c4199-a421-4ac2-b4ca-ee2576af6626",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 331
        },
        {
            "id": "7abcbd1c-fced-411b-a770-d8fc4e3a9e92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 333
        },
        {
            "id": "3df10836-88f6-471a-8bbb-028c5f7bc82c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 335
        },
        {
            "id": "53cc05b9-f629-4231-96de-d453ca81d168",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 337
        },
        {
            "id": "bcd925ce-760d-4966-8f22-80b44f51f85f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 339
        },
        {
            "id": "9d9ffdf6-f8e3-4883-9837-32ca7139fc64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 341
        },
        {
            "id": "a866dc10-6853-460a-8aa9-4d2707785740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 343
        },
        {
            "id": "0ed48f08-5fad-41a5-8d0f-3a3e40685548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 347
        },
        {
            "id": "5d3ff368-ab6a-48d7-a93c-3964f971138f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 351
        },
        {
            "id": "bec5c03f-d50b-414d-ab47-d3849ff1c784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 361
        },
        {
            "id": "46cdc0aa-7990-407a-805c-31cce548cf9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 363
        },
        {
            "id": "91bc3589-8e8a-4309-8620-b589cd77aa33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 365
        },
        {
            "id": "9166b66f-1a49-4211-b179-f35270218823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 367
        },
        {
            "id": "86c32bf4-5b2f-483d-a8f9-e625a60afd78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 369
        },
        {
            "id": "622249a3-11e9-4d28-b39d-acee7f67b826",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 371
        },
        {
            "id": "b92c3bd1-e382-4e60-86f3-5635d3a06c44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 373
        },
        {
            "id": "217422af-88c2-4ca6-a39a-83e26be247e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 375
        },
        {
            "id": "c8ee4046-6951-49fd-8384-07c6994b9dd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 378
        },
        {
            "id": "29a169d4-14a0-4c2a-a4c3-9fdbf99c04da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 380
        },
        {
            "id": "fd78ed1f-64a2-458b-96e3-58892716c0cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 382
        },
        {
            "id": "6b40110f-011f-48d6-ad98-b5734598ff76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 417
        },
        {
            "id": "097bc5e8-d56f-4cfb-8e1b-04b2363b64ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 432
        },
        {
            "id": "b40fabd8-bf04-4b55-b9aa-e1d3a1f941a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 506
        },
        {
            "id": "efc3e9bf-41ab-41dd-b6de-3d8780be6c17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 507
        },
        {
            "id": "5de6b3d2-c9be-4522-a2a4-2e857b46a468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 508
        },
        {
            "id": "a07361d8-5827-4555-a955-9c5eff06d5d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 509
        },
        {
            "id": "e3e40443-c33d-4bb3-8973-7556d295b7ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 511
        },
        {
            "id": "31481404-9eab-40c2-83ad-4ec725aef373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 537
        },
        {
            "id": "43b5b976-55df-4a6f-ab43-fc9bb1ed3517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7809
        },
        {
            "id": "772bb65e-9201-436f-84a7-7221dfa78aa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7811
        },
        {
            "id": "7e564407-bd90-4d1b-be5b-fb4dc41d1e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7813
        },
        {
            "id": "66ebed26-0977-44b8-a6c3-73df99e638b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7840
        },
        {
            "id": "1040a220-a2b8-4df9-87f5-43b2a90c8b5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7841
        },
        {
            "id": "84a2b46f-259b-48fb-b0e7-333e1559447e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7842
        },
        {
            "id": "3b97d7b2-4fff-4920-ac68-ba998aa21be1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7843
        },
        {
            "id": "e124d72a-b246-4890-abfa-9cb056377cf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7844
        },
        {
            "id": "ba24abe7-9d9f-48ee-9ba3-4656bee232bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7845
        },
        {
            "id": "8cfd1791-1ba4-4b68-9d6e-2870b8e0a938",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7846
        },
        {
            "id": "d39e80bd-c7a8-4e24-be06-44dcb924d2bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7847
        },
        {
            "id": "64d58549-2ee5-4374-8748-bafcec7f136b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7848
        },
        {
            "id": "62ffa6be-0e05-4655-813b-68479750d85c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7849
        },
        {
            "id": "3b77a60b-bf02-400a-a8c0-558f2277e3b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7850
        },
        {
            "id": "4922546a-512e-4964-9ed6-c1eaf8cc50cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7851
        },
        {
            "id": "1884fbbf-46a5-4b0e-830f-4dc2bc764786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7852
        },
        {
            "id": "27d4907c-7117-45c5-9b6b-5462e16c9d3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7853
        },
        {
            "id": "36250609-e2ef-4a9f-9426-d4d7a1b426af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7854
        },
        {
            "id": "ac92efee-8e76-4521-b27b-96b7aa8000d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7855
        },
        {
            "id": "bc72f245-0da7-4ea4-95b0-fc2882cebfbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7856
        },
        {
            "id": "e079494d-89db-4060-ba0a-cc3e9680c3c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7857
        },
        {
            "id": "807eee85-321a-40c0-8921-e45e1cf7e3fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7858
        },
        {
            "id": "54fd2ba6-7560-483d-83ad-f7556f9a8df1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7859
        },
        {
            "id": "d48cd9cb-dd27-49c0-9d5d-47f5406d48bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7860
        },
        {
            "id": "aa7371ab-f1b5-49ca-9649-572167b71c7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7861
        },
        {
            "id": "8eb58abb-4c07-4ced-b33a-875cb2805c34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7862
        },
        {
            "id": "f680a2d9-6fe1-41f6-a876-7bdcb9201958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7863
        },
        {
            "id": "fd166997-a083-421b-949d-ec6b95e4368a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7865
        },
        {
            "id": "5f65389b-ee84-4eea-b467-59bf0b44a2d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7867
        },
        {
            "id": "8c721ed2-57f7-46ff-8ca5-458c6fc10a30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7869
        },
        {
            "id": "fafd26de-a971-4fa8-908e-caea70ecc991",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7871
        },
        {
            "id": "e39c4c64-d09a-4ab1-9fd7-329aa67ab9f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7873
        },
        {
            "id": "e7e4e431-3ebf-45e8-8903-0d6431068a24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7875
        },
        {
            "id": "702fb1ba-8d28-414f-ae3e-429e4f7fb5a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7877
        },
        {
            "id": "7188f3d4-a78c-4627-a036-b52c5b951a92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7879
        },
        {
            "id": "55792ed0-39a6-424c-805c-b65f2bc04c72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 7881
        },
        {
            "id": "b7148344-1c56-4c26-84ea-e5e9be03be8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7885
        },
        {
            "id": "7617fb78-273d-4580-8a50-7daf73bf0592",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7887
        },
        {
            "id": "91572fd7-c499-42a9-8201-b2c07b6d8c73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7889
        },
        {
            "id": "d9a53662-e9ac-47fc-bca6-d57ed2e83731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7891
        },
        {
            "id": "0a4900d8-a4f8-44fc-9139-664a039d60af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7893
        },
        {
            "id": "77809cc3-f455-49b2-b849-0e27f69aef3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7895
        },
        {
            "id": "15f556a0-f68f-4c83-81e7-1a19a1c9315e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7897
        },
        {
            "id": "b552e3c0-fee9-4300-a3c1-d9cdf79990ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7899
        },
        {
            "id": "bbe4969e-258e-40ce-8b3c-df44bae538e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7901
        },
        {
            "id": "169b9a6b-6011-4458-95cb-e87d1009fa04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7903
        },
        {
            "id": "a108dc26-22b2-4ff5-999e-d55a729274ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7905
        },
        {
            "id": "58d0cd13-f0d3-4ba1-9f6a-dac5e7e2aeb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7907
        },
        {
            "id": "3234a836-53ff-4169-89af-ce10885b3775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7909
        },
        {
            "id": "2583cc3b-05e6-413a-9fdf-9b0a4ae9cc82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7911
        },
        {
            "id": "39e7352d-f02b-4360-ad1d-017ba15b50e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7913
        },
        {
            "id": "43548f12-a5fc-4dea-aebb-2158fd85aa69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7915
        },
        {
            "id": "2adfbf72-2fa8-4bf8-9e0c-7a80b0379bca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7917
        },
        {
            "id": "fa45553f-4585-4701-9282-a552c91d2bd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7919
        },
        {
            "id": "05be7ec0-6f27-44ac-94bd-7b171d24af42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7923
        },
        {
            "id": "ce17bb38-835c-4c33-9428-544c382d981a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7925
        },
        {
            "id": "87e5327a-7018-40e6-9684-9a599b2ad26e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7927
        },
        {
            "id": "a08ec900-dff3-49aa-a4cb-1344ea8f3349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7929
        },
        {
            "id": "b90c3a9f-2661-42d8-80f6-63e7380597be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8208
        },
        {
            "id": "2e39b1d5-1f96-4954-9b61-11bb98665451",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8209
        },
        {
            "id": "7297ef9c-e0d0-46f4-8fe3-d50f56e5853d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8211
        },
        {
            "id": "9af17b5a-dee6-4931-a743-a17013a78a42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8212
        },
        {
            "id": "2fd43f54-228b-4994-9c47-f5846baf0925",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8213
        },
        {
            "id": "0e053ec5-0bbd-462c-a93e-ad83d800ca05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 8218
        },
        {
            "id": "5a419e89-8842-4c5c-a532-440ee0f16bc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 8222
        },
        {
            "id": "bdc5ef83-b6de-4b0d-ac81-54faecca4192",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 8230
        },
        {
            "id": "49288d5a-dbcd-4711-9240-cd5e8810692c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8249
        },
        {
            "id": "a55beee3-8bca-4e48-8611-047bc692d063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8250
        },
        {
            "id": "1caac45f-aeeb-4956-af2c-4e2f7a1f2a9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 44
        },
        {
            "id": "5c49ace7-7beb-4aee-9ace-3638d3db1a2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 46
        },
        {
            "id": "556bdb51-47c6-4f5f-80c8-f49458c8fa42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 65
        },
        {
            "id": "484ea285-420b-4859-bfb7-9dff73ff913e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 192
        },
        {
            "id": "ae071aab-54da-48c0-8bae-c7805d77a59f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 193
        },
        {
            "id": "7276d3e4-12c9-4929-97d2-9c761444b34b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 194
        },
        {
            "id": "f4220dfd-5b97-4448-8681-5cbec31e213a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 195
        },
        {
            "id": "d4a49372-6bcc-4050-aa42-deb56167a5d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 196
        },
        {
            "id": "0502be28-eb5c-45af-a9a3-1b38e9c94097",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 197
        },
        {
            "id": "d498a5c0-6836-46f5-ae52-f8aed0eb9992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 198
        },
        {
            "id": "1fb2b4e0-d1d4-488a-b423-692214b23df1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 256
        },
        {
            "id": "ceb75c44-c3bf-4818-b5bb-4829af2f2232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 258
        },
        {
            "id": "611bf930-efa9-45c3-9da9-bbb6fadf2f32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 260
        },
        {
            "id": "f1865cd2-6689-4a6c-ae85-51eeeb606d3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 506
        },
        {
            "id": "93eeffc2-8e8b-4fa0-8e58-e5ffbff5ac06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 508
        },
        {
            "id": "29db1f4b-c669-4f50-b392-d9a4f2011953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7840
        },
        {
            "id": "94162f6b-ebbe-4449-8ff8-910212d9ffff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7842
        },
        {
            "id": "dda384a2-842e-479a-9f40-1636e4f7f220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7844
        },
        {
            "id": "f3874f5e-5d67-44e2-9bdc-c95f20bb444b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7846
        },
        {
            "id": "d9c7caea-54af-431a-8029-3b0cd27da4b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7848
        },
        {
            "id": "0b4643aa-bb7a-49b9-b889-1bd74a939040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7850
        },
        {
            "id": "d2ba7dd8-6b55-46ea-9aab-5813c55a9838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7852
        },
        {
            "id": "5c2c3886-9f1a-4b0f-914e-b0b9399717da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7854
        },
        {
            "id": "5ebed17c-892e-4ebe-8009-ee7a0c0ed851",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7856
        },
        {
            "id": "77974b79-44ea-4de4-90a3-79b6fce31ca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7858
        },
        {
            "id": "c122dba8-5899-423f-b953-39c549241f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7860
        },
        {
            "id": "5bdcf6ac-bcdc-4bce-aa1c-6758eefd2ec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7862
        },
        {
            "id": "8fb792a5-d32d-407a-98a4-725b1dcc8a4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8218
        },
        {
            "id": "7b7ef40e-45e1-4694-9e8e-dd2197eb2dce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8222
        },
        {
            "id": "850cd073-4b6d-4a8d-9187-37caf04ce456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8230
        },
        {
            "id": "6a361d47-a04c-4311-bbb8-f06000ea0e7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "e3657c5b-b715-4dcf-a1ae-fc9f1d90f079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 45
        },
        {
            "id": "7eeef9a1-28de-489f-8f82-14db543fe51a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 46
        },
        {
            "id": "8fe0fb3b-e494-4339-9d0a-4981a92ad7b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 58
        },
        {
            "id": "cb67eb24-37cc-471f-bb3a-3941a6134ac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 59
        },
        {
            "id": "032f02fb-2219-42fe-a0e6-266c1d7b26d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 65
        },
        {
            "id": "2a1d9875-e89e-4fb6-8e8b-d70e81aa6102",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 67
        },
        {
            "id": "539e9a8b-9691-4052-9066-1df371e14731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 71
        },
        {
            "id": "6f602c53-8df4-4880-b0c1-4e26d1bf77bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 79
        },
        {
            "id": "e7254a66-dfa6-4749-a387-03bd9c02cc90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 81
        },
        {
            "id": "2c22c00e-1616-4a4d-8f29-138055119ee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "508d9ae7-5d36-49a0-a24c-c7197c38b72e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 99
        },
        {
            "id": "38917c7a-7fe5-42dd-b4e9-6ec977a720e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 100
        },
        {
            "id": "fe3a64c6-bf81-400a-bb42-861241932752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "df003675-b9ee-4ce0-969d-9144d45782d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 102
        },
        {
            "id": "ceb11d8f-aaaa-47ef-81fd-fe0a26c52490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 103
        },
        {
            "id": "a5d36ab9-65d1-4246-9e9d-81421c997c3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 109
        },
        {
            "id": "96aa1591-c8d4-4d04-993f-66c0677bbdf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 110
        },
        {
            "id": "b4e5e62a-af38-4ee8-ae51-7bc7a79cb923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "a3794bfd-261a-4e3f-a7c5-eb0e06aa3b67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 112
        },
        {
            "id": "ca60aa71-0865-4898-89e7-cfd4ca1271ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 113
        },
        {
            "id": "2957a06f-fcd5-44b7-96a7-3a6a40f059c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "172ddd60-a0b1-4c6c-9044-9a32c4242562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 115
        },
        {
            "id": "d3d7c414-8290-4d53-9bde-2086f2f8c889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 116
        },
        {
            "id": "085ab8c5-4e25-424d-8f75-c7768fe97520",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 117
        },
        {
            "id": "43a6edc0-7be0-4b42-b715-21a4c552aba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 118
        },
        {
            "id": "5d14043a-af15-44b4-bfb9-330aa5918f13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 119
        },
        {
            "id": "3c084de7-14fe-4c63-bcf8-07cd221279d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 120
        },
        {
            "id": "6aba638d-ae93-4f28-a440-5e1544f330f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "2c5cc278-eb67-4296-b261-ce7552d287e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 122
        },
        {
            "id": "ff9a7da6-df47-4f37-9a4e-f294fa5e6c0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 171
        },
        {
            "id": "96b62472-984a-4221-bcbd-a6a3e365b679",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 173
        },
        {
            "id": "09adb9bf-ee96-47cc-bc67-ec0ca57d1b23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 187
        },
        {
            "id": "1e585b09-8610-47d1-865a-ea6cf2ad3e47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 192
        },
        {
            "id": "7f8c65c6-9e33-4bc1-b771-9a79b402e84e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 193
        },
        {
            "id": "7b0fcf0e-0b2c-41f2-9f97-aa71e7f077bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 194
        },
        {
            "id": "0de26205-937d-4808-8a0d-873923d21bc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 195
        },
        {
            "id": "979afefb-7435-4cba-8bf6-ab29f2c4ae33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 196
        },
        {
            "id": "94b53552-8976-417a-875d-df34a1fb8504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 197
        },
        {
            "id": "8b17ee22-b877-42f8-8985-b38b542bec90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 198
        },
        {
            "id": "ae9eba67-5ae7-47fa-a691-c0e83b533924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 199
        },
        {
            "id": "e65f1160-431b-486d-8e9b-a4543f6a0e90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 210
        },
        {
            "id": "da11ea5c-8ded-4c3b-a2c0-756b8a3c9b1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 211
        },
        {
            "id": "7e9ebba2-b2bf-4057-bf3a-1dbb4feb57a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 212
        },
        {
            "id": "33d802f8-0097-490e-84aa-30afdf8f202e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 213
        },
        {
            "id": "7f69167c-a834-4498-94db-38cb8dd47081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 214
        },
        {
            "id": "f9c19121-3c2b-4bff-9756-6a5b912493f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 216
        },
        {
            "id": "0d1b1030-045c-4137-9478-dce20a9a2ed7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 223
        },
        {
            "id": "249e8def-c111-4dea-93b9-5f46ecbc1f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 224
        },
        {
            "id": "5d0624d7-5e18-4236-a40d-558f9666762f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 225
        },
        {
            "id": "969ad19c-3549-48f5-a508-3630013c5e50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 226
        },
        {
            "id": "2e980369-19ed-414b-974d-9f7ece37d915",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 227
        },
        {
            "id": "cea9953c-4d1a-4b71-9d64-a2fb2665f1e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 228
        },
        {
            "id": "8c0e5dc9-bf66-4e25-ba1c-a014c1917958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 229
        },
        {
            "id": "b904c644-6352-48af-8041-ebd6e214d94e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 230
        },
        {
            "id": "53cf4afd-d1f2-4c8d-99e7-53f116057c4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 231
        },
        {
            "id": "a29e2a55-4eb5-497f-b19e-0cffa5d71703",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 232
        },
        {
            "id": "24d3599a-c7ca-47b7-8599-73d949098440",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 233
        },
        {
            "id": "2b87fae1-d700-40d6-b0d4-8eac44141ed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 234
        },
        {
            "id": "1b9c87f6-cae2-474e-8851-ddbff895986e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 235
        },
        {
            "id": "f16bd38c-a7ad-4d55-928c-997962beec80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 236
        },
        {
            "id": "352a166f-c2ae-4955-925c-8267aa964571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 238
        },
        {
            "id": "6d69042d-f2c9-4a75-8402-8f7ac50e70db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 239
        },
        {
            "id": "2d3813af-a7ac-4bcd-9137-10ff1e5f399f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 240
        },
        {
            "id": "2888dbde-7dd5-4a46-8c58-fd4d4c5bce49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 241
        },
        {
            "id": "1a207d31-f45f-465b-920e-b027ae0d870e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 242
        },
        {
            "id": "0c21e966-e4d5-4d8f-8afc-24f958f00ec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 243
        },
        {
            "id": "568a21aa-d433-46ce-980d-d1af1b78e941",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 244
        },
        {
            "id": "f35efa29-7df9-4af5-abff-87c765cd428f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 245
        },
        {
            "id": "50602375-0b8e-4e5f-823b-7e9d83aa709e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 246
        },
        {
            "id": "73cc5e42-ec1a-4563-91dc-9348736dfcf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 248
        },
        {
            "id": "d260eeaa-04d3-4be6-b181-7d66969c02ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 249
        },
        {
            "id": "67eb5cf1-0e8b-46c8-ad5a-ab9db736c993",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 250
        },
        {
            "id": "03362276-db5f-452f-864c-07bc78f64eee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 251
        },
        {
            "id": "77ac7cc8-e109-4f17-bea7-504cfb0f4b28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 252
        },
        {
            "id": "6c33526a-8a9e-4ad4-8980-b2e9d16986dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 253
        },
        {
            "id": "7e4bfd50-ef15-44be-a058-0bb3790818ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 255
        },
        {
            "id": "ecfb7630-8ad0-4159-8e25-e086c653e280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 256
        },
        {
            "id": "fbc77643-cf20-4090-88e1-78884940dc0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 257
        },
        {
            "id": "bc2360dd-559f-41d1-8016-97b97bcb8da4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 258
        },
        {
            "id": "ad422e1a-f674-4143-9568-6daaa066c81c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 259
        },
        {
            "id": "9624ce73-22a0-4102-a721-28d03d5994b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 260
        },
        {
            "id": "3c104eff-f8eb-43ae-9da2-3f748a908875",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 261
        },
        {
            "id": "ef7fead3-e55e-4d29-914d-b1a92515ba7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 262
        },
        {
            "id": "779be3f3-fa0b-4932-be6a-6d901dcc1cf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 263
        },
        {
            "id": "6951307f-4099-42fd-b663-f6fbb78c0684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 264
        },
        {
            "id": "b26bb072-36f0-47a3-8af3-a9a50729cb61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 265
        },
        {
            "id": "2d0290cb-df38-4227-a03e-91f731534f5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 266
        },
        {
            "id": "6045005d-8667-45c4-b464-89a310e9f584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 267
        },
        {
            "id": "129f027e-7b84-4792-afcf-814e9973781d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 268
        },
        {
            "id": "eecf8b24-86e4-4352-92af-fe0beb2bcec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 269
        },
        {
            "id": "c35e8785-4828-457c-9440-b7c6e8100669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 271
        },
        {
            "id": "a3b9c176-1fa4-46ae-8a98-9a51eddccfd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 273
        },
        {
            "id": "802f8150-f48f-46d2-b43d-41eabd56cba4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 275
        },
        {
            "id": "6e5977eb-9e4f-4996-8edb-724d5611b1f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 277
        },
        {
            "id": "5591ffc0-3d65-4546-a099-678fe2394d5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 279
        },
        {
            "id": "da7a7a85-cd66-49f5-92bc-a3a904e295ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 281
        },
        {
            "id": "7e511aae-6c59-42b7-929b-2131a4d03226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 283
        },
        {
            "id": "de288895-b765-46fe-94a2-4ee9271d0833",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 284
        },
        {
            "id": "8670b5fc-08bd-4901-8325-2be171c895d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 285
        },
        {
            "id": "c4747573-741b-4429-ae65-2e05508b88b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 286
        },
        {
            "id": "7a9bf443-7a66-4121-bf48-961b82797646",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 287
        },
        {
            "id": "49ea989f-5db7-4726-bf85-41c3e4060560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 288
        },
        {
            "id": "79d6c607-f4cf-4379-b423-f20680e056b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 289
        },
        {
            "id": "a8cb39ec-fb3f-402e-ab8f-c7998c676cf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 290
        },
        {
            "id": "47161ef0-7318-4596-b99a-cc0f2abf967c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 291
        },
        {
            "id": "fca39fb4-b1d1-482d-9cc0-e3c1802ec4cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 297
        },
        {
            "id": "dcec09af-e300-44ac-b807-d95bd73fc7c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 299
        },
        {
            "id": "ed3d8105-f7cd-4a95-b911-1092fdb02fe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 301
        },
        {
            "id": "6d40ef0e-4f3b-49ce-b033-bbc87427668a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 305
        },
        {
            "id": "9dcba9a7-7927-4ab7-9ed6-b4ee2e98c27d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 309
        },
        {
            "id": "ec666b23-7eca-40c2-a386-e0dd164dee6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 312
        },
        {
            "id": "b8fbecee-29bd-43e9-a671-ac015678ab9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 324
        },
        {
            "id": "697a0157-6443-42bc-a50d-695e220e25c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 326
        },
        {
            "id": "2121b0b3-29df-4bc2-af01-f85cf74d4032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 328
        },
        {
            "id": "cebbe7a3-30e7-4193-9c0c-341e55de015d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 331
        },
        {
            "id": "ead003ca-8b3a-47af-992b-b5f96d0a8e27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 332
        },
        {
            "id": "dae9e805-7b07-4403-b69c-020a99e0969c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 333
        },
        {
            "id": "6c712c02-73c4-4e53-9fcc-c7803c493c98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 334
        },
        {
            "id": "ec340bf5-d058-446f-817f-999a04dafe12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 335
        },
        {
            "id": "d5663be2-e251-4e72-b1cb-67b755ae28b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 336
        },
        {
            "id": "bee387af-744b-4f65-b0c7-2349a93ffb5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 337
        },
        {
            "id": "7a9d2064-eee4-4562-8690-c945ae8401a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 338
        },
        {
            "id": "f734b96f-cd4d-4f17-8435-7b55c0826cb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 339
        },
        {
            "id": "1d385b78-179e-454f-85c8-3a2f42568984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 341
        },
        {
            "id": "83b93a7e-2926-494e-9032-0b7ed546b54f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 343
        },
        {
            "id": "13b6431c-1c62-4bd0-b448-a5412debbf67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 347
        },
        {
            "id": "f142a714-b970-4561-946e-9abaf4534da2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 349
        },
        {
            "id": "9b1a69a8-e449-4104-ab60-26b28aee9f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 351
        },
        {
            "id": "cda16617-9d6b-4fa3-9f42-aea8db94c439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 353
        },
        {
            "id": "e04e80e0-d5bb-4c66-9c9d-e31111190a45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 355
        },
        {
            "id": "6a60d199-1f3d-400f-821e-afb9614122dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 357
        },
        {
            "id": "538ba73f-7a2a-4e04-83ed-fc864c677fa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 359
        },
        {
            "id": "f4dffd31-ce99-4996-9085-195dea00e89b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 361
        },
        {
            "id": "016dd07c-1110-4045-8e66-0d6d68129cc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 363
        },
        {
            "id": "4288a83f-597e-47a1-89bf-3556cdba1fb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 365
        },
        {
            "id": "4f833725-b31b-41e7-9e3c-4c2fcca69683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 367
        },
        {
            "id": "0451cdbb-a7c9-49d6-9151-666beefbecc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 369
        },
        {
            "id": "5ccd75ba-5d0a-4596-b0ee-957b3dcb4078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 371
        },
        {
            "id": "6371a157-f9ad-4bb7-a877-e4d7ea763860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 373
        },
        {
            "id": "63b67cda-63a8-40b5-813e-036216a87330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 375
        },
        {
            "id": "af6d045f-b029-4e35-bf07-b5afa08f6d58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 378
        },
        {
            "id": "1449bbd1-e57e-431e-bc1b-9023223a4a39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 380
        },
        {
            "id": "a9382561-a3a7-45ab-b7af-99a0814350d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 382
        },
        {
            "id": "f774d9b9-056c-42f6-9bf7-a4c8986a98e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 383
        },
        {
            "id": "9734b9ce-85e2-40e5-b2b1-9b1a92b22bfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 417
        },
        {
            "id": "b5979fcc-dbc8-4b78-8d5e-f59054a5d348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 432
        },
        {
            "id": "680828c8-3c16-4a20-a64b-7527e07972a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 506
        },
        {
            "id": "a025bcf9-e290-4de5-8ff8-0184b365e9ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 507
        },
        {
            "id": "764b609f-8325-4408-ba83-c543ded03d62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 508
        },
        {
            "id": "cc5855b6-66c5-41cd-b272-9dd871dbe65a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 509
        },
        {
            "id": "c64e9260-1f8e-4e04-973b-f951e2e85e29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 510
        },
        {
            "id": "65a9ff6b-390a-48a6-a0c9-1a932b2c6160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 511
        },
        {
            "id": "1f222af8-0589-4d1d-ac46-c2b274da4dfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 537
        },
        {
            "id": "203a879a-2aee-4e4f-ad25-9ff68baf8883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 539
        },
        {
            "id": "a5829274-10ab-469b-940f-7dfacb8e823d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7809
        },
        {
            "id": "00c46e39-0a72-4f38-b88e-375c82dcf71b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7811
        },
        {
            "id": "5925695d-f8d0-4719-b837-37cab5cb4587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7813
        },
        {
            "id": "f1bd99da-c6f6-4763-a218-1d1a43e3cf31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7840
        },
        {
            "id": "9c7d967c-364e-4f74-b844-794604f84677",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7841
        },
        {
            "id": "bc79175a-fff3-49ae-81e5-47bcebcff119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7842
        },
        {
            "id": "68912512-6d95-482c-8bd8-e278a498c0bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7843
        },
        {
            "id": "f6e7ad56-d449-4f52-b6df-3169ad2fba5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7844
        },
        {
            "id": "b024dd4a-73da-4ed4-9f89-582a7de7ec73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7845
        },
        {
            "id": "f7185387-584e-48d6-8572-92af5dd49b8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7846
        },
        {
            "id": "93a84f76-7474-45ca-9c47-693483dbc9a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7847
        },
        {
            "id": "1fd3bcbb-9de0-4ff9-ac17-ed556e7e8f68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7848
        },
        {
            "id": "059a7362-71d4-46ce-b9c3-0355c05da6a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7849
        },
        {
            "id": "68e29168-c526-465a-a937-45b121ced756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7850
        },
        {
            "id": "85b5121b-fc54-45e0-ac5f-79e5c0646c9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7851
        },
        {
            "id": "db2642db-a2a4-4363-8d70-65a84a97c927",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7852
        },
        {
            "id": "c27643ea-9d40-41c6-9d30-272555cc4b52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7853
        },
        {
            "id": "d3612064-2259-48c9-a877-84062bd05008",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7854
        },
        {
            "id": "c517d724-10e0-41f4-8ef3-be69d7dd98e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7855
        },
        {
            "id": "0d6b09f0-966b-41e9-9f8b-5fb7ba40940c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7856
        },
        {
            "id": "662d1d17-09dd-420d-81d8-aa88f91eaf8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7857
        },
        {
            "id": "9b056bdf-f570-41a8-829a-6c3a8668c6f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7858
        },
        {
            "id": "0445f356-f063-48a3-9218-7622d3c93d48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7859
        },
        {
            "id": "716c37a2-712a-40f5-bd1d-6061788f50d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7860
        },
        {
            "id": "8c037ffd-8a08-412f-9154-dba8cfa85acb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7861
        },
        {
            "id": "ebb3dfc6-054d-4b17-ab18-2f33b1b4f956",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7862
        },
        {
            "id": "b030cb1f-c1e7-4a26-a251-1371d2ded804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7863
        },
        {
            "id": "f2d778c3-2c92-41ff-a8b5-a9672bbc3b4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7865
        },
        {
            "id": "847f9f21-7fe3-4b1a-815f-40fffd3bdf52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7867
        },
        {
            "id": "a6023990-40db-45fa-a89a-981a4460d8b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7869
        },
        {
            "id": "48509396-59e5-430e-bd86-f8e60bbab498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7871
        },
        {
            "id": "70d15b9a-629f-4ada-a40b-2ab4166da3a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7873
        },
        {
            "id": "2ce67bf2-f828-422f-a806-ed474e3d1913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7875
        },
        {
            "id": "c5eaed73-c501-4dbd-91c0-ed26b63401f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7877
        },
        {
            "id": "19453598-a24f-449e-97db-b83851e57df9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7879
        },
        {
            "id": "3bc91018-ca2d-43bc-9386-40ce7d45ba40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 7881
        },
        {
            "id": "9e226a3d-9b6f-4192-bff0-8e3e03ae8e70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7885
        },
        {
            "id": "815d16b7-101d-451d-96bd-57970cce2125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7887
        },
        {
            "id": "d5bc0f36-8b03-416b-990f-8fb47b9e8053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7889
        },
        {
            "id": "b98a4928-b7e5-4c38-b473-7141e8613603",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7891
        },
        {
            "id": "1b4158d2-b3fe-4398-af9d-acce73972e86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7893
        },
        {
            "id": "b7347afa-7a71-4784-9234-2d53014c4207",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7895
        },
        {
            "id": "53293cb6-cdb0-4dcf-8264-25f8e529ef13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7897
        },
        {
            "id": "fb43b438-3b2c-462a-8a05-723d561b3fc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7899
        },
        {
            "id": "dc7fa698-80cb-4119-bfd4-9e4f84fc60e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7901
        },
        {
            "id": "b56247d8-1a3f-44c1-8ed5-a888e5fb4b1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7903
        },
        {
            "id": "eecd1ac8-8c92-443d-8e19-7983423daa51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7905
        },
        {
            "id": "b2dc96ce-ea6b-42b0-b03c-3b365b7ddb7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7907
        },
        {
            "id": "b4b9f039-af71-4ca5-b850-34127047c1fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7909
        },
        {
            "id": "7a3b5e46-886a-454c-97d4-434cacc1e14f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7911
        },
        {
            "id": "03907ae4-5ee4-4024-b92f-020a54b533ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7913
        },
        {
            "id": "4be96906-927d-4165-8250-799daf277c1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7915
        },
        {
            "id": "3d055329-5cd2-4640-9f41-cd84d7693020",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7917
        },
        {
            "id": "52b9225d-1fd5-4b9d-879a-25bd1ca185e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7919
        },
        {
            "id": "c98340d2-2636-42a9-8008-16dab171f55f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7923
        },
        {
            "id": "8c034e3e-31f9-4b24-9283-19b0060bbcdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7925
        },
        {
            "id": "84198231-9d4b-4666-8b29-7eded4e62867",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7927
        },
        {
            "id": "bbe95b12-a702-40d2-859e-30b32d0e239c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7929
        },
        {
            "id": "be4ecb76-10ae-437d-8329-acd730a4efa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8208
        },
        {
            "id": "95ca765a-7e42-469d-ac9c-08911f332b5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8209
        },
        {
            "id": "6a1b4c02-fabd-49dc-b9cc-a148672cd6fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8211
        },
        {
            "id": "6919bc37-2fc3-4430-b0e7-9a6250b13bb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8212
        },
        {
            "id": "1678d8be-5cdb-44f0-9621-33f93e0a9d13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8213
        },
        {
            "id": "eb89452e-931f-437b-8207-2e98eba7401e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 8218
        },
        {
            "id": "3a0bb1a3-14c2-401b-9036-e6337d4a309b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 8222
        },
        {
            "id": "5e2f4d5e-47f4-4498-8ffe-298864fbc2fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 8230
        },
        {
            "id": "dc6e6ef8-85f3-4e3a-8fba-cba614150c21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8249
        },
        {
            "id": "46461406-1e6b-446f-ad1e-e650b2bcc31d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8250
        },
        {
            "id": "ae1ecca2-252d-47e0-8292-53c9533f3953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 64256
        },
        {
            "id": "6cd5428c-9b8d-40a1-b611-0b226721c479",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 64257
        },
        {
            "id": "a3627ccc-cec2-47e8-a68c-91a7a9815e26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 64258
        },
        {
            "id": "a1067d6d-34e0-4ab9-b7d1-5ccba83a0f21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 64259
        },
        {
            "id": "4d0af9a8-0236-4971-b593-10c4e660c9b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 64260
        },
        {
            "id": "8b304efd-cd58-48ae-bb47-080a27488766",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 44
        },
        {
            "id": "66424871-0ab3-444f-b7a4-ebc096238a82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 45
        },
        {
            "id": "5ec16301-44aa-418f-ac94-bbbc67f39888",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 46
        },
        {
            "id": "a2e7bd49-b5c0-4144-86f3-b426d86d3049",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "e3775f54-55e7-43f1-8f7c-4f8f7a5d4437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "1a8764b6-ef2a-4ae7-bc29-f23546917970",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "3e378339-635f-460a-b944-22c45f40bf62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 67
        },
        {
            "id": "066a57e4-29c2-48cc-9a76-2235e52e9bb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 71
        },
        {
            "id": "8fb3d88a-6e3d-48e6-b112-838939d3a792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 79
        },
        {
            "id": "56e72c5e-1332-4254-ac85-93a2b316113e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 81
        },
        {
            "id": "c29f89df-7c1e-4814-85cd-561b48b01bec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "50903517-c32e-4dd2-9860-133509ee9816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 99
        },
        {
            "id": "7c7e7e35-e220-49cc-8f1e-bd0d35d21706",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 100
        },
        {
            "id": "3181fe8d-960e-4365-80f2-ca4422624970",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 101
        },
        {
            "id": "f96a9141-a066-476e-9ba4-107c7eafe386",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 103
        },
        {
            "id": "1a8a7a8c-2d62-40b4-b704-86d6b730378c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 109
        },
        {
            "id": "9ca1dcba-bd2e-43d9-b32b-2ea513ff712f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 110
        },
        {
            "id": "6ef05147-40b6-4826-9408-fe0ec2ed35dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 111
        },
        {
            "id": "8aad3149-b24e-4a90-89d0-12c17d91d8f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 112
        },
        {
            "id": "88163ee5-a88f-47ab-a847-375b2f8738bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 113
        },
        {
            "id": "1fefa844-e135-4c9a-b407-ae4d03b8bdc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "0ad351eb-9c9f-4c2c-a373-ae587b20247b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 115
        },
        {
            "id": "6363c95f-76b9-4e8f-b2af-c91dbe6b00be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "621930f4-c879-456a-b8b9-419663e9bb6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 118
        },
        {
            "id": "46a15804-e10e-4093-a0cb-0f5cd2c691af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 119
        },
        {
            "id": "6f255644-04ce-475e-82cf-8fc111539b88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 120
        },
        {
            "id": "eee5741e-7265-4069-97bc-f56669be4f97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "f2068d89-9a2e-4428-bea7-7011aebef108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 122
        },
        {
            "id": "c7d000eb-f182-46a6-8a40-664aef3ec0f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 171
        },
        {
            "id": "be56b6cc-6f7b-4576-a742-3d43d7c2b6d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 173
        },
        {
            "id": "a0c7a0b4-ab60-4b48-92aa-434e7c3538ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 187
        },
        {
            "id": "2e7457c7-5d39-490e-8e94-c42c5f5b0d3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 192
        },
        {
            "id": "c877a9d6-8d49-446c-8790-47a55aca12f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 193
        },
        {
            "id": "5c52e0f4-92c0-437c-9d7d-3b2c8956efa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 194
        },
        {
            "id": "8f376e2a-0f3f-4ead-8c2f-e30a7c7817a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 195
        },
        {
            "id": "a878ab2b-784b-4101-a269-1a0d7399fd08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 196
        },
        {
            "id": "5387221a-7b38-43f3-9642-4ce719e1541d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 197
        },
        {
            "id": "85412c75-8280-435b-a7e6-28d26600748c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 198
        },
        {
            "id": "fede8a6c-c95a-42be-a3ab-f8d6e27b1c8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 199
        },
        {
            "id": "95e28df5-4f6e-49c7-8575-ab8453c8565a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 210
        },
        {
            "id": "78421475-ff3d-4691-821f-7cf815b33738",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 211
        },
        {
            "id": "fd71dbc8-9adb-4357-bd03-b1dec48bf02d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 212
        },
        {
            "id": "d82374ad-f99d-481d-9c9a-41f87050a7fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 213
        },
        {
            "id": "4b5c6ab2-34eb-495c-a640-7dcb278046d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 214
        },
        {
            "id": "1447327f-a053-4238-b3b3-6b1eed1115b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 216
        },
        {
            "id": "664403a5-83d5-4707-a4f4-5913671841c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 224
        },
        {
            "id": "663336c3-fef9-48b0-96fe-fa92b9eccb97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 225
        },
        {
            "id": "e8597d44-62c4-408d-a89a-34e8b8255276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 226
        },
        {
            "id": "1d349732-54ec-42a8-a447-a83eb6dee097",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 227
        },
        {
            "id": "f9c4564d-dd69-42b4-b611-2805c2ee397d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 228
        },
        {
            "id": "ec266091-7ae3-4799-aa34-6520e95a14a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 229
        },
        {
            "id": "9d517d77-ee78-4c91-86cc-8652325ce5f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 230
        },
        {
            "id": "8250942c-31e0-483c-acab-7e2d3aabe6ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 231
        },
        {
            "id": "796374f0-79bc-4d46-bbd8-8696bb46d202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 232
        },
        {
            "id": "dd150b7d-6d2e-41d4-8e9c-50b5ab818cf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 233
        },
        {
            "id": "b44e0cef-e30a-4713-bbbf-c2fe23a95e5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 234
        },
        {
            "id": "fb458279-2bac-4fad-9881-59ae29e7015b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 235
        },
        {
            "id": "e65cbb7f-0763-45ff-a77c-551c2a19d097",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 236
        },
        {
            "id": "8a6f5ea3-389b-46be-9d05-889c2ca60bec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 238
        },
        {
            "id": "82d67ff1-0726-41c0-a194-8522249b7621",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 239
        },
        {
            "id": "5fcc9b7a-7d4b-44a6-9f80-8e9bea5ccfbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 240
        },
        {
            "id": "c8f0a566-7f5c-4b55-aa79-3381842834d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 241
        },
        {
            "id": "cd1db7d8-9cfb-4dbb-908d-e97fff752cec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 242
        },
        {
            "id": "f0544b34-8e14-46c8-ae39-710168ab7787",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 243
        },
        {
            "id": "5166c576-c838-458a-aaf7-b19a5864a269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 244
        },
        {
            "id": "cf03dc92-cc8d-43ca-8c57-31f0f41b0023",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 245
        },
        {
            "id": "9a893ca6-990e-4994-84a8-299605c6c489",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 246
        },
        {
            "id": "b56b0d24-ee89-43bf-ac09-6b8eec893dab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 248
        },
        {
            "id": "72993d8a-eafa-4c95-a856-cedd1bf4e2a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 249
        },
        {
            "id": "e3129b0f-6534-4579-b456-3f044e4f3aab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 250
        },
        {
            "id": "a5c01bd1-0b79-4087-8b43-b33e24422864",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 251
        },
        {
            "id": "a77d3261-0eb4-40f0-a0e5-b7ba5332f0ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 252
        },
        {
            "id": "fa9ebf66-ecd6-4f85-96ab-3011e8fe480f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 253
        },
        {
            "id": "6adc9767-b577-4a8d-9127-b5d7bd7123e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 255
        },
        {
            "id": "1359a94c-ad73-4e7b-a3a9-d3f54cf70689",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 256
        },
        {
            "id": "48a9932d-102e-45f2-bca0-b46f1380742b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 257
        },
        {
            "id": "452efdb6-f529-4cdf-86c5-19e0d270b9c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 258
        },
        {
            "id": "51520a98-514e-48b7-9668-48a38f982ba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 259
        },
        {
            "id": "d5bfce47-cfef-45d0-bdec-d88f1601f14f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 260
        },
        {
            "id": "51ebb3ae-878d-46ee-9273-432b2156eac3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 261
        },
        {
            "id": "eecb10c3-11e7-4b1a-9fb3-b380bcf69115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 262
        },
        {
            "id": "6d55f83f-7671-40c7-bf3b-908d079ba86b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 263
        },
        {
            "id": "360f8aef-1dd2-4303-b2aa-12bd8cf2ac2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 264
        },
        {
            "id": "b9cdcd49-3d95-49d4-b696-0d688f2dfe31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 265
        },
        {
            "id": "b1d92f79-0a7e-46c1-85ab-f68d5df84c05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 266
        },
        {
            "id": "658ba49f-ebae-43f0-a356-417c316beed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 267
        },
        {
            "id": "78862462-d883-4478-a89b-7f40e29eaeb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 268
        },
        {
            "id": "2d4a1ea5-bd48-4ca5-a780-52ad2de30587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 269
        },
        {
            "id": "7257a3f7-8c2b-45d6-aa50-375e2b78665e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 271
        },
        {
            "id": "6703c72d-b089-4c0d-b9be-268fcb0aa838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 273
        },
        {
            "id": "9f9e115e-b2ea-431e-9bdb-2af899568e46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 275
        },
        {
            "id": "05e79294-6a3a-4a52-9fa7-09cee2443d05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 277
        },
        {
            "id": "e4673f3e-feda-4b15-8ea8-f3df56e7de92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 279
        },
        {
            "id": "acbe4ea3-846d-4f0a-bc0e-4ce232e9b73d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 281
        },
        {
            "id": "5b2151e8-8bbe-43ec-8bae-7545e5867a1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 283
        },
        {
            "id": "27a5eaa1-0ed5-47e8-aab6-6e13c8f26c7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 284
        },
        {
            "id": "75ee108f-1660-486f-a04f-42794289795c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 285
        },
        {
            "id": "64443bf5-4126-44a1-9c7d-4116b36c5e6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 286
        },
        {
            "id": "7c08bfca-0d93-48cc-8fc4-9935357b273c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 287
        },
        {
            "id": "df8780ff-1670-44e3-a72a-09bb8878f7a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 288
        },
        {
            "id": "30dbe572-7811-4ace-9af1-9466cd0987a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 289
        },
        {
            "id": "160f7696-9d06-4749-bcb2-658a7708292f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 290
        },
        {
            "id": "bc2cf1da-7bf0-43cd-9656-3ed2f13e5d8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 291
        },
        {
            "id": "375e41ad-76a6-41ec-98d1-f8e0ca27f9a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 297
        },
        {
            "id": "4e4e019c-c271-4045-8b93-a2ccfe8f3261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 299
        },
        {
            "id": "9cc12a3e-3b42-4240-875c-399ce7c2fc6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 301
        },
        {
            "id": "b096d859-b336-4f55-a226-8f50abd24aa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 305
        },
        {
            "id": "dd85d1ea-9a88-4178-8864-80299608572f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 309
        },
        {
            "id": "dd7f0c3f-7a4c-45c5-a2f5-db89d2e305d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 312
        },
        {
            "id": "1939239a-9fda-41da-a910-b731b756f3c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 324
        },
        {
            "id": "2be9c954-a139-44cb-8dc5-022a8b4bfbb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 326
        },
        {
            "id": "1fb15826-9c3a-4b31-a5b9-8471d1269385",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 328
        },
        {
            "id": "82dcabd0-da9d-4927-bf14-3226e9436070",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 331
        },
        {
            "id": "e5bebea7-e5ff-4606-9d0f-117c39fa3302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 332
        },
        {
            "id": "fa117ec2-368c-4c3e-bcc4-d1096d879028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 333
        },
        {
            "id": "535e0790-83ba-4919-a4f7-99578ec7a834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 334
        },
        {
            "id": "80ea7ba5-99e2-4186-9d19-7b3271cf3773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 335
        },
        {
            "id": "f60de7fd-d45c-4fe4-aa3f-75a290228873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 336
        },
        {
            "id": "043a3a0e-a613-4bab-91ab-06ff889ec849",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 337
        },
        {
            "id": "0a3a4613-0ef4-4605-8648-cb373451613e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 338
        },
        {
            "id": "dde03899-5aad-4bb1-9108-c51e841780bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 339
        },
        {
            "id": "a9f362be-deba-4321-9e3c-c6c10293b819",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 341
        },
        {
            "id": "047f1d0e-4108-4874-b8e6-8c187fa08704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 343
        },
        {
            "id": "71528181-42e2-48be-85c4-4534bc04d1be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 347
        },
        {
            "id": "409706c7-f158-4924-88d2-ac4dc4a39381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 351
        },
        {
            "id": "824bff1f-64af-4acf-ace4-da51230ab570",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 361
        },
        {
            "id": "41328eed-9b61-4e25-9c23-dfbf36d8f0d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 363
        },
        {
            "id": "c75bcf17-c8a5-4007-bfc4-daa931c68833",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 365
        },
        {
            "id": "f0624e1a-5785-46b4-accd-09e21bf88ee8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 367
        },
        {
            "id": "745c3e75-8bd0-44fd-b2f4-ae60b37da039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 369
        },
        {
            "id": "fccc2380-1af3-4394-8a78-4b5d19dbe14c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 371
        },
        {
            "id": "3f90e1b9-292b-4c75-a5a8-537fe6885dec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 373
        },
        {
            "id": "3b1f361c-8a78-4ca1-a427-19aab4bf5102",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 375
        },
        {
            "id": "047a6bef-1342-4711-a358-66daa2a4468d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 378
        },
        {
            "id": "a4300f0a-17d1-4c41-af84-dc34f3d210dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 380
        },
        {
            "id": "79939996-4df9-4599-bed1-56d93dcf2b56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 417
        },
        {
            "id": "528c3a34-84ef-453d-adf5-6f1ae1450352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 432
        },
        {
            "id": "0a95b3a3-603c-4569-b7a7-6bfee1ff3246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 506
        },
        {
            "id": "815d67da-7cc7-4500-a912-0fdc87c8c5b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 507
        },
        {
            "id": "ded7c8b5-2b2e-48bf-895a-36c3c56ab776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 508
        },
        {
            "id": "c19e9ec7-ebb8-4c47-8ba6-12886296fff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 509
        },
        {
            "id": "3611f35b-7e46-4ffb-a401-cac58d17ba58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 510
        },
        {
            "id": "f45ab676-03c0-4e35-936c-07202b1c885a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 511
        },
        {
            "id": "08303f4a-da47-4b6e-a174-fc00b4826d45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 537
        },
        {
            "id": "a97877eb-ffb4-4ebc-b8b2-08f2251ee45e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7809
        },
        {
            "id": "bde61358-c510-4663-b74e-b0f684e3b0a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7811
        },
        {
            "id": "8d26385a-0a7e-43e7-9a50-e48975d31d2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7813
        },
        {
            "id": "a2f2c4d4-6145-42c8-bdea-6b4524a4f6b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7840
        },
        {
            "id": "bc13cf8b-0fbc-4d5c-be74-b4c4fce16413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7841
        },
        {
            "id": "2c0ff500-5de9-44d9-b9d9-71ceb899a184",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7842
        },
        {
            "id": "1c16c1ee-f719-4c0b-8102-fda4d9a27186",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7843
        },
        {
            "id": "ba21318c-eca0-4038-b18c-0ee47e69060a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7844
        },
        {
            "id": "44d12fc0-83e7-4ce2-b492-bdbe3af6b3e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7845
        },
        {
            "id": "e4d4cc59-cfaa-44d5-b41c-eaac7346d00b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7846
        },
        {
            "id": "ec38a942-ae50-4efa-befb-7d21b1f354a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7847
        },
        {
            "id": "70fbafa8-6ef0-48da-aeac-47dd494090d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7848
        },
        {
            "id": "d0ce7795-6d72-4db5-9116-197317e5f504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7849
        },
        {
            "id": "5165b5df-58dc-42d7-97d0-fb9d5f18ecdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7850
        },
        {
            "id": "a4f0c0cd-b2c3-44fd-9f17-99189ee0b3b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7851
        },
        {
            "id": "a98132c2-5a33-420d-b1dc-c9fa153a5e62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7852
        },
        {
            "id": "d1a32a30-bb37-4a50-8d16-8462f2f59f75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7853
        },
        {
            "id": "c1cc4eae-f53d-406e-9cf4-ba2d738aa7a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7854
        },
        {
            "id": "07bb741b-5034-4faa-ba07-d980c0c97659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7855
        },
        {
            "id": "6427165c-6ca8-4174-ac92-398f47e241c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7856
        },
        {
            "id": "192f986e-dd6f-40f9-9393-e27f3c4a6a9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7857
        },
        {
            "id": "846e2c48-c3a9-4f60-8241-20ae91475d62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7858
        },
        {
            "id": "8af87e35-b219-4c29-b88e-22d7bca86f1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7859
        },
        {
            "id": "ee630b55-c34c-4a15-9ccb-8e4b7330e54b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7860
        },
        {
            "id": "af293a23-8f74-46a2-8d73-cfad87b4fdb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7861
        },
        {
            "id": "56d0f1f2-8a21-4647-bb55-ef28316685f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7862
        },
        {
            "id": "d1abc49f-b127-42de-b786-77074ef613f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7863
        },
        {
            "id": "4cd97f6a-874e-4270-b86c-b3b7eb42087f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7865
        },
        {
            "id": "a0d57702-66f3-427c-ae6b-80929b71ce63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7867
        },
        {
            "id": "0121bbbf-dcfc-4ef6-b19f-553fa9e3352f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7869
        },
        {
            "id": "69030da3-2a11-47dc-9042-0aeb70f74b8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7871
        },
        {
            "id": "3a7797a8-24b6-4962-80f8-e1a86585a746",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7873
        },
        {
            "id": "9bd79de4-f761-478d-a5fe-bd8c15ad3322",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7875
        },
        {
            "id": "2c479b25-3122-44fa-89ce-1fac01c5eea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7877
        },
        {
            "id": "9032847c-acb6-42d2-978f-9053c0da697e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7879
        },
        {
            "id": "5439914b-1224-4591-971d-d2f5e7b58908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 7881
        },
        {
            "id": "5e5731d5-f2f4-4a10-84fa-0ef245416e7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7885
        },
        {
            "id": "1f65b6ab-3d33-4549-905c-16baac83d150",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7887
        },
        {
            "id": "f9973f8c-04f6-4c8a-a1ae-0761357b72aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7889
        },
        {
            "id": "dd41fb13-8865-4c3c-b364-938ba916121b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7891
        },
        {
            "id": "b0053e88-7191-4c6e-87d7-a799fe5604b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7893
        },
        {
            "id": "eee969f0-94b9-41d2-a34e-5bc85f6f4828",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7895
        },
        {
            "id": "3f82abaa-5c27-4ec2-97d2-993fa9cb15f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7897
        },
        {
            "id": "0d953d78-7829-4baf-b9e9-8c5ac2ad1386",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7899
        },
        {
            "id": "4731c19a-e55d-48ba-9780-4a9268a6da49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7901
        },
        {
            "id": "27876cbc-9e27-4612-a256-422ea94d3ac1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7903
        },
        {
            "id": "8ddaa420-e7db-4461-ba42-eaefef86c727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7905
        },
        {
            "id": "56991f52-ba3e-4a5d-9e04-4e40c291f2ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7907
        },
        {
            "id": "d5cf8f99-f921-4b2d-9c76-ff1cc862274f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7909
        },
        {
            "id": "957f411d-3cde-403b-a5b5-cccc6bf1fbe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7911
        },
        {
            "id": "ac41918b-b847-4195-949b-e3bdc0e8a963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7913
        },
        {
            "id": "837e9e42-bef2-43b2-9e54-109fdfa0dd3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7915
        },
        {
            "id": "e7260879-62c0-4a25-af28-a8e5084e58fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7917
        },
        {
            "id": "0c48dc5e-b7be-4dd8-b1c5-bb23683128f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7919
        },
        {
            "id": "a648bd66-aa93-400c-a5b7-5096e903c888",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7923
        },
        {
            "id": "21c0de6b-70b1-4c97-9893-02b5aa77b7e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7925
        },
        {
            "id": "c8a45440-eb7d-41be-a63c-cb2cf49487b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7927
        },
        {
            "id": "ace6a559-018a-407f-929e-28f10c258d98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7929
        },
        {
            "id": "e6646596-2785-4a5e-b7f7-a0bcdc8c52af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8208
        },
        {
            "id": "5aa1dccd-eec8-41b7-b0f6-6731d4bd4927",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8209
        },
        {
            "id": "f8967057-58d8-4e93-85b0-bd82703667fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8211
        },
        {
            "id": "46dc2c99-3c2c-45c5-b55c-f29760052a7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8212
        },
        {
            "id": "150048df-2c2e-4a34-946a-c05ad4f7f082",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8213
        },
        {
            "id": "523982fc-1ad7-4669-acc2-0b1f5dc06962",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 8218
        },
        {
            "id": "551b8e90-fb67-49ac-b96d-a6cbc2399cec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 8222
        },
        {
            "id": "ea7279e1-27e9-4aa7-b920-875f5c5f021a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 8230
        },
        {
            "id": "af5dd5be-11a7-4dab-a693-f0f6b1e70b8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8249
        },
        {
            "id": "f0ae9285-ddca-430e-adaf-1f7ae483053a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8250
        },
        {
            "id": "88021ebb-1074-424e-9032-7f956c2bc8c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 45
        },
        {
            "id": "e7789a04-fabe-42e6-9d5a-ad093530ea06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 67
        },
        {
            "id": "c539444c-d1f9-47ef-9771-d7c2e9a679c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 71
        },
        {
            "id": "03209c6d-aabd-4eb5-91fe-3a59e333e15f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 79
        },
        {
            "id": "53283805-0699-4469-bbca-684687e5bd0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 81
        },
        {
            "id": "027cbfaf-3bb5-4534-8050-988bbaa80393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 99
        },
        {
            "id": "ef76bdb0-5c83-4c92-9fe3-62315396de0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 100
        },
        {
            "id": "696d4b1e-353f-4462-9da9-8c5a907d4a26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 101
        },
        {
            "id": "674daf5d-f79f-4a7f-a2c9-642b0f4582e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 111
        },
        {
            "id": "2c501a16-bbee-4978-818c-799b9e55ba80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 113
        },
        {
            "id": "523bb201-3300-4ea5-a2d0-4b614003033f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 117
        },
        {
            "id": "58e2e191-d457-40ab-948e-f33d3d25ea65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 118
        },
        {
            "id": "7c772284-7876-4030-be05-f81ab0528339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 119
        },
        {
            "id": "9a606b87-83ba-40f5-adf1-89128674cd72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 121
        },
        {
            "id": "99a2f5be-2665-47ff-b41a-ddda79c03e6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 171
        },
        {
            "id": "e3afa18b-ab24-413f-8bb2-0a0f8e83fad0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 173
        },
        {
            "id": "b9b475fe-3410-4261-bba9-ec3015133b08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 199
        },
        {
            "id": "30ada2af-3386-4873-8468-695dafd9e5e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 210
        },
        {
            "id": "9cc656c7-53c9-4d2e-9130-3f61cc2f28e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 211
        },
        {
            "id": "75b5a80b-ed62-4ddd-ab3e-2c93c751c159",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 212
        },
        {
            "id": "617bc9e5-92c4-40a2-b738-59a4b704cc1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 213
        },
        {
            "id": "cb3ca754-0938-41d7-a80a-254f2fec4b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 214
        },
        {
            "id": "361a8262-4cc4-42f7-8ed5-1368b2de1bb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 216
        },
        {
            "id": "90b3d772-1d5f-4d12-b0f9-fdaf3b5a664c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 231
        },
        {
            "id": "82760be5-1520-4bfc-995a-2c5fec6681b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 232
        },
        {
            "id": "dbb634cc-49e0-4d3d-af60-083dafa32f0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 233
        },
        {
            "id": "ad2eacb3-f995-4e5f-8a60-8a3ca33ca46d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 234
        },
        {
            "id": "a066c62a-dca8-422a-9351-0c213bc70796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 235
        },
        {
            "id": "d9081b10-c5d5-4a9a-b384-b8ba65cc1ca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 240
        },
        {
            "id": "d003fe27-2b2d-41f3-a614-35d47730dd1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 242
        },
        {
            "id": "ccefe834-483e-4cf5-ba37-7f54126c39bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 243
        },
        {
            "id": "f6f1be21-8b3d-429b-8737-dec644c57e5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 244
        },
        {
            "id": "4546f4fd-bccf-4fec-a423-b52e00e1aaa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 245
        },
        {
            "id": "22b92377-31ba-436d-8553-cbe88475f2a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 246
        },
        {
            "id": "b254e338-374c-40d9-9137-687b76dd95a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 248
        },
        {
            "id": "8e59122a-5939-415a-9d1f-a837581897ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 249
        },
        {
            "id": "3e4c59e3-71f5-4cbd-8b37-0df671ce99b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 250
        },
        {
            "id": "71760170-26f7-493e-aabd-bf62e87c979e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 251
        },
        {
            "id": "92ea267e-ca0e-4d4b-88b3-585ee07cf740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 252
        },
        {
            "id": "e77ff3b4-7e7b-43d5-8dc8-30a22407b5d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 253
        },
        {
            "id": "6b030f9d-8932-4f19-9a22-bf19243434b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 255
        },
        {
            "id": "e91ec25d-9097-46fb-98cc-459f49572fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 262
        },
        {
            "id": "2dfb95d0-d980-4d65-a301-c227de76ebaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 263
        },
        {
            "id": "bb38a79b-77e7-4134-a319-380c0c490dc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 264
        },
        {
            "id": "d87ea551-6e2c-4342-9468-37fb82ca0787",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 265
        },
        {
            "id": "95f2646a-def6-4b8b-b714-8d770a7b037a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 266
        },
        {
            "id": "54f75738-e505-4943-808d-737ba74b4d60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 267
        },
        {
            "id": "3faacfa9-5c02-44ac-9bdc-72db94074dba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 268
        },
        {
            "id": "9e15d13e-9363-4052-bcc7-8527745562d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 269
        },
        {
            "id": "02505cda-915c-4682-b615-44c647a88756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 271
        },
        {
            "id": "438135d6-ee85-4ae6-9455-074297c34732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 273
        },
        {
            "id": "399aadc0-36e5-4577-9c6a-e9dacd144f8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 275
        },
        {
            "id": "1c0a0ba4-8c40-4a31-86c4-1dc4c30e1553",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 277
        },
        {
            "id": "9d33a9fb-55db-4978-8b76-e0d38777339d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 279
        },
        {
            "id": "e8e92717-ea6b-4116-8851-22ee991a6aa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 281
        },
        {
            "id": "213a28a5-0d3d-4290-b9db-4dd47fe0f495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 283
        },
        {
            "id": "fba168de-7677-4843-a730-acae6d1e1516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 284
        },
        {
            "id": "3d3d5583-6a0d-42f7-97da-e4da92bb9d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 286
        },
        {
            "id": "cf516f9f-d192-4ae6-b766-9c02d7530f44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 288
        },
        {
            "id": "9845cdc4-970d-4e60-9eb1-b196b5aaea9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 290
        },
        {
            "id": "9484d08c-2cd4-4716-bbf6-e27dae75e2e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 332
        },
        {
            "id": "5b7b6c67-f2a4-4cca-b34d-28d2178d61e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 333
        },
        {
            "id": "61194059-332a-4d3d-9a93-a1766c7d4af4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 334
        },
        {
            "id": "8ebfa1bd-8ea1-42c0-a22f-13bcbc8d9236",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 335
        },
        {
            "id": "ef5eedd6-957f-4b34-8ba0-b87d4f0c0f13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 336
        },
        {
            "id": "6f1e1faa-4e7c-4fbf-a01c-689454fc841a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 337
        },
        {
            "id": "3b5da5f1-a732-4481-8c4e-72eefc8fde68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 338
        },
        {
            "id": "03560930-8919-4373-b88b-f4146db99938",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 339
        },
        {
            "id": "f203d9cd-0bf6-40c0-84e3-ac18b203610b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 361
        },
        {
            "id": "0a238c94-96a5-4e25-8645-94ccbf48d0e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 363
        },
        {
            "id": "013c73e9-3ab1-4186-a03c-953200a4fcb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 365
        },
        {
            "id": "090067f4-4d31-427e-bedf-351ffe921681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 367
        },
        {
            "id": "b70b0199-621d-4d97-a337-1d45d96c88b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 369
        },
        {
            "id": "0e6ffca7-da17-4879-a7ea-325d4fac171d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 371
        },
        {
            "id": "19c8ffba-15f6-4fae-bdd1-7e9757f55690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 373
        },
        {
            "id": "01f81ed2-7b81-4d99-a8ae-916294e4e2ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 375
        },
        {
            "id": "63affe8d-6452-4905-b2cb-d8749ad38549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 417
        },
        {
            "id": "22a3b937-e59d-4d9f-a4e9-7ec612f3353b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 432
        },
        {
            "id": "4fcfca58-0f80-4d6c-87ee-6c653ae4e218",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 510
        },
        {
            "id": "2fb31758-374e-4e96-8e3a-11dc09103978",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 511
        },
        {
            "id": "1194d88c-0339-4138-b731-c3c8f633e52d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7809
        },
        {
            "id": "01fe30a8-e353-471b-899e-b25b3f775c4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7811
        },
        {
            "id": "ad2c089a-2753-42b4-bbe1-f5e108f68c04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7813
        },
        {
            "id": "3437893d-bf6f-4910-96c1-44c2d310a03a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7865
        },
        {
            "id": "3ddeab4a-17dc-46dd-96a3-11cdb500ddf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7867
        },
        {
            "id": "8f3bd177-38b8-4f20-8268-3eedd57023b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7869
        },
        {
            "id": "4bdf3731-bb56-40fb-b2bb-6b4a93476b83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7871
        },
        {
            "id": "d8647b8f-dc03-4df0-b931-71ebcee86495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7873
        },
        {
            "id": "4a0e3d70-81b1-41c6-ac16-0377905d434c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7875
        },
        {
            "id": "5a7e4dde-a6ae-47e5-96f8-59fadcf655bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7877
        },
        {
            "id": "a1297a26-a63c-4054-83c3-42d1439f4732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7879
        },
        {
            "id": "1f98be91-f0fe-4348-b5ad-133af0b0c544",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7885
        },
        {
            "id": "ad8570bf-719e-43e6-a825-0273b7e164c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7887
        },
        {
            "id": "9dabfab9-babf-4811-8d9b-2cfd8e7b7c90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7889
        },
        {
            "id": "2e01fd9f-daa2-474a-856c-d3ce2e7c1152",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7891
        },
        {
            "id": "b305661a-4b8d-41d8-843d-d14f83a7e094",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7893
        },
        {
            "id": "3a98d89c-80c1-4519-ada2-24a27df6c9b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7895
        },
        {
            "id": "d3aa357f-e119-459b-a25b-b00d297328e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7897
        },
        {
            "id": "56655dec-ef8c-4c7e-81bf-ab0c9fad7d55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7899
        },
        {
            "id": "6a24fc40-ad36-4af7-a009-d137651227cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7901
        },
        {
            "id": "8d7fa9c9-57d9-4c70-a8fb-5cd08a673770",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7903
        },
        {
            "id": "d5f940e7-3326-4d25-bc4b-cb725d677251",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7905
        },
        {
            "id": "8eebd388-4f2b-4716-b1c4-53c013129b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7907
        },
        {
            "id": "97f7721f-c760-44c9-b6c1-3e19c98b6e7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7909
        },
        {
            "id": "923441be-dfc5-4f28-bb8d-db920bafab2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7911
        },
        {
            "id": "f3f8537e-b74a-4dec-bb88-37b9b2e490f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7913
        },
        {
            "id": "fad077fd-6642-4706-ac29-d6f0aa4ae0db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7915
        },
        {
            "id": "1a0374c8-d344-46c1-a119-00b2038f6c1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7917
        },
        {
            "id": "a5329657-82df-4435-b81e-be644a0fc3cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7919
        },
        {
            "id": "c36dbc36-ca57-4a8f-8115-a7b3e7e74a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7923
        },
        {
            "id": "14dd3fcb-75d0-4afb-a1f9-7d58fe31e587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7925
        },
        {
            "id": "1294636e-18e4-4fec-942c-482c75ecdf3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7927
        },
        {
            "id": "4c2312f8-f04d-4346-938d-5d1af2048824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7929
        },
        {
            "id": "59f617f3-4fc5-466e-811a-e8cc92fd2b99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 8208
        },
        {
            "id": "283c738f-ba2f-4543-92a2-10fec18b06f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 8209
        },
        {
            "id": "f1c01359-78a1-4ae6-a53e-e6360bec3624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8211
        },
        {
            "id": "bdd96716-2f37-4a8c-aed2-78233955246e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8212
        },
        {
            "id": "5f36343a-c447-436e-9ae3-aff844201bca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8213
        },
        {
            "id": "8cf23dd1-4642-4309-a7f2-cb8aacf8c059",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8249
        },
        {
            "id": "bb9ba12b-3ae7-4fa2-a4d6-3c0b977a8f98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "88ce9b30-cec5-47c9-9114-d75bbfaa0ffd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 45
        },
        {
            "id": "f7d036a1-f479-4d94-aa54-aa5a5d3bf09d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "c628a172-b228-4e4f-89aa-eb4a1cb8b177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 58
        },
        {
            "id": "3f16fe35-8339-45f2-b833-ed52f33e6a46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 59
        },
        {
            "id": "16c8e7e3-21fd-4a4c-8143-92768b4af7ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 65
        },
        {
            "id": "2c82269c-de52-47f1-9b24-c619d78d173b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 67
        },
        {
            "id": "bab22f93-1e13-44af-841c-c571aa695e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 71
        },
        {
            "id": "0bab6b94-f086-48d5-9252-e70f7ea04043",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 79
        },
        {
            "id": "1aebb472-9541-46b1-a5e9-6f6afcb0b2a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 81
        },
        {
            "id": "da8d635f-f51b-4482-9f97-96727aff5ac3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "57950c72-da55-4006-8b03-4cec1c9a6f67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 99
        },
        {
            "id": "98cd3881-d68e-498d-9ac6-a800a1dc6687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 100
        },
        {
            "id": "5301dc06-fb43-48c7-9028-a45d6d29acba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 101
        },
        {
            "id": "bc1700ff-cd9f-4cb5-91b4-8c2a616706c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 102
        },
        {
            "id": "0ee1adeb-e4e7-4d3d-82fb-39e272091147",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 103
        },
        {
            "id": "02ce8160-8def-4053-af3d-f88407c0794e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 109
        },
        {
            "id": "089c89b0-e9df-4244-bf1b-9703730ff993",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 110
        },
        {
            "id": "4e44ef45-dc4b-48cb-b529-2847e34f1645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 111
        },
        {
            "id": "96c48bec-a92b-46bf-a735-1fab494a3348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "1a6f2d81-915b-40bd-bfa3-e2cbc3309e80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 113
        },
        {
            "id": "ec21ecc5-0d69-4b82-b5b1-235986231e7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 114
        },
        {
            "id": "43ee7fd5-3ae1-4c23-b38f-2c8b7b203070",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 115
        },
        {
            "id": "67735c79-9cb8-4437-b070-dbbb7663174f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 116
        },
        {
            "id": "c0dcca58-83a2-43ea-a8ec-50c8dbe1c57c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "af739fae-b541-424e-bf07-87adcb459dc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "e23ce82a-10fa-4f13-b08a-8a7ca3676238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 119
        },
        {
            "id": "e1d324e7-444c-43cb-b827-3f537d97085e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 120
        },
        {
            "id": "57920bbc-cacb-4c9d-b4bd-6ba9601a2f06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 121
        },
        {
            "id": "c91ecb24-6f04-4760-a94b-05f8890ea917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 122
        },
        {
            "id": "0227cc80-b258-487e-abee-8a6f77f27ebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 171
        },
        {
            "id": "159ec249-0104-4307-b7df-2dbef2d3618c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 173
        },
        {
            "id": "4c6b76ff-0917-46d3-812f-5f0bb0ca0425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 187
        },
        {
            "id": "cabfcb25-6204-425a-a55a-83ed68dbed77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 192
        },
        {
            "id": "8612c003-8910-4127-9f27-8d92921949be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 193
        },
        {
            "id": "ed346174-fc8b-4703-b104-9691b092fb40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 194
        },
        {
            "id": "7769f386-c097-4197-953e-7458f273254d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 195
        },
        {
            "id": "d2ebcd21-817e-4237-afd0-51dd3125f2ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 196
        },
        {
            "id": "ee97691c-1a75-4306-9c55-487147f4b0fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 197
        },
        {
            "id": "45136b7c-5b9a-4e05-bf02-b93953b928d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 198
        },
        {
            "id": "df9c4659-ed0f-40bc-8196-31b416536554",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 199
        },
        {
            "id": "04727903-27ee-4af3-82fc-34325f8a5ef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 210
        },
        {
            "id": "32d332cf-7223-4bca-84dc-ee2bd6787e84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 211
        },
        {
            "id": "2606f386-b80c-4a1c-ae6d-cbc3ad990066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 212
        },
        {
            "id": "28911e05-2eed-407a-951d-0de4519179ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 213
        },
        {
            "id": "c65767d4-18bf-4dcb-9b3a-1ed22ec938d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 214
        },
        {
            "id": "6d98fdb1-0f0e-4386-ad4d-4d248c8fd11b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 216
        },
        {
            "id": "51e4e4f1-21f8-4dc3-b5e0-f626746f5202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 223
        },
        {
            "id": "b897f596-15a6-4ece-8f6c-75e5f047a173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 224
        },
        {
            "id": "96f66adf-199d-4a8e-baf2-9f1392683126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 225
        },
        {
            "id": "4c03ce2d-cd35-41f6-8055-78692833f0b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 226
        },
        {
            "id": "6fa3519b-4748-49cf-a16a-3c0ea1a6ea0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 227
        },
        {
            "id": "cd8d69f6-b862-426c-ae2d-e2b552e97a53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 228
        },
        {
            "id": "3040dd49-8847-4ca2-8d02-0e713d2e82e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 229
        },
        {
            "id": "1694688d-dcb6-48b0-a6fe-829dfbffa77a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 230
        },
        {
            "id": "5ae3cb92-d9c2-4edc-baf2-c03c3c76f5a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 231
        },
        {
            "id": "f62ca5bc-aefe-414b-962d-f5eb1c65c369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 232
        },
        {
            "id": "c69da377-dbea-45d0-be3d-25ff05090b4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 233
        },
        {
            "id": "414f0d14-15a7-4dc7-8c22-b12eaea0a792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 234
        },
        {
            "id": "e0442f67-a90e-44fd-a8a0-b38874907464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 235
        },
        {
            "id": "2911e33e-1971-4411-a334-e34c7221129e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 236
        },
        {
            "id": "4a8cbe5c-7455-4781-b0bd-a538fa3ce09a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 238
        },
        {
            "id": "bd3d53d4-3915-428f-a098-81dbc23d1277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 239
        },
        {
            "id": "ab4d9a60-6ec1-41a2-989a-631ef9b51b7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 240
        },
        {
            "id": "92738825-74ae-499a-90cf-8dd92756060c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 241
        },
        {
            "id": "c2fde7a9-a4e5-4a30-a343-05b359837997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 242
        },
        {
            "id": "eeff3f1f-92a5-40c8-99b1-32fabe4688c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 243
        },
        {
            "id": "feca0802-38d6-4bb3-8aba-102da66a183d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 244
        },
        {
            "id": "d04da1d7-f176-49b4-86c7-289b3fa537a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 245
        },
        {
            "id": "696dd6b3-cb26-48f6-93bf-2dc1fe775bc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 246
        },
        {
            "id": "cc0ae793-30d2-47f9-a074-1da86732ed77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 248
        },
        {
            "id": "29655310-cd09-480f-9b56-00884b6a6d40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 249
        },
        {
            "id": "6cbe1b8d-42cf-4494-9159-7a08eacccfad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 250
        },
        {
            "id": "1643ea03-165b-4295-b1ae-72e8aca79a6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 251
        },
        {
            "id": "ba19194d-84b4-4b9e-aeb5-312396edf195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 252
        },
        {
            "id": "d6764db2-c556-4212-89e9-a78ae58ae5e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 253
        },
        {
            "id": "84426a7f-14b0-4127-aed3-ea543e882f7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 256
        },
        {
            "id": "3caaa553-3c35-4e31-8efb-32d8ce3eff7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 257
        },
        {
            "id": "2f33c478-c207-49ec-be10-1a36169c07f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 258
        },
        {
            "id": "ebb7f6ad-2703-48ed-9e05-05825c36feae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 259
        },
        {
            "id": "27ee4861-847d-4c0f-ab7a-fe8533c32aff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 260
        },
        {
            "id": "682caa65-3f80-4280-880f-f848232f0a3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 261
        },
        {
            "id": "1444e366-20d0-45a6-974b-806fa943d40b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 262
        },
        {
            "id": "ad07b08f-9b17-4362-a812-6c04533872ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 263
        },
        {
            "id": "d64151b0-2f99-49a2-a2c9-a04078a3e215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 264
        },
        {
            "id": "b273f707-5205-454f-aaf6-21b27142c7b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 265
        },
        {
            "id": "52aecddc-d353-4701-946c-4666e8162222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 266
        },
        {
            "id": "cff53eef-a82e-4000-b3ff-cc21e16e0dc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 267
        },
        {
            "id": "5d70f837-5bbc-4ac4-9ba5-3e00dc1a22d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 268
        },
        {
            "id": "9b68d2af-8f97-4f47-9fc8-fbe7747c88f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 269
        },
        {
            "id": "831ff866-946f-457e-8932-d872a4e942b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 271
        },
        {
            "id": "e690d87f-37ff-4d45-bbc0-38ba54180eca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 273
        },
        {
            "id": "e0760445-71c4-4c89-a9a7-41c82e1b638f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 275
        },
        {
            "id": "e4a80f33-ffa9-4ea1-a689-27e131f8abc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 277
        },
        {
            "id": "2f04d520-e5a5-433e-b176-9b798299f3a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 279
        },
        {
            "id": "c80ae729-505f-4d36-9039-1d6936e64601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 281
        },
        {
            "id": "256d5d8e-5feb-44ef-bab2-c6c88e44271d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 283
        },
        {
            "id": "e4822bfa-4a36-42b4-9cdb-8fdeec5ffb57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 284
        },
        {
            "id": "02d6cfd5-d285-4e1d-ae93-a68490e24b96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 285
        },
        {
            "id": "724e8ee5-d998-4d6b-87dd-612bbb19540b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 286
        },
        {
            "id": "2d48f165-ce94-47b8-8113-2e2801ddc473",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 287
        },
        {
            "id": "3c291aa9-2e3f-4439-8af1-2fc6523551d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 288
        },
        {
            "id": "756a839a-9392-4006-8e77-9eac73115a90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 289
        },
        {
            "id": "dda01c5e-e75c-4ee1-b3be-3118bf31e35b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 290
        },
        {
            "id": "af3a7647-d0e6-4cef-a72a-662a5236b8ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 291
        },
        {
            "id": "110e210b-fda4-4646-8ec4-0e44096ec597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 297
        },
        {
            "id": "caecdca7-1d00-4201-a67e-003852a6f3b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 299
        },
        {
            "id": "f8e5bb00-ea68-42d1-bc9d-dc71af0cd6fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 301
        },
        {
            "id": "ed1663a7-5d9b-4111-87de-b6324123c68d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 305
        },
        {
            "id": "0b67eaba-5f67-4562-88d5-531950c490f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 309
        },
        {
            "id": "42ee41be-edcb-4d30-a5c4-b8d9fb454a40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 312
        },
        {
            "id": "2c1c6a2f-7a7d-4abd-8095-7682e899b1a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 324
        },
        {
            "id": "cbcf0184-47f0-4f0d-b1b1-030029648c1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 326
        },
        {
            "id": "5433df37-c40b-487f-980c-f4b94ee38ca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 328
        },
        {
            "id": "c4bc271e-b5ae-440b-a794-bf43a0d88280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 331
        },
        {
            "id": "b8c602eb-62b0-49b3-ab3f-7049ea26eaae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 332
        },
        {
            "id": "059f4b35-27a4-4625-8793-a47e0827bd9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 333
        },
        {
            "id": "0181fbfc-1918-4685-b095-5341a2822c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 334
        },
        {
            "id": "e97bb9a2-a4c3-47df-afe0-42c99af1f34a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 335
        },
        {
            "id": "4e718987-0d34-463d-a196-0f907ed30750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 336
        },
        {
            "id": "ceef7065-a39d-4296-859a-c672af34cbd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 337
        },
        {
            "id": "a3c4bfcf-a507-44aa-b221-f0a95e9e198d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 338
        },
        {
            "id": "ad7261a2-b003-47f4-844d-c8e79837794f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 339
        },
        {
            "id": "4e402716-354e-486e-9ade-cc08ceb98b2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 341
        },
        {
            "id": "ea769242-e218-4904-bea0-0c681c53e01e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 343
        },
        {
            "id": "447178f8-db8b-415b-8fe7-898eee418289",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 347
        },
        {
            "id": "4160db57-6060-4f16-b141-c5fca42ec86c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 351
        },
        {
            "id": "3d8a31ca-7f68-4110-b71c-3a2f4b62005d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 355
        },
        {
            "id": "138ddda2-7872-4b2d-b42b-7fbf3dd540ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 357
        },
        {
            "id": "0c1361fd-3a55-46da-818c-ada9bbc2f4b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 359
        },
        {
            "id": "5444428a-dd8a-446a-9b7c-0c487699b2d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 361
        },
        {
            "id": "ffe04243-db2c-479f-98be-49d21fa8556d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 363
        },
        {
            "id": "9ed3451c-2358-43df-94d8-c3d84ce087e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 365
        },
        {
            "id": "a8c9720c-d97c-4457-9611-f314da083db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 367
        },
        {
            "id": "ec5e9774-7875-4d51-a43b-db8a8a368b09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 369
        },
        {
            "id": "9f4870c1-5e33-4dab-84ef-f4843427d781",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 371
        },
        {
            "id": "b619925e-d1e3-4c39-aff9-d28aa3af78d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 373
        },
        {
            "id": "ec6b8c53-89c8-41cb-a090-bcdc4f3adbd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 378
        },
        {
            "id": "f448a7da-7827-4ad9-b5f0-71888c467fe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 380
        },
        {
            "id": "eac2542c-a8ec-4cda-9e7a-b6c25058b223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 383
        },
        {
            "id": "4bec0bf2-ba2e-4158-a364-1071b5f83384",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 417
        },
        {
            "id": "9f79d0f0-2a1f-4ec1-a374-56579ee40c78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 432
        },
        {
            "id": "d0a67898-a328-42a3-8bf3-9913058e4b3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 506
        },
        {
            "id": "f2d68ab5-b5db-4e12-879f-26f045425ee3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 507
        },
        {
            "id": "1435601e-1009-4235-b3ab-356cdbfa1ef5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 508
        },
        {
            "id": "dff2bfe6-87cd-4712-86a6-66097e1a7eae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 509
        },
        {
            "id": "4db4a85e-1810-42e9-b010-3e5a02196733",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 510
        },
        {
            "id": "0003fb15-e9e5-4bed-b134-8791062cb983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 511
        },
        {
            "id": "e8e81a96-217c-4093-9321-cfb58a52dbc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 537
        },
        {
            "id": "927eafdb-9307-4bd7-88c1-480d81db4313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 539
        },
        {
            "id": "97f424fd-df10-4d82-8b11-b01f8beee8d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7809
        },
        {
            "id": "a08466c0-fc54-4752-b084-19c587a032aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7811
        },
        {
            "id": "65240572-602a-4017-928a-70ec92c08d32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7813
        },
        {
            "id": "38988d6c-dbd0-47bf-bd9c-6b7850fab97b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7840
        },
        {
            "id": "405a5eca-19eb-4808-9d96-40596be8b161",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7841
        },
        {
            "id": "62d6cbe8-c607-480b-8a29-52f01c72f14a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7842
        },
        {
            "id": "6e0fedab-9f05-44d5-b72f-5ed191c6af33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7843
        },
        {
            "id": "e98da86e-91e3-46c2-a016-b793f606bcce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7844
        },
        {
            "id": "5ffc183d-3d03-4d96-9857-90bd49c70d6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7845
        },
        {
            "id": "a4492f5b-ca70-43b4-84ac-24d286fc6dfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7846
        },
        {
            "id": "25fe05e3-e72f-4c1a-a6ae-af8517ac97d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7847
        },
        {
            "id": "e413fea0-0a32-4865-973f-22c11a16cd71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7848
        },
        {
            "id": "0b0a0254-f3ea-49c7-8913-ac2004ae0595",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7849
        },
        {
            "id": "b1e73d35-19cd-47d8-9684-89611a115bc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7850
        },
        {
            "id": "b7b9123d-1c68-4080-b16c-7eb7d68290fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7851
        },
        {
            "id": "0f9b5284-c830-419f-bf31-856a62977016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7852
        },
        {
            "id": "5761a575-253f-491d-bd7a-beca4deac140",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7853
        },
        {
            "id": "535037ba-5904-4537-a251-ef87591f35a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7854
        },
        {
            "id": "d272b117-c73e-43dc-8c1e-25d925784638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7855
        },
        {
            "id": "ff7be476-5232-4432-b327-632eb20bf6b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7856
        },
        {
            "id": "e08bb74d-9f5c-45c3-b8de-81b757a04c6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7857
        },
        {
            "id": "867d3bf7-8347-4137-a4c7-1960e754ddf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7858
        },
        {
            "id": "7fbd800a-eb8d-4d8e-99c3-b29919737a47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7859
        },
        {
            "id": "525c6c67-b65e-4256-8869-f22a92fe6bce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7860
        },
        {
            "id": "109d64e4-b138-4ff7-9206-37fa19a37a64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7861
        },
        {
            "id": "098144ae-261d-4448-976d-3955cc9d67fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7862
        },
        {
            "id": "cf2bd1bd-8b00-41d0-846d-31610d2514d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7863
        },
        {
            "id": "cdb176c0-59a3-408a-a3d7-3384ee8edc18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7865
        },
        {
            "id": "ff03d6a9-1dc3-4036-aaea-6505d6fab8cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7867
        },
        {
            "id": "a11729b2-2c7d-4820-adf6-872680dce8f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7869
        },
        {
            "id": "d7b1390c-5f15-490b-ae13-1434af536e5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7871
        },
        {
            "id": "33ec1415-841d-43d9-86e0-9ce0ec20a120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7873
        },
        {
            "id": "4420832e-fcd6-4855-8d2a-0bf0b59ca409",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7875
        },
        {
            "id": "8fbeb7e0-92d2-4c98-b04f-d29826a549da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7877
        },
        {
            "id": "61406ada-78d6-4b97-b589-4534a53af130",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7879
        },
        {
            "id": "529ca824-73fd-48c6-b085-1ac5a2dc963c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 7881
        },
        {
            "id": "7c5af0d7-fc85-41b9-9736-41dda9a7ab01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7885
        },
        {
            "id": "381f547d-2f59-47dc-a2dc-6099b35ec739",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7887
        },
        {
            "id": "58091b69-acd5-4375-9c43-b66e42bb913f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7889
        },
        {
            "id": "815972fd-7d91-4cf2-98e4-43ba18d1254d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7891
        },
        {
            "id": "76ad8af4-d630-4787-843b-b31dccee03a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7893
        },
        {
            "id": "45b95aca-82a4-4b27-84bd-73995cdaf981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7895
        },
        {
            "id": "db684d39-9487-40db-b968-1bd5684d49c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7897
        },
        {
            "id": "c0f16fd2-6d1f-4c80-86a1-d885d8defb38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7899
        },
        {
            "id": "94ad642b-8fdc-47bf-8dd0-b14254bcebb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7901
        },
        {
            "id": "bda175f1-98d4-4dc0-a870-6672c34df7ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7903
        },
        {
            "id": "d8b1f53f-50dc-4b2a-a9ac-7ac2c98fa3fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7905
        },
        {
            "id": "66546330-0910-4858-b8aa-fffacb6412b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7907
        },
        {
            "id": "1d3d4ecc-7685-4aed-a425-3622e57d41a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7909
        },
        {
            "id": "8aee99de-81a8-441e-b21a-a61b8379b6d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7911
        },
        {
            "id": "f350b6e5-0c1f-4345-bcd2-1bc42d2862bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7913
        },
        {
            "id": "a6a079b7-584a-42b1-b48b-a06b48f9b021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7915
        },
        {
            "id": "191a95ce-a7f4-4cf4-879d-2b49c8792672",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7917
        },
        {
            "id": "c5554f82-b799-41d2-b09f-aa5a467a72c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7919
        },
        {
            "id": "60ac0b5a-3619-42d7-b1c5-9df62d906b7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7925
        },
        {
            "id": "a018cf47-7e18-458b-9fa8-6a79c3865ea7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 8208
        },
        {
            "id": "3a0f968c-f517-4565-8e42-0750c5c11889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 8209
        },
        {
            "id": "f08e1f7c-d8f7-438a-94cb-64b02a6ca3a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8211
        },
        {
            "id": "20ca2f66-b1bf-4ca6-8399-78fe4524d477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8212
        },
        {
            "id": "e9c41911-43e4-4f61-a270-d97053ffe617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8213
        },
        {
            "id": "7750ea62-34a5-400e-b561-68321ed63a02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8218
        },
        {
            "id": "bd427d09-4773-4fd4-a3bb-ad7ef8e04854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8222
        },
        {
            "id": "38e64fc6-67fc-42c2-8e9a-029a88e600b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8230
        },
        {
            "id": "cbbf0ca0-5879-4986-8dee-faae1006fbf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 8249
        },
        {
            "id": "318a4b13-5b1d-4543-ab38-e9de2d19967b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8250
        },
        {
            "id": "136c220f-04bc-40da-8604-85d2af5905fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 64256
        },
        {
            "id": "c268d1e5-ed8d-43d8-a9ab-43ed8aedac5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 64257
        },
        {
            "id": "30d00b08-a6c5-42aa-a493-e6e20f38f4f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 64258
        },
        {
            "id": "c989cf4e-73a2-4f74-aacf-bb51eec186e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 64259
        },
        {
            "id": "dc3741ea-67af-4d63-aee0-7294bb5d7a31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 64260
        },
        {
            "id": "211d905c-386a-4c53-ad46-1e2240334b50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 67
        },
        {
            "id": "47ce541a-b2e5-42cf-9183-ccc79b377aca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 71
        },
        {
            "id": "d1394d63-4fea-4a3f-a33a-1678b63bd0a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 79
        },
        {
            "id": "f27e7df9-4832-4903-9464-f76da86db615",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 81
        },
        {
            "id": "389afde0-f663-4f1d-841b-3a1c8587d465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 118
        },
        {
            "id": "08ef61af-9eed-496b-85de-fb6c0fc9be4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 119
        },
        {
            "id": "3c424f51-a43b-4bb6-947c-dedd4ccf5ce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 121
        },
        {
            "id": "f1892524-fa07-4a2c-bd6b-0e01955a0620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 199
        },
        {
            "id": "9320cf4e-0ab8-4870-a7bc-f3575a0edf8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 210
        },
        {
            "id": "f2d10568-ca2f-4a6e-acc2-3011fcd5de7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 211
        },
        {
            "id": "f3c86e7a-9be7-4211-a23d-acbd08700a64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 212
        },
        {
            "id": "091bed4b-12c8-453e-92c9-93f5f6f62440",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 213
        },
        {
            "id": "8c29ad10-89a2-4894-9174-d34d4462de75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 214
        },
        {
            "id": "5750529a-4eb6-47e2-8c14-bb8c40f28699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 216
        },
        {
            "id": "dec72c37-23a3-4d48-9549-f75666ae661a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 253
        },
        {
            "id": "5b9ce91d-27fa-4bb0-b499-222aeb454a75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 255
        },
        {
            "id": "960090a1-046b-4f07-9f3e-86e6045d666d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 262
        },
        {
            "id": "80512e68-e0fb-42b6-8e35-ee8c262caea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 264
        },
        {
            "id": "9fbcdd34-1cbc-4edf-8a30-56b1e30d07e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 266
        },
        {
            "id": "bbe0d5cd-05d1-434f-b5f6-af8a063dad56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 268
        },
        {
            "id": "1502bf44-e375-41b3-aeca-d9dabdbc6cde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 284
        },
        {
            "id": "8a5d896c-2acf-41a9-b751-1d6bcb2c9454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 286
        },
        {
            "id": "cbf3fa90-424c-4049-bbca-f7269ec46a7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 288
        },
        {
            "id": "61db08d8-3f1b-4914-9103-8a11bef29803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 290
        },
        {
            "id": "4a58b94a-1044-4c15-9b4f-5ecd908c40be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 332
        },
        {
            "id": "002083c0-eb56-42d4-a6ba-3b3c4b4ff1ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 334
        },
        {
            "id": "ec3198f6-667b-4d52-8601-8fded581b246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 336
        },
        {
            "id": "7ff463e9-0d72-4b00-af39-80c160fdcdcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 338
        },
        {
            "id": "fac96d09-7e70-4174-b2f3-670e8c5bc313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 373
        },
        {
            "id": "d4714bef-e8ce-4ef7-b153-d945590e030e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 375
        },
        {
            "id": "c420f6d0-fd2c-41ab-962b-d000058c9443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 510
        },
        {
            "id": "abb1ba16-edfd-4aab-82f7-6670af6066b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7809
        },
        {
            "id": "8c5f16aa-1a49-47be-8716-f94158fcd5a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7811
        },
        {
            "id": "171c6c7e-718b-4bdf-99d3-4d3f7c2fe544",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7813
        },
        {
            "id": "5bd9fe46-cb53-4093-9bcd-f4e824e52461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7923
        },
        {
            "id": "75647dc1-9b39-4778-8d02-ed8961b0b6e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7925
        },
        {
            "id": "695f4941-d037-477b-ba21-e7c64d50cfb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7927
        },
        {
            "id": "964a735b-53ef-4b2e-8a0c-d6e89bbd374a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7929
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}