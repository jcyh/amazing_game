/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 46BD6C8F
/// @DnDArgument : "code" "// Checks to see if doors are clear$(13_10)// If so, it will move all red doors back, play sound$(13_10)// ELSE, it will set alarm 1 for 5-15 frames$(13_10)$(13_10)if (object_red_switch.doorsAreClear)$(13_10){$(13_10)	object_red_door.image_index = 0;$(13_10)	audio_play_sound(sound_door , 50, false);$(13_10)	object_red_switch.isActivated = false;$(13_10)	// move all red doors into original position$(13_10)	var i;$(13_10)	for (i = 0; i < array_length_1d(red_doors); i++)$(13_10)	{$(13_10)		red_doors[i].y = red_doors[i].ystart;$(13_10)	}$(13_10)}$(13_10)else$(13_10){$(13_10)	alarm_set(1,5);$(13_10)}"
// Checks to see if doors are clear
// If so, it will move all red doors back, play sound
// ELSE, it will set alarm 1 for 5-15 frames

if (object_red_switch.doorsAreClear)
{
	object_red_door.image_index = 0;
	audio_play_sound(sound_door , 50, false);
	object_red_switch.isActivated = false;
	// move all red doors into original position
	var i;
	for (i = 0; i < array_length_1d(red_doors); i++)
	{
		red_doors[i].y = red_doors[i].ystart;
	}
}
else
{
	alarm_set(1,5);
}