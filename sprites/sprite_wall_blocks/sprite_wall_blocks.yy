{
    "id": "a959a9ce-1fdf-40c2-b0be-b2b087897895",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_wall_blocks",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cca84b6c-31eb-426c-a5c9-928f0647fdac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a959a9ce-1fdf-40c2-b0be-b2b087897895",
            "compositeImage": {
                "id": "82fc9460-2627-46e3-8fb4-2d575a6da0bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cca84b6c-31eb-426c-a5c9-928f0647fdac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87928323-4d7a-47f6-bd66-9cd1d6485d15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cca84b6c-31eb-426c-a5c9-928f0647fdac",
                    "LayerId": "9581e95c-dc42-4cea-a4ff-9da8237042a9"
                }
            ]
        },
        {
            "id": "c97c140b-b1ce-447b-8cda-99169346e1d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a959a9ce-1fdf-40c2-b0be-b2b087897895",
            "compositeImage": {
                "id": "9f449214-7c18-4fa9-863b-091c83950b58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c97c140b-b1ce-447b-8cda-99169346e1d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13d93b3a-9b1a-4ce3-93e5-b3e0ec67cf6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c97c140b-b1ce-447b-8cda-99169346e1d3",
                    "LayerId": "9581e95c-dc42-4cea-a4ff-9da8237042a9"
                }
            ]
        },
        {
            "id": "59355c92-ad31-40f1-b228-380084a4df47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a959a9ce-1fdf-40c2-b0be-b2b087897895",
            "compositeImage": {
                "id": "f73179a9-95e7-4e5f-b5ef-ea41f89d4b14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59355c92-ad31-40f1-b228-380084a4df47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "260b19ed-983a-44a2-b28a-5e988a78d23e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59355c92-ad31-40f1-b228-380084a4df47",
                    "LayerId": "9581e95c-dc42-4cea-a4ff-9da8237042a9"
                }
            ]
        },
        {
            "id": "32eeae42-19e9-465f-8965-9da999a6a05a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a959a9ce-1fdf-40c2-b0be-b2b087897895",
            "compositeImage": {
                "id": "ddeb9507-fe33-416a-aebe-abc8103a8f2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32eeae42-19e9-465f-8965-9da999a6a05a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "326f1bd5-eeec-48c2-8483-9b247a91a301",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32eeae42-19e9-465f-8965-9da999a6a05a",
                    "LayerId": "9581e95c-dc42-4cea-a4ff-9da8237042a9"
                }
            ]
        },
        {
            "id": "b798fca6-1f5c-4350-8c87-39ddc2f53bb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a959a9ce-1fdf-40c2-b0be-b2b087897895",
            "compositeImage": {
                "id": "38424663-7c8a-4609-b5d5-c49bcb269f5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b798fca6-1f5c-4350-8c87-39ddc2f53bb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f90139d-f177-43ca-9fe2-460dacd3bc64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b798fca6-1f5c-4350-8c87-39ddc2f53bb3",
                    "LayerId": "9581e95c-dc42-4cea-a4ff-9da8237042a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9581e95c-dc42-4cea-a4ff-9da8237042a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a959a9ce-1fdf-40c2-b0be-b2b087897895",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}