{
    "id": "34dba03d-8014-4cdd-b5a2-98f3be2b8b09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_darkness",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2047,
    "bbox_left": 0,
    "bbox_right": 2047,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0a5bca4-4a80-4e80-8c82-85cf3a1bcf51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34dba03d-8014-4cdd-b5a2-98f3be2b8b09",
            "compositeImage": {
                "id": "0b019f93-5e05-45b8-8ba4-b7d329ee9337",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0a5bca4-4a80-4e80-8c82-85cf3a1bcf51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "415e9436-1df9-4b01-b76a-4061333a5885",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0a5bca4-4a80-4e80-8c82-85cf3a1bcf51",
                    "LayerId": "9a84faa0-2da7-4a44-a212-dfb10665075e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2048,
    "layers": [
        {
            "id": "9a84faa0-2da7-4a44-a212-dfb10665075e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34dba03d-8014-4cdd-b5a2-98f3be2b8b09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2048,
    "xorig": 1024,
    "yorig": 1024
}