{
    "id": "8a752168-d642-40aa-90ed-1205f25d7b21",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_torch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 8,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c3e743c-dead-4d0b-b5be-1539cbcf53e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a752168-d642-40aa-90ed-1205f25d7b21",
            "compositeImage": {
                "id": "765b0a76-eb8f-4828-bf9c-af4e70de4433",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c3e743c-dead-4d0b-b5be-1539cbcf53e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd4acbdd-3fe4-4014-a626-2ab45f333a7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c3e743c-dead-4d0b-b5be-1539cbcf53e3",
                    "LayerId": "35a3c20d-871c-4c7a-864e-be2476a78b80"
                }
            ]
        },
        {
            "id": "6ebbafbe-26b4-461e-bb72-e623d1d2817b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a752168-d642-40aa-90ed-1205f25d7b21",
            "compositeImage": {
                "id": "85a62240-fae7-497d-8138-9f9e9081f3ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ebbafbe-26b4-461e-bb72-e623d1d2817b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b249b42-a94a-4fec-9a22-e643388d6c60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ebbafbe-26b4-461e-bb72-e623d1d2817b",
                    "LayerId": "35a3c20d-871c-4c7a-864e-be2476a78b80"
                }
            ]
        },
        {
            "id": "6dfa8b58-2ff4-467e-9220-a4f2bee1c8c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a752168-d642-40aa-90ed-1205f25d7b21",
            "compositeImage": {
                "id": "0feacb15-0471-4b51-8b00-d0b01cf76d9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dfa8b58-2ff4-467e-9220-a4f2bee1c8c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "984f2796-dfcc-4f26-8b50-61c74f12bf86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dfa8b58-2ff4-467e-9220-a4f2bee1c8c0",
                    "LayerId": "35a3c20d-871c-4c7a-864e-be2476a78b80"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "35a3c20d-871c-4c7a-864e-be2476a78b80",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a752168-d642-40aa-90ed-1205f25d7b21",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 29
}