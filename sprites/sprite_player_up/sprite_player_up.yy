{
    "id": "4e79e179-72c1-455e-b9de-9c15d13c54a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 3,
    "bbox_right": 27,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e59ec5ef-4ccb-4d43-bee5-c355ce22a741",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e79e179-72c1-455e-b9de-9c15d13c54a4",
            "compositeImage": {
                "id": "7643f9a9-3a95-4163-91f3-87763d8dafc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e59ec5ef-4ccb-4d43-bee5-c355ce22a741",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9fcdcc9-579a-4733-8db3-bbfd9768172f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e59ec5ef-4ccb-4d43-bee5-c355ce22a741",
                    "LayerId": "8992898d-a2f7-44b6-8844-2e299726751e"
                }
            ]
        },
        {
            "id": "5d260799-28ea-41ed-92bd-551a01afddcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e79e179-72c1-455e-b9de-9c15d13c54a4",
            "compositeImage": {
                "id": "c778ff14-cd61-4c2e-a257-b6ce56e42253",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d260799-28ea-41ed-92bd-551a01afddcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b7785a8-372e-4372-a2a0-183fb0b206dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d260799-28ea-41ed-92bd-551a01afddcf",
                    "LayerId": "8992898d-a2f7-44b6-8844-2e299726751e"
                }
            ]
        },
        {
            "id": "d59020bf-355b-40a3-aa3f-fddbd71f5b22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e79e179-72c1-455e-b9de-9c15d13c54a4",
            "compositeImage": {
                "id": "38d46d51-ab7f-4117-86d7-1691e7d3e4e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d59020bf-355b-40a3-aa3f-fddbd71f5b22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55695eac-867a-4f82-a3f8-a1ebf6e48fd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d59020bf-355b-40a3-aa3f-fddbd71f5b22",
                    "LayerId": "8992898d-a2f7-44b6-8844-2e299726751e"
                }
            ]
        },
        {
            "id": "e8ae62ba-6953-409f-a37b-431ad19e1c3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e79e179-72c1-455e-b9de-9c15d13c54a4",
            "compositeImage": {
                "id": "60f46382-a57c-4c00-9f7c-b580b2bd60ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8ae62ba-6953-409f-a37b-431ad19e1c3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "929ffd6c-4ceb-4262-9c47-1064d1c7d707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8ae62ba-6953-409f-a37b-431ad19e1c3a",
                    "LayerId": "8992898d-a2f7-44b6-8844-2e299726751e"
                }
            ]
        },
        {
            "id": "533fb268-2009-44b2-be34-4fdc61d307a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e79e179-72c1-455e-b9de-9c15d13c54a4",
            "compositeImage": {
                "id": "a6243d13-92ec-428c-b003-21803379c4d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "533fb268-2009-44b2-be34-4fdc61d307a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86b03a44-c29f-4f4a-bbaa-0c755cc26db3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "533fb268-2009-44b2-be34-4fdc61d307a7",
                    "LayerId": "8992898d-a2f7-44b6-8844-2e299726751e"
                }
            ]
        },
        {
            "id": "59186769-df36-4a32-8cba-020ad335c125",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e79e179-72c1-455e-b9de-9c15d13c54a4",
            "compositeImage": {
                "id": "9cc53f0a-2c73-4065-ab77-143d1bf92bdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59186769-df36-4a32-8cba-020ad335c125",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1027811c-b147-4c12-8971-afcb245c937f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59186769-df36-4a32-8cba-020ad335c125",
                    "LayerId": "8992898d-a2f7-44b6-8844-2e299726751e"
                }
            ]
        },
        {
            "id": "98f2d182-5166-4ce8-a973-e761ba9d715c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e79e179-72c1-455e-b9de-9c15d13c54a4",
            "compositeImage": {
                "id": "0c0258bd-d4ad-4a20-910d-57de326aa5d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98f2d182-5166-4ce8-a973-e761ba9d715c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdcb77f4-4837-440f-99c5-e5aac2a1bda4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98f2d182-5166-4ce8-a973-e761ba9d715c",
                    "LayerId": "8992898d-a2f7-44b6-8844-2e299726751e"
                }
            ]
        },
        {
            "id": "880c776d-1631-4027-8e3e-dfbee5f458b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e79e179-72c1-455e-b9de-9c15d13c54a4",
            "compositeImage": {
                "id": "506f92a0-3dc2-402b-b4c3-9695c91b551a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "880c776d-1631-4027-8e3e-dfbee5f458b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65c5f99b-c3ad-4bab-b200-da06f7530a8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "880c776d-1631-4027-8e3e-dfbee5f458b1",
                    "LayerId": "8992898d-a2f7-44b6-8844-2e299726751e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8992898d-a2f7-44b6-8844-2e299726751e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e79e179-72c1-455e-b9de-9c15d13c54a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}