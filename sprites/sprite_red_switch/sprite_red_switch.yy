{
    "id": "d6b78fe7-1d27-4535-bfc2-6fbee796c725",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_red_switch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ffd338d-d702-452e-bf27-ec64fee8518a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6b78fe7-1d27-4535-bfc2-6fbee796c725",
            "compositeImage": {
                "id": "d604080a-f95f-4cb2-b47c-891912954953",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ffd338d-d702-452e-bf27-ec64fee8518a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5895da2-9f92-4705-bf36-0707a2cde91b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ffd338d-d702-452e-bf27-ec64fee8518a",
                    "LayerId": "cdf402d8-c6dd-4adf-b12a-124bc3bfd9ad"
                }
            ]
        },
        {
            "id": "66b211de-7c55-4d2d-8d68-01f66aa468fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6b78fe7-1d27-4535-bfc2-6fbee796c725",
            "compositeImage": {
                "id": "a1959468-4a8b-4a78-840f-1f824d917d69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66b211de-7c55-4d2d-8d68-01f66aa468fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9278ff6e-9914-47a8-bdec-50ba959a8bf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66b211de-7c55-4d2d-8d68-01f66aa468fb",
                    "LayerId": "cdf402d8-c6dd-4adf-b12a-124bc3bfd9ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cdf402d8-c6dd-4adf-b12a-124bc3bfd9ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6b78fe7-1d27-4535-bfc2-6fbee796c725",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}