{
    "id": "c0b6f91e-790c-44b2-b4d6-966658134364",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 3,
    "bbox_right": 27,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12d4e326-8608-4aee-97e3-e5a47219c5a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0b6f91e-790c-44b2-b4d6-966658134364",
            "compositeImage": {
                "id": "a12a94c2-12c9-417b-8bc6-fa89f3be6e4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12d4e326-8608-4aee-97e3-e5a47219c5a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d42a334-8518-423c-ad7e-1a3667fab86e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12d4e326-8608-4aee-97e3-e5a47219c5a4",
                    "LayerId": "58806812-5d98-4aef-a0ed-5bcbfa592b98"
                }
            ]
        },
        {
            "id": "f9ad4b7b-79eb-45ee-b590-8144cb87924c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0b6f91e-790c-44b2-b4d6-966658134364",
            "compositeImage": {
                "id": "421353cb-2c56-4e21-9eff-a3dfa20a276a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9ad4b7b-79eb-45ee-b590-8144cb87924c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f35596da-9a26-4952-a047-6389a76bae88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9ad4b7b-79eb-45ee-b590-8144cb87924c",
                    "LayerId": "58806812-5d98-4aef-a0ed-5bcbfa592b98"
                }
            ]
        },
        {
            "id": "08f81755-f045-4221-8e7b-d005852a71c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0b6f91e-790c-44b2-b4d6-966658134364",
            "compositeImage": {
                "id": "6c437b56-32a8-4047-81a2-18c09a4cde23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08f81755-f045-4221-8e7b-d005852a71c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fa89227-0434-4ff8-8c7d-ce6ccfb2f1a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08f81755-f045-4221-8e7b-d005852a71c2",
                    "LayerId": "58806812-5d98-4aef-a0ed-5bcbfa592b98"
                }
            ]
        },
        {
            "id": "df5de057-9642-4fa1-852d-3eb2b2922e0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0b6f91e-790c-44b2-b4d6-966658134364",
            "compositeImage": {
                "id": "5716182c-f31c-41f7-9eff-048202a8e9d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df5de057-9642-4fa1-852d-3eb2b2922e0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51dc992e-85a7-40d2-802e-f955bc89611a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df5de057-9642-4fa1-852d-3eb2b2922e0a",
                    "LayerId": "58806812-5d98-4aef-a0ed-5bcbfa592b98"
                }
            ]
        },
        {
            "id": "b899a397-a59a-4ad8-a38a-cbe06e5cc651",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0b6f91e-790c-44b2-b4d6-966658134364",
            "compositeImage": {
                "id": "cbb40da1-2dbb-4ab2-ab06-0f0260f3955c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b899a397-a59a-4ad8-a38a-cbe06e5cc651",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceed9b22-b891-4c40-ad5b-1cdda8e80a26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b899a397-a59a-4ad8-a38a-cbe06e5cc651",
                    "LayerId": "58806812-5d98-4aef-a0ed-5bcbfa592b98"
                }
            ]
        },
        {
            "id": "4d62c7a5-4b3a-482f-8e14-dd0160bd13ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0b6f91e-790c-44b2-b4d6-966658134364",
            "compositeImage": {
                "id": "9b09fabc-bfcf-4c26-b6a6-2edd3e953c58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d62c7a5-4b3a-482f-8e14-dd0160bd13ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a65ed281-33ff-458c-ba42-04e48f8964fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d62c7a5-4b3a-482f-8e14-dd0160bd13ae",
                    "LayerId": "58806812-5d98-4aef-a0ed-5bcbfa592b98"
                }
            ]
        },
        {
            "id": "ca702bd3-64db-48e4-9b2a-d0201eadd7d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0b6f91e-790c-44b2-b4d6-966658134364",
            "compositeImage": {
                "id": "2a8a20cd-5441-4bdb-83ae-e7a38a583b7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca702bd3-64db-48e4-9b2a-d0201eadd7d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf2f7342-81f8-4d7e-9947-04e002ff81b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca702bd3-64db-48e4-9b2a-d0201eadd7d7",
                    "LayerId": "58806812-5d98-4aef-a0ed-5bcbfa592b98"
                }
            ]
        },
        {
            "id": "34417be9-63e2-4221-9852-1a7a1c1f1052",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0b6f91e-790c-44b2-b4d6-966658134364",
            "compositeImage": {
                "id": "cad6d475-0cba-454a-b867-13589a6435e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34417be9-63e2-4221-9852-1a7a1c1f1052",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b7e0967-70da-43f2-bea9-edc221f5ecdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34417be9-63e2-4221-9852-1a7a1c1f1052",
                    "LayerId": "58806812-5d98-4aef-a0ed-5bcbfa592b98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "58806812-5d98-4aef-a0ed-5bcbfa592b98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0b6f91e-790c-44b2-b4d6-966658134364",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}