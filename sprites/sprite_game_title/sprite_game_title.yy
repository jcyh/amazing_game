{
    "id": "6cecf46f-1518-49ab-8d77-d1edd0739aad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_game_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1388b5b-42bb-4e14-8f8e-1f744e4a1d27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cecf46f-1518-49ab-8d77-d1edd0739aad",
            "compositeImage": {
                "id": "f38c4578-63f9-4c7d-bbc9-ad64da0ae780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1388b5b-42bb-4e14-8f8e-1f744e4a1d27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d455bd77-49ca-4738-9837-730f1010f7e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1388b5b-42bb-4e14-8f8e-1f744e4a1d27",
                    "LayerId": "a20c27c8-5b27-4764-bc89-a6a91349778e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "a20c27c8-5b27-4764-bc89-a6a91349778e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6cecf46f-1518-49ab-8d77-d1edd0739aad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}