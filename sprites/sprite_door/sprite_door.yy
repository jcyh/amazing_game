{
    "id": "36e9d2bc-c39b-4b93-acc2-43fd11112509",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "408e2603-a588-4777-b770-93d381102ecc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36e9d2bc-c39b-4b93-acc2-43fd11112509",
            "compositeImage": {
                "id": "3628a2ae-7209-4c3c-a32f-ee2a0f31656e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "408e2603-a588-4777-b770-93d381102ecc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba024b2c-5739-4a5b-b6ac-449414d89920",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "408e2603-a588-4777-b770-93d381102ecc",
                    "LayerId": "8dbebc03-4ac0-4711-acc1-f84cd03bf809"
                }
            ]
        },
        {
            "id": "c26d9a5c-1736-41ac-b980-9b1f9a5a81c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36e9d2bc-c39b-4b93-acc2-43fd11112509",
            "compositeImage": {
                "id": "19983176-5f16-4ea2-b547-87dd33e16eff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c26d9a5c-1736-41ac-b980-9b1f9a5a81c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8fb3965-0005-40ce-93dc-7b75dde4df5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c26d9a5c-1736-41ac-b980-9b1f9a5a81c0",
                    "LayerId": "8dbebc03-4ac0-4711-acc1-f84cd03bf809"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8dbebc03-4ac0-4711-acc1-f84cd03bf809",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36e9d2bc-c39b-4b93-acc2-43fd11112509",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}