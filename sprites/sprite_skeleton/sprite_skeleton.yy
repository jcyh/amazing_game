{
    "id": "60fc826e-3f91-40f4-8339-71b4346e2a3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_skeleton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d512620-aca1-41d6-a3da-4395d7cb5a90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60fc826e-3f91-40f4-8339-71b4346e2a3b",
            "compositeImage": {
                "id": "6dc69f50-34cc-42e9-b742-028e19517c56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d512620-aca1-41d6-a3da-4395d7cb5a90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3632defb-5f0f-4d12-a5a6-f408bf7ea9ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d512620-aca1-41d6-a3da-4395d7cb5a90",
                    "LayerId": "c56cfe3a-043e-4fed-8d26-119eff2126ea"
                }
            ]
        },
        {
            "id": "07007b74-28a7-431f-9872-fcc9b7f72f64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60fc826e-3f91-40f4-8339-71b4346e2a3b",
            "compositeImage": {
                "id": "6bdc4d3c-f634-4e70-a83a-fd33d9308161",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07007b74-28a7-431f-9872-fcc9b7f72f64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82b767f4-6913-46d2-a785-da85c34bba45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07007b74-28a7-431f-9872-fcc9b7f72f64",
                    "LayerId": "c56cfe3a-043e-4fed-8d26-119eff2126ea"
                }
            ]
        },
        {
            "id": "1cd6fbda-07da-4192-9e0e-afa5e2522a12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60fc826e-3f91-40f4-8339-71b4346e2a3b",
            "compositeImage": {
                "id": "72822a08-37ac-4575-a161-47d26cd12dda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cd6fbda-07da-4192-9e0e-afa5e2522a12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8b10519-7fdf-4c55-8014-e6d422de150b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cd6fbda-07da-4192-9e0e-afa5e2522a12",
                    "LayerId": "c56cfe3a-043e-4fed-8d26-119eff2126ea"
                }
            ]
        },
        {
            "id": "f74f20fd-4fa1-4a46-ab8b-4ed80a563327",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60fc826e-3f91-40f4-8339-71b4346e2a3b",
            "compositeImage": {
                "id": "89c5f206-9e30-46ca-8aca-b410533c225b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f74f20fd-4fa1-4a46-ab8b-4ed80a563327",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee8eeb72-f7c9-48d1-8866-97c3fa81a23e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f74f20fd-4fa1-4a46-ab8b-4ed80a563327",
                    "LayerId": "c56cfe3a-043e-4fed-8d26-119eff2126ea"
                }
            ]
        },
        {
            "id": "9b39a13a-1863-4cd7-b431-7cf176f97696",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60fc826e-3f91-40f4-8339-71b4346e2a3b",
            "compositeImage": {
                "id": "c6063295-d4ba-4a72-8370-33bd9ddde90e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b39a13a-1863-4cd7-b431-7cf176f97696",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bac4762-add3-45ec-bd1e-c3b551f26c5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b39a13a-1863-4cd7-b431-7cf176f97696",
                    "LayerId": "c56cfe3a-043e-4fed-8d26-119eff2126ea"
                }
            ]
        },
        {
            "id": "da7ed5ab-7839-47e1-ab43-eda007d090cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60fc826e-3f91-40f4-8339-71b4346e2a3b",
            "compositeImage": {
                "id": "3f1ed894-44a2-4e24-995d-260314273586",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da7ed5ab-7839-47e1-ab43-eda007d090cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6512d185-57a9-4713-8837-d18943a8fbdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da7ed5ab-7839-47e1-ab43-eda007d090cb",
                    "LayerId": "c56cfe3a-043e-4fed-8d26-119eff2126ea"
                }
            ]
        },
        {
            "id": "bd24dce3-e65c-4772-a580-8af5d47e82a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60fc826e-3f91-40f4-8339-71b4346e2a3b",
            "compositeImage": {
                "id": "435a7347-e5be-44b6-96d9-8f116c2eab59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd24dce3-e65c-4772-a580-8af5d47e82a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4115ecea-cda6-4682-9766-28f7952db894",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd24dce3-e65c-4772-a580-8af5d47e82a7",
                    "LayerId": "c56cfe3a-043e-4fed-8d26-119eff2126ea"
                }
            ]
        },
        {
            "id": "bd5ad027-ea6d-40ad-a680-3355f0642c44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60fc826e-3f91-40f4-8339-71b4346e2a3b",
            "compositeImage": {
                "id": "d5843160-7a37-423f-bd3f-a2a5e2a4a2a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd5ad027-ea6d-40ad-a680-3355f0642c44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2311d1d-8f46-452e-8ebc-d92b987c1a24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd5ad027-ea6d-40ad-a680-3355f0642c44",
                    "LayerId": "c56cfe3a-043e-4fed-8d26-119eff2126ea"
                }
            ]
        },
        {
            "id": "4c1a8560-4ba6-4a8b-8060-879336d4fb58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60fc826e-3f91-40f4-8339-71b4346e2a3b",
            "compositeImage": {
                "id": "8ff4face-3641-46ae-86c3-16b4b012b1ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c1a8560-4ba6-4a8b-8060-879336d4fb58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51d52165-ffcb-4a3f-8ff2-7e30fd2c66e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c1a8560-4ba6-4a8b-8060-879336d4fb58",
                    "LayerId": "c56cfe3a-043e-4fed-8d26-119eff2126ea"
                }
            ]
        },
        {
            "id": "81f564ea-9875-4ac3-9e48-c29daf60a4fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60fc826e-3f91-40f4-8339-71b4346e2a3b",
            "compositeImage": {
                "id": "001a1cac-91a3-43f6-994f-3f67826b730c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81f564ea-9875-4ac3-9e48-c29daf60a4fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6fa76bc-d822-4a59-9757-afd09d95a97c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81f564ea-9875-4ac3-9e48-c29daf60a4fa",
                    "LayerId": "c56cfe3a-043e-4fed-8d26-119eff2126ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c56cfe3a-043e-4fed-8d26-119eff2126ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60fc826e-3f91-40f4-8339-71b4346e2a3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}