{
    "id": "cf1e3768-27f7-4adf-ba99-a96d65160372",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 3,
    "bbox_right": 27,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2bc2dcff-58a2-44c4-98e7-77ae1aa4a40c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf1e3768-27f7-4adf-ba99-a96d65160372",
            "compositeImage": {
                "id": "8830beb0-9537-4f6c-976d-f5a83dd7845b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bc2dcff-58a2-44c4-98e7-77ae1aa4a40c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27d392ec-aef0-44c0-b31b-d5fac723974e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bc2dcff-58a2-44c4-98e7-77ae1aa4a40c",
                    "LayerId": "b10a1688-a579-4aca-b77c-d3c55f345866"
                }
            ]
        },
        {
            "id": "27faadf6-8786-46b9-8279-e7a1e3eed457",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf1e3768-27f7-4adf-ba99-a96d65160372",
            "compositeImage": {
                "id": "5b310c9f-e340-428a-987f-5eea970f0104",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27faadf6-8786-46b9-8279-e7a1e3eed457",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e4dbbd6-dd91-4689-b26e-088227e1f060",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27faadf6-8786-46b9-8279-e7a1e3eed457",
                    "LayerId": "b10a1688-a579-4aca-b77c-d3c55f345866"
                }
            ]
        },
        {
            "id": "a4be8da7-971e-4c09-9c10-ace28d159482",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf1e3768-27f7-4adf-ba99-a96d65160372",
            "compositeImage": {
                "id": "e1498688-adc6-4c0b-9afb-7b4688fb7c17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4be8da7-971e-4c09-9c10-ace28d159482",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3da7ebc0-db70-4af8-8b57-033ea38d28e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4be8da7-971e-4c09-9c10-ace28d159482",
                    "LayerId": "b10a1688-a579-4aca-b77c-d3c55f345866"
                }
            ]
        },
        {
            "id": "8fa46b07-0de3-4cc5-83ab-5390c5a0390e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf1e3768-27f7-4adf-ba99-a96d65160372",
            "compositeImage": {
                "id": "229abf14-c928-4905-ae60-48578fe51e7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fa46b07-0de3-4cc5-83ab-5390c5a0390e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d65ea237-673d-4dae-8f14-2327d8020786",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fa46b07-0de3-4cc5-83ab-5390c5a0390e",
                    "LayerId": "b10a1688-a579-4aca-b77c-d3c55f345866"
                }
            ]
        },
        {
            "id": "b25ef8b8-c033-4ea6-ba6b-df2279075835",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf1e3768-27f7-4adf-ba99-a96d65160372",
            "compositeImage": {
                "id": "97b457bc-6984-4631-ac1a-8d19f38ea1d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b25ef8b8-c033-4ea6-ba6b-df2279075835",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0256ae41-83c1-4e15-ac19-1a305dba25ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b25ef8b8-c033-4ea6-ba6b-df2279075835",
                    "LayerId": "b10a1688-a579-4aca-b77c-d3c55f345866"
                }
            ]
        },
        {
            "id": "c1512c30-61ca-429e-922e-82c984217fc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf1e3768-27f7-4adf-ba99-a96d65160372",
            "compositeImage": {
                "id": "6786e524-44bc-40d6-bbd0-55a16ba1a5a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1512c30-61ca-429e-922e-82c984217fc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f3184cb-a9bb-4a10-adca-7cfbb0baab4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1512c30-61ca-429e-922e-82c984217fc3",
                    "LayerId": "b10a1688-a579-4aca-b77c-d3c55f345866"
                }
            ]
        },
        {
            "id": "421301a3-632b-4f66-8886-a9918a8ce35e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf1e3768-27f7-4adf-ba99-a96d65160372",
            "compositeImage": {
                "id": "d398f33d-0b6c-482b-b4d1-da6c156172f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "421301a3-632b-4f66-8886-a9918a8ce35e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0f5c21e-a444-4c64-8c1a-2de417969a13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "421301a3-632b-4f66-8886-a9918a8ce35e",
                    "LayerId": "b10a1688-a579-4aca-b77c-d3c55f345866"
                }
            ]
        },
        {
            "id": "980bda54-2af5-4b01-b313-d127ac5720d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf1e3768-27f7-4adf-ba99-a96d65160372",
            "compositeImage": {
                "id": "ced9fa06-1e10-438f-acc9-f08942468af1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "980bda54-2af5-4b01-b313-d127ac5720d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "631ce0a5-16d8-4a72-987b-24a3d9c4d6f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "980bda54-2af5-4b01-b313-d127ac5720d4",
                    "LayerId": "b10a1688-a579-4aca-b77c-d3c55f345866"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b10a1688-a579-4aca-b77c-d3c55f345866",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf1e3768-27f7-4adf-ba99-a96d65160372",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}