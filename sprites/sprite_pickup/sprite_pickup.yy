{
    "id": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_pickup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de2141a7-967e-4399-b45c-752400b1c166",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "45b7a5d9-e83b-474d-8e00-ec2d573ad0cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de2141a7-967e-4399-b45c-752400b1c166",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07c7ef3c-dc75-4820-bc7a-32d90f33310f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de2141a7-967e-4399-b45c-752400b1c166",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "a18fc14a-b695-4192-89a8-c3b146a84083",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "ca68434f-e1a8-435a-9e16-63426a217806",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a18fc14a-b695-4192-89a8-c3b146a84083",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46885c84-ccf9-4255-b15f-f5f5dd4ce00b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a18fc14a-b695-4192-89a8-c3b146a84083",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "f13ac1f9-8f37-4c42-99df-7ddef44372b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "dc1bb77c-92db-4d69-9d1c-5b71247f686b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f13ac1f9-8f37-4c42-99df-7ddef44372b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26832e79-1888-465a-a747-bc3bdd054664",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f13ac1f9-8f37-4c42-99df-7ddef44372b0",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "a73b9e87-0e98-428f-8ce4-05e3e74677e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "d42d94ba-cb2e-4083-a60a-58e1cefb3ae9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a73b9e87-0e98-428f-8ce4-05e3e74677e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89ccc317-5ec6-4a88-9e8f-b6bf56c2d8b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a73b9e87-0e98-428f-8ce4-05e3e74677e4",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "9895eb56-51dc-40eb-b15e-2b72f49b1e7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "ccfb8dd2-d233-48a3-9108-c874250bfbbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9895eb56-51dc-40eb-b15e-2b72f49b1e7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc6a6bcf-5d37-478e-b5d1-1de89eae274b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9895eb56-51dc-40eb-b15e-2b72f49b1e7c",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "290c8779-cf46-44b7-ae16-877e285f4083",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "93eb7049-9eca-4b82-99ef-d75046f0329e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "290c8779-cf46-44b7-ae16-877e285f4083",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9a3340d-c870-487d-8d56-41ebfc75ad15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "290c8779-cf46-44b7-ae16-877e285f4083",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "7435cc99-3544-472c-9112-4543f4179075",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "957c19d7-2e7d-4ec7-adb4-1fefe0c4adaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7435cc99-3544-472c-9112-4543f4179075",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c00aaae-0b5f-4bbe-96cf-24954629fdc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7435cc99-3544-472c-9112-4543f4179075",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "afa2c02c-e33e-4f31-b18c-88587005625c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "b696f5e8-dcc5-4002-b3e3-41f2dba50786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afa2c02c-e33e-4f31-b18c-88587005625c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79c5af49-84f8-4322-8019-b2191a631907",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afa2c02c-e33e-4f31-b18c-88587005625c",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "54b8f5c1-1899-4c61-b4d5-3f95b5977bcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "7467061e-7418-439d-88a8-28ee7d60d38c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54b8f5c1-1899-4c61-b4d5-3f95b5977bcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a0d51b0-93b4-4bb3-bf5c-46b160792772",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54b8f5c1-1899-4c61-b4d5-3f95b5977bcf",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "4eaa28d1-3861-4147-b7a9-8699fcdb99a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "12925621-dc63-48e4-97e6-662ec25b36e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4eaa28d1-3861-4147-b7a9-8699fcdb99a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc978fd9-949c-4e94-9a60-fd223ebf8d98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eaa28d1-3861-4147-b7a9-8699fcdb99a5",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "7e8c4698-66e8-401b-9cfc-97a3d2a8cae4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "67411354-b798-4115-8ecb-09412a6136a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e8c4698-66e8-401b-9cfc-97a3d2a8cae4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47dd9363-1a75-40c7-9c25-321c7267267a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e8c4698-66e8-401b-9cfc-97a3d2a8cae4",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "74de29dd-3166-4e34-b61c-313277864c87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "40d63f84-f819-4b84-8e57-1ec563c94cb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74de29dd-3166-4e34-b61c-313277864c87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b840fb8-ce6d-43bc-9b7c-9f30dde1a5a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74de29dd-3166-4e34-b61c-313277864c87",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "8ab77d75-3679-4da4-9a0d-5a19d4972b4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "e08e8556-6dc8-4014-a8c0-71312de324c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ab77d75-3679-4da4-9a0d-5a19d4972b4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1c79575-efb9-4576-be97-dcec26002d69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ab77d75-3679-4da4-9a0d-5a19d4972b4a",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "4980f6d2-c4ea-4846-9444-a7e6887700de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "d9630816-1f96-40fc-86a8-7e1279b32048",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4980f6d2-c4ea-4846-9444-a7e6887700de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9216949f-328f-4100-97d1-dda63d2d13a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4980f6d2-c4ea-4846-9444-a7e6887700de",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "8773b184-8e5f-4a71-bf0c-20b1fe33ea73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "d6c652c0-cb67-4a89-8b74-b7b4aa16a0bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8773b184-8e5f-4a71-bf0c-20b1fe33ea73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7a9e5b1-4f69-4b24-98bb-d7d2daa74ed0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8773b184-8e5f-4a71-bf0c-20b1fe33ea73",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "66be8702-b32e-4f77-9504-910596548d88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "234b1b82-637f-49fe-bb42-b97663cf1598",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66be8702-b32e-4f77-9504-910596548d88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8939c535-15fd-4ddb-b9a3-8375a1111adb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66be8702-b32e-4f77-9504-910596548d88",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "f57636b1-3439-4e68-9dfd-5a3e3963a71d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "8d1935f2-43bb-42b5-aac2-8eecd1912cf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f57636b1-3439-4e68-9dfd-5a3e3963a71d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58518e44-59e0-4ebc-a8ef-4f7ebd3a6ec6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f57636b1-3439-4e68-9dfd-5a3e3963a71d",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "0a14fcd3-974f-4ab4-baf9-2f1a22d05bce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "9b7a9c5e-5e29-4687-938e-1816f7711b8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a14fcd3-974f-4ab4-baf9-2f1a22d05bce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9d36e6c-9f4b-40b2-aab9-3c682724faa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a14fcd3-974f-4ab4-baf9-2f1a22d05bce",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "642e70e8-1d6c-4a97-aa22-566acb87787c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "ff790aee-ba21-40a4-933c-f489ed993dfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "642e70e8-1d6c-4a97-aa22-566acb87787c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51fe9f4c-f797-4ce5-8e42-a4acb4760c46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "642e70e8-1d6c-4a97-aa22-566acb87787c",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "ed87ba1e-f0c9-49f7-9c23-036db15de02e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "27ebee88-8a66-47b1-954b-7bb1377f10c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed87ba1e-f0c9-49f7-9c23-036db15de02e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11dd8e67-fd1a-4b93-9efc-a6fe516393a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed87ba1e-f0c9-49f7-9c23-036db15de02e",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "34f7a45a-c4f1-448d-9ad6-0b0cb4ef1129",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "6df804da-48db-4cb9-ac1a-c85fb34398bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34f7a45a-c4f1-448d-9ad6-0b0cb4ef1129",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c158b6f7-c75a-4d5b-8d2d-9d2274af7b04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34f7a45a-c4f1-448d-9ad6-0b0cb4ef1129",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "a765b278-c787-47b0-9dda-86d2ea29aaed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "477d53fe-f434-4270-8b39-c091103e272d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a765b278-c787-47b0-9dda-86d2ea29aaed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b89f7221-d9a1-4fcc-bb57-4010bda20620",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a765b278-c787-47b0-9dda-86d2ea29aaed",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "eed3ba24-1ece-42c1-8ead-9240fb724f5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "815a4dfc-9b80-4314-8e6b-15e4efcd2185",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eed3ba24-1ece-42c1-8ead-9240fb724f5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b85a828-1087-4093-991d-7a8b39818660",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eed3ba24-1ece-42c1-8ead-9240fb724f5e",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "0c9c6cb0-ffd0-4bfe-9367-4c705324c5fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "824b69f4-f703-47a3-8e0d-ddde45d2d8e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c9c6cb0-ffd0-4bfe-9367-4c705324c5fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "994d6c66-b0fe-49a2-81a0-294ba434c60a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c9c6cb0-ffd0-4bfe-9367-4c705324c5fa",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "55473974-82af-4cbd-92ed-e54a1a63b5fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "84cc3427-6ecd-4e7c-96e4-13ae630f7717",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55473974-82af-4cbd-92ed-e54a1a63b5fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a41e79a-ca09-4d88-ac92-2332ab0befbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55473974-82af-4cbd-92ed-e54a1a63b5fc",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "f4fc2945-8bda-46bd-b9fe-dd7b076198fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "6a6f17b8-bbcc-4c16-901d-d367d37a0ae6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4fc2945-8bda-46bd-b9fe-dd7b076198fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "335899cb-c2a8-4cdb-8174-11c4d1241ea1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4fc2945-8bda-46bd-b9fe-dd7b076198fe",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        },
        {
            "id": "aa5ea846-2a50-4204-bd61-08dbcf3a7d28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "compositeImage": {
                "id": "4c1b8b5b-d0a1-426e-8aa6-fb026e883980",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa5ea846-2a50-4204-bd61-08dbcf3a7d28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76273f48-b147-409a-bb7e-a16f91928371",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa5ea846-2a50-4204-bd61-08dbcf3a7d28",
                    "LayerId": "0dc206c7-d384-4879-b98e-d85b391ca4a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0dc206c7-d384-4879-b98e-d85b391ca4a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}