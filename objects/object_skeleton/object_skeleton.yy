{
    "id": "1dac8742-05d5-45ba-9242-3c0eab5b9ed0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_skeleton",
    "eventList": [
        {
            "id": "c654ae20-836f-4d99-8eda-68c34c4dd71f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1dac8742-05d5-45ba-9242-3c0eab5b9ed0"
        },
        {
            "id": "66ce69c3-bb57-4daa-87d3-6b0e1fefd531",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "77433527-2dde-43de-8a2e-465a43b16056",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1dac8742-05d5-45ba-9242-3c0eab5b9ed0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "04027611-5f9a-4fea-a4fe-fee830302721",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "60fc826e-3f91-40f4-8339-71b4346e2a3b",
    "visible": true
}