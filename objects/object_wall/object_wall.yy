{
    "id": "8ded4a01-2316-438a-8115-e425ac15a07a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_wall",
    "eventList": [
        {
            "id": "a3f7925d-3658-4775-9dcb-9f1fd1d7fa5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8ded4a01-2316-438a-8115-e425ac15a07a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "a959a9ce-1fdf-40c2-b0be-b2b087897895",
    "visible": true
}