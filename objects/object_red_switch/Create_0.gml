/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 798611C8
/// @DnDArgument : "code" "doorsAreClear = true;$(13_10)isActivated = false; // has not been thrown$(13_10)image_speed = 0; // stop animating$(13_10)image_index = 0; // set subimage to 0 (first frame)$(13_10)$(13_10)// Create an array of all red doors$(13_10)// the room (red_doors)$(13_10)numberofRedDoors = instance_number(object_red_door);$(13_10)$(13_10)for (var i = 0; i < numberofRedDoors; i++ ) {$(13_10)	red_doors[i] = instance_find(object_red_door, i);$(13_10)}$(13_10)$(13_10)// instance_number(object) tells us how many objects there are$(13_10)// instance_find(object, index) gets us the unique id of the object$(13_10)// in an idex position"
doorsAreClear = true;
isActivated = false; // has not been thrown
image_speed = 0; // stop animating
image_index = 0; // set subimage to 0 (first frame)

// Create an array of all red doors
// the room (red_doors)
numberofRedDoors = instance_number(object_red_door);

for (var i = 0; i < numberofRedDoors; i++ ) {
	red_doors[i] = instance_find(object_red_door, i);
}

// instance_number(object) tells us how many objects there are
// instance_find(object, index) gets us the unique id of the object
// in an idex position

/// @DnDAction : YoYo Games.Instances.Set_Sprite
/// @DnDVersion : 1
/// @DnDHash : 092C3AD3
/// @DnDArgument : "spriteind" "sprite_red_switch"
/// @DnDSaveInfo : "spriteind" "d6b78fe7-1d27-4535-bfc2-6fbee796c725"
sprite_index = sprite_red_switch;
image_index = 0;

/// @DnDAction : YoYo Games.Instances.Sprite_Animation_Speed
/// @DnDVersion : 1
/// @DnDHash : 2D65A550
/// @DnDArgument : "speed" "0"
image_speed = 0;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 44DA8E5D
/// @DnDArgument : "expr" "true"
/// @DnDArgument : "var" "canPlaySound"
canPlaySound = true;