{
    "id": "aebd6d0b-8aa7-4f05-99da-459e90ad3fcf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_red_switch",
    "eventList": [
        {
            "id": "6ceaf3bc-d3c8-47ff-84a7-343175f5d7a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aebd6d0b-8aa7-4f05-99da-459e90ad3fcf"
        },
        {
            "id": "42c8a5c5-a86d-496c-9436-c42dd5b9ddcb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "207726f4-b8e3-4d9e-8d92-766b2da070c9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "aebd6d0b-8aa7-4f05-99da-459e90ad3fcf"
        },
        {
            "id": "1a763776-a39b-45e7-9f6e-6079164d591c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "aebd6d0b-8aa7-4f05-99da-459e90ad3fcf"
        },
        {
            "id": "861d03d2-715e-498f-9492-9d9974d7aa20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "aebd6d0b-8aa7-4f05-99da-459e90ad3fcf"
        },
        {
            "id": "f8eb1186-d725-4dfc-a859-78ec59800c04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "aebd6d0b-8aa7-4f05-99da-459e90ad3fcf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d6b78fe7-1d27-4535-bfc2-6fbee796c725",
    "visible": true
}