/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 7141D5B8
/// @DnDArgument : "code" "// we are checking to make sure that if the doors need to be moved back$(13_10)// then we check to see if any door position is blocked$(13_10)// NOTE: only check doors if the switch has been activated$(13_10)if (isActivated) {$(13_10)	doorsAreClear = true;$(13_10)	var i$(13_10)	for (i = 0; i < array_length_1d(red_doors); i++)$(13_10)	{$(13_10)		if ( place_meeting(red_doors[i].xstart, red_doors[i].ystart, all) )$(13_10)		{$(13_10)			doorsAreClear = false;$(13_10)		}$(13_10)	}$(13_10)}"
// we are checking to make sure that if the doors need to be moved back
// then we check to see if any door position is blocked
// NOTE: only check doors if the switch has been activated
if (isActivated) {
	doorsAreClear = true;
	var i
	for (i = 0; i < array_length_1d(red_doors); i++)
	{
		if ( place_meeting(red_doors[i].xstart, red_doors[i].ystart, all) )
		{
			doorsAreClear = false;
		}
	}
}