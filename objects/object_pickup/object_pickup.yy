{
    "id": "fe52e29f-466d-4946-b15c-3802d5243550",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_pickup",
    "eventList": [
        {
            "id": "139dcbf9-56ab-45af-a15e-80a040c41040",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fe52e29f-466d-4946-b15c-3802d5243550"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3d79493f-9d65-4d41-b82f-cc15b50a9adf",
    "visible": true
}