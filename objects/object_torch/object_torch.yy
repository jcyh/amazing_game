{
    "id": "538af1f3-09e7-469d-9cda-01e4dd8b9d0f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_torch",
    "eventList": [
        {
            "id": "864a0128-92e8-4c34-b524-636eb27b3e90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "538af1f3-09e7-469d-9cda-01e4dd8b9d0f"
        },
        {
            "id": "0ca01f5f-9c67-4544-adb1-60157a0160d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "207726f4-b8e3-4d9e-8d92-766b2da070c9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "538af1f3-09e7-469d-9cda-01e4dd8b9d0f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8a752168-d642-40aa-90ed-1205f25d7b21",
    "visible": true
}