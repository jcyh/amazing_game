/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 6BD530B2
/// @DnDArgument : "code" "// rotate torch a bit to be angled from the wall$(13_10)// depends on which wall is nearest$(13_10)$(13_10)rotate_angle = 20;$(13_10)nearest_wall = instance_nearest(x, y, object_wall);$(13_10)if (nearest_wall.x >= x) {$(13_10)	image_angle += rotate_angle;$(13_10)}$(13_10)else {$(13_10)	image_angle -= rotate_angle;$(13_10)}"
// rotate torch a bit to be angled from the wall
// depends on which wall is nearest

rotate_angle = 20;
nearest_wall = instance_nearest(x, y, object_wall);
if (nearest_wall.x >= x) {
	image_angle += rotate_angle;
}
else {
	image_angle -= rotate_angle;
}