/// @DnDAction : YoYo Games.Movement.Jump_To_Point
/// @DnDVersion : 1
/// @DnDHash : 68862D9B
/// @DnDArgument : "x" "object_player.x"
/// @DnDArgument : "y" "object_player.y + 16"
x = object_player.x;
y = object_player.y + 16;

/// @DnDAction : YoYo Games.Instances.Set_Alarm
/// @DnDVersion : 1
/// @DnDHash : 70923E23
/// @DnDArgument : "steps" "room_speed"
alarm_set(0, room_speed);