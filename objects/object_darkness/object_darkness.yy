{
    "id": "cda78fa0-f5aa-475c-95d3-a12c876a7c97",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_darkness",
    "eventList": [
        {
            "id": "9869c6f7-38c5-4a23-992f-3d83e1da08b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cda78fa0-f5aa-475c-95d3-a12c876a7c97"
        },
        {
            "id": "2f3bb262-2386-4a82-9494-1abfc58afff6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "cda78fa0-f5aa-475c-95d3-a12c876a7c97"
        },
        {
            "id": "7785bd05-f4e5-43da-a2af-40ca046aead8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "cda78fa0-f5aa-475c-95d3-a12c876a7c97"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "34dba03d-8014-4cdd-b5a2-98f3be2b8b09",
    "visible": true
}