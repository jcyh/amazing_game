/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 6EBB6F6A
/// @DnDArgument : "code" "// destroy the pickup$(13_10)instance_destroy(other);$(13_10)$(13_10)if (other.type == "points") {$(13_10)	var bonus = other.image_index * 10;$(13_10)	game_controller.p_score += bonus;$(13_10)}$(13_10)else if (other.type == "health") {$(13_10)	game_controller.p_health = 100;$(13_10)}$(13_10)else if (other.type == "lives") {$(13_10)	game_controller.p_lives += 1;$(13_10)}$(13_10)else if (other.type == "damage") {$(13_10)	var damage = irandom_range(20,60);$(13_10)	game_controller.p_health -= damage;$(13_10)}"
// destroy the pickup
instance_destroy(other);

if (other.type == "points") {
	var bonus = other.image_index * 10;
	game_controller.p_score += bonus;
}
else if (other.type == "health") {
	game_controller.p_health = 100;
}
else if (other.type == "lives") {
	game_controller.p_lives += 1;
}
else if (other.type == "damage") {
	var damage = irandom_range(20,60);
	game_controller.p_health -= damage;
}