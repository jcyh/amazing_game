{
    "id": "207726f4-b8e3-4d9e-8d92-766b2da070c9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_player",
    "eventList": [
        {
            "id": "8e94e311-8d37-4185-8c16-474293060dc0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "207726f4-b8e3-4d9e-8d92-766b2da070c9"
        },
        {
            "id": "9b70e84d-4f5b-4d1c-80a8-86aa775eeab4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "207726f4-b8e3-4d9e-8d92-766b2da070c9"
        },
        {
            "id": "265bc04f-a3c7-469b-8115-c4ebd2ec073e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 5,
            "m_owner": "207726f4-b8e3-4d9e-8d92-766b2da070c9"
        },
        {
            "id": "451d6776-f9fa-4687-9f0d-cf9df3329c85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "207726f4-b8e3-4d9e-8d92-766b2da070c9"
        },
        {
            "id": "a36a8a8b-c86e-46ec-aaf9-4bb43b313b4d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "207726f4-b8e3-4d9e-8d92-766b2da070c9"
        },
        {
            "id": "c4b132ba-a7f5-4ac5-9374-837578ca8464",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "207726f4-b8e3-4d9e-8d92-766b2da070c9"
        },
        {
            "id": "29557115-51b9-4c15-9f4a-d8fc683c3e46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "8ded4a01-2316-438a-8115-e425ac15a07a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "207726f4-b8e3-4d9e-8d92-766b2da070c9"
        },
        {
            "id": "166c3da4-3167-4fff-acc1-34b7bc59d8b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "5575be2f-cdc1-45e7-a4c5-bc6647de07eb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "207726f4-b8e3-4d9e-8d92-766b2da070c9"
        },
        {
            "id": "4a258b11-14ce-4a97-80e3-f64720d35330",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 33,
            "eventtype": 9,
            "m_owner": "207726f4-b8e3-4d9e-8d92-766b2da070c9"
        },
        {
            "id": "3f7396a6-e05f-4450-abce-2829a2617556",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 34,
            "eventtype": 9,
            "m_owner": "207726f4-b8e3-4d9e-8d92-766b2da070c9"
        },
        {
            "id": "da01968f-43f9-40fd-82cc-13d723a65d5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "04027611-5f9a-4fea-a4fe-fee830302721",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "207726f4-b8e3-4d9e-8d92-766b2da070c9"
        },
        {
            "id": "4665649a-dae7-4ab7-b76f-6e99484b389e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "77433527-2dde-43de-8a2e-465a43b16056",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "207726f4-b8e3-4d9e-8d92-766b2da070c9"
        },
        {
            "id": "ac34f02d-26ab-4fd1-a40d-745b5ada6a07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "fe52e29f-466d-4946-b15c-3802d5243550",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "207726f4-b8e3-4d9e-8d92-766b2da070c9"
        },
        {
            "id": "15cadf4b-38c1-43a0-89f3-4120a3e43ff4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "538af1f3-09e7-469d-9cda-01e4dd8b9d0f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "207726f4-b8e3-4d9e-8d92-766b2da070c9"
        },
        {
            "id": "4a3db908-8504-4784-9a10-6d03c0c0f737",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 2,
            "m_owner": "207726f4-b8e3-4d9e-8d92-766b2da070c9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c0b6f91e-790c-44b2-b4d6-966658134364",
    "visible": true
}