/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 475BCA4C
/// @DnDArgument : "code" "// Display Start Page Content$(13_10)if (state == "start") {$(13_10)	// display the title$(13_10)	draw_set_font(font_titles);$(13_10)	draw_set_halign(fa_center)$(13_10)	title = "The Labyrinth of Death and the\nLost Amulet of Onat";$(13_10)	draw_text_color(512, 50, title, c_maroon, c_maroon, c_red, c_red, 1);$(13_10)$(13_10)	// display instructions (give objectives/how to play the game)$(13_10)	instructions = "Delve deep into the catabombs to seek fame and fortune";$(13_10)	instructions += "\nby recovering the mythical lost amulet of Onat;";$(13_10)	instructions += "\nbut be warned, no one who has entered the ";$(13_10)	instructions += "\ncatacombs has returned to tell the tale. ";$(13_10)	instructions += "\nCan you fing the amulet and return alive?";$(13_10)	draw_set_font(font_hud);$(13_10)	draw_set_halign(fa_left);$(13_10)	draw_text(120, 250, instructions);$(13_10)}$(13_10)if (state == "game_over")$(13_10){$(13_10)	draw_set_font(font_titles);$(13_10)	draw_set_halign(fa_center)$(13_10)	title = "Game Over";$(13_10)	draw_text_color(512, 50, title, c_maroon, c_maroon, c_red, c_red, 1);$(13_10)}"
// Display Start Page Content
if (state == "start") {
	// display the title
	draw_set_font(font_titles);
	draw_set_halign(fa_center)
	title = "The Labyrinth of Death and the\nLost Amulet of Onat";
	draw_text_color(512, 50, title, c_maroon, c_maroon, c_red, c_red, 1);

	// display instructions (give objectives/how to play the game)
	instructions = "Delve deep into the catabombs to seek fame and fortune";
	instructions += "\nby recovering the mythical lost amulet of Onat;";
	instructions += "\nbut be warned, no one who has entered the ";
	instructions += "\ncatacombs has returned to tell the tale. ";
	instructions += "\nCan you fing the amulet and return alive?";
	draw_set_font(font_hud);
	draw_set_halign(fa_left);
	draw_text(120, 250, instructions);
}
if (state == "game_over")
{
	draw_set_font(font_titles);
	draw_set_halign(fa_center)
	title = "Game Over";
	draw_text_color(512, 50, title, c_maroon, c_maroon, c_red, c_red, 1);
}