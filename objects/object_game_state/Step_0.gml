/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 5D99CA7D
/// @DnDArgument : "code" "if (state == "start")$(13_10){$(13_10)	// If user hits enter, begin the game$(13_10)	if (keyboard_check_released(vk_enter))$(13_10)	{$(13_10)		state = "game";$(13_10)		room_goto_next();$(13_10)	}$(13_10)}$(13_10)if (state == "game_over")$(13_10){$(13_10)	// If user hits enter, restart the game$(13_10)	if (keyboard_check_released(vk_enter))$(13_10)	{$(13_10)		game_restart();$(13_10)	}$(13_10)}"
if (state == "start")
{
	// If user hits enter, begin the game
	if (keyboard_check_released(vk_enter))
	{
		state = "game";
		room_goto_next();
	}
}
if (state == "game_over")
{
	// If user hits enter, restart the game
	if (keyboard_check_released(vk_enter))
	{
		game_restart();
	}
}