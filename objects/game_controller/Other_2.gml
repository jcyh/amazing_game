/// @DnDAction : YoYo Games.Drawing.Set_Font
/// @DnDVersion : 1
/// @DnDHash : 6A30BED7
/// @DnDArgument : "font" "font_hud"
/// @DnDSaveInfo : "font" "615e6bfc-c553-4a4e-a161-fca73ee97353"
draw_set_font(font_hud);

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 76701C2D
/// @DnDArgument : "code" "// set initial game variables$(13_10)p_score = 0;$(13_10)p_health = 100;$(13_10)p_lives = 3;$(13_10)$(13_10)// Set the display to the upper left corner $(13_10)// of the screen$(13_10)x = view_xport[view_current] + 32;$(13_10)y = view_yport[view_current] + 32;"
// set initial game variables
p_score = 0;
p_health = 100;
p_lives = 3;

// Set the display to the upper left corner 
// of the screen
x = view_xport[view_current] + 32;
y = view_yport[view_current] + 32;