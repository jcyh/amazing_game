{
    "id": "3f6e867e-e83a-4c75-8307-31cece1861f5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "game_controller",
    "eventList": [
        {
            "id": "179b385c-900d-4c69-9ba3-fd995bb2050b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "3f6e867e-e83a-4c75-8307-31cece1861f5"
        },
        {
            "id": "88140c49-61d6-4a24-a38d-1f63d428da5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3f6e867e-e83a-4c75-8307-31cece1861f5"
        },
        {
            "id": "fddb9c26-0adf-4171-8eb3-a424ba569a4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3f6e867e-e83a-4c75-8307-31cece1861f5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}