/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 19C3AC6A
/// @DnDArgument : "code" "// During game state$(13_10)if (object_game_state.state == "game") {$(13_10)	// Slowly take health away$(13_10)	p_health -= 0.03;$(13_10)$(13_10)	if (p_health <= 0)$(13_10)	{$(13_10)		p_lives -= 1;$(13_10)		room_restart();$(13_10)		p_health = 100;$(13_10)	}$(13_10)	if (p_lives <= 0)$(13_10)	{$(13_10)		object_game_state.state = "game_over";$(13_10)		room_goto(room_first);$(13_10)	}$(13_10)}"
// During game state
if (object_game_state.state == "game") {
	// Slowly take health away
	p_health -= 0.03;

	if (p_health <= 0)
	{
		p_lives -= 1;
		room_restart();
		p_health = 100;
	}
	if (p_lives <= 0)
	{
		object_game_state.state = "game_over";
		room_goto(room_first);
	}
}