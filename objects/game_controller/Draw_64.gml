/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 7A7947D9
/// @DnDArgument : "code" "if (object_game_state.state == "game")$(13_10){$(13_10)	draw_set_font(font_hud);$(13_10)	draw_set_halign(fa_left);$(13_10)	// Draw a health bar$(13_10)	draw_healthbar(x, y, x + 150, y + 20,$(13_10)	p_health, c_gray, c_red, c_lime, 0, true, true);$(13_10)$(13_10)	// Draw lives and score as text$(13_10)	draw_text(x, y + 30, "Score: " + string(p_score));$(13_10)$(13_10)	// Loop$(13_10)	for (var i = 0; i < p_lives; i++ ) {$(13_10)		draw_sprite(sprite_player_down, 0, x + 16 + i * 40, y + 70);$(13_10)	}$(13_10)}"
if (object_game_state.state == "game")
{
	draw_set_font(font_hud);
	draw_set_halign(fa_left);
	// Draw a health bar
	draw_healthbar(x, y, x + 150, y + 20,
	p_health, c_gray, c_red, c_lime, 0, true, true);

	// Draw lives and score as text
	draw_text(x, y + 30, "Score: " + string(p_score));

	// Loop
	for (var i = 0; i < p_lives; i++ ) {
		draw_sprite(sprite_player_down, 0, x + 16 + i * 40, y + 70);
	}
}