{
    "id": "77433527-2dde-43de-8a2e-465a43b16056",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_red_door",
    "eventList": [
        {
            "id": "2b29c7b1-2891-4123-bd2a-0cdb13111ece",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "77433527-2dde-43de-8a2e-465a43b16056"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "36e9d2bc-c39b-4b93-acc2-43fd11112509",
    "visible": true
}